/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Base/Portability.hpp"
#include "Source/Resources/EventQueue.hpp"

#include <optional>

#include <features.h>
#include <linux/version.h>

//
// Check support to display a useful error message
//

#if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 0)
    // For clarification: Linux kernel 2.6 was released in 2003. This project
    // isn't made for platforms **that** old.
    #error "Linux version is way too old to support epoll(7)"
#endif

#if defined (__GLIBC__) && defined (__GLIBC_MINOR__)
#   if __GLIBC__ < 2 || __GLIBC_MINOR__ < 3
#       error "glibc is too old for epoll(7) support!"
#   endif
#else
#   warning "Standard library isn't glibc, epoll(7) support is untested"
#endif

#include <sys/epoll.h>
#include <cerrno>

#include <array>
#include <thread>
#include <iostream>
#include <sstream>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 8)
    #define EPOLL_SIZE 1
#else
    #define EPOLL_SIZE 32
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 9)
#warning "Linux version before 2.6.9 contained a bug that caused epoll_ctl(2) "
         "with EPOLL_CTL_DEL and events as a NULL pointer to be reported as an"
         " error. This implementation ignores this problem, but details can be "
         "found in a modern version of the epoll(7) Linux Programmers' manual."
#endif

namespace Resources {

    using namespace std::chrono_literals;
    constexpr std::chrono::duration pollTimeout = 15ms;

    EventQueue::EventQueue() {
        eventfd = epoll_create(EPOLL_SIZE);
        if (eventfd == -1) {
            throw EventQueueException{"[Linux] Failed to create epoll(7)!"};
        }
    }

    EventQueue::~EventQueue() {
        if (eventfd != -1) {
            close(eventfd);
            errno = 0;
        }
    }

    void EventQueue::Terminate() {
        terminateFlag = true;

        if (eventfd != -1) {
            close(eventfd);
            eventfd = -1;
        }

        // TODO
    }

    [[nodiscard]] BASE_PORTABILITY_CXX20_CONSTEXPR inline struct epoll_event
    CreateEvent(int fd, Connection *connection) {
        struct epoll_event event{};

        event.data.fd = fd;

        event.events = EPOLLONESHOT
                     | EPOLLRDHUP
                     | EPOLLPRI;

        if (connection->ioState == IOState::WANT_READ) {
            event.events |= EPOLLIN;
        } else if (connection->ioState == IOState::WANT_WRITE) {
            event.events |= EPOLLOUT;
        }

        return event;
    }

    void
    EventQueue::Update(Connection *connection) {
        if (connection->ioState == IOState::READY ||
                connection->ioState == IOState::DEAD) {
            std::cout << "\033[35mEQ> Modify was invalid\033[0m\n";
            return;
        }

        auto sockfd = connection->GetSocket();
        auto event = CreateEvent(sockfd, connection);

        connection->isRescheduled = true;
        epoll_ctl(eventfd, EPOLL_CTL_MOD, sockfd, &event);
    }

    void
    EventQueue::Register(std::unique_ptr<Connection> &&inConnection) {
        std::lock_guard guard(mapMutex);
        auto socket = inConnection->GetSocket();

        const auto it = map.emplace(socket, std::move(inConnection));

        auto event = CreateEvent(socket, it.first->second.connection.get());

        if (epoll_ctl(eventfd, EPOLL_CTL_ADD, event.data.fd, &event) == -1) {
            perror("[Linux] Failed to add to epoll(7) using epoll_ctl(2)");
            throw EventQueueException{"[Linux] epoll_ctl(2) failed"};
        }
    }

    EventQueue::Entry *
    EventQueue::Poll() {
        struct epoll_event event{};

        int ret;
        do {
            if (terminateFlag) {
                std::cout << "\033[35mEQ> Poll got terminate\033[0m\n";
                return nullptr;
            }
            ret = epoll_wait(eventfd, &event, 1, this->timeout);
        } while (ret == 0);

        if (ret == -1) {
            if (errno != EINTR) {
                perror("[Linux] epoll_wait failure information");
            }
            return nullptr;
        }

        auto mapGuard = std::make_optional<std::lock_guard<std::mutex>>(mapMutex);

        auto it = map.find(event.data.fd);
        if (it == std::end(map)) {
            std::cout << "\033[35mEQ> Poll got empty for fd: " << event.data.fd << "\033[0m\n";
            return nullptr;
        }

        if (event.events & EPOLLRDHUP) {
            it->second.connection->ioState = IOState::DEAD;

            mapGuard = std::nullopt;
            Remove(it->second.connection.get());
            return nullptr;
        }

        it->second.connection->ioState = IOState::READY;

        return std::addressof(it->second);
    }

    void EventQueue::Remove(Connection *connection) {
        const auto fd = connection->GetSocket();
        std::lock_guard mapGuard(mapMutex);

        // remove from map (user space resources)
        auto it = map.find(fd);
        if (it != std::end(map)) {
            map.erase(it);
        } else {
            std::stringstream stream{};

            stream << "Fatal Error in EventQueue.\n";
            stream << "Function: " << __PRETTY_FUNCTION__ << '\n';
            stream << "Description: failed to remove connection from map.\n";
            stream << "Map size: " << std::size(map) << '\n';

            throw std::runtime_error(stream.str());
        }

        // remove from epoll (kernel space resources)
        (void)epoll_ctl(eventfd, EPOLL_CTL_DEL, fd, nullptr);
        // done
    }

} // namespace Resources

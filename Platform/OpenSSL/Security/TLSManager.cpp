/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <cstdio>

#include <openssl/err.h>
#include <Security/TLSManager.hpp>

#include "Source/Resources/TLSConnection.hpp"

namespace Security {

    namespace ALPNs {

        const unsigned char HTTP_1_ONLY[] = "\x08http/1.1";

        const unsigned char HTTP_2_ONLY[] = "\x02h2";

        const unsigned char HTTP_1_AND_2[] = "\x02h2\x08http/1.1";
    }

    int
    OpenSSLALPNSelector(SSL *, const unsigned char **out,
                        unsigned char *outlen, const unsigned char *in,
                        unsigned int inlen, void *data) {
        auto *tlsManager = static_cast<TLSManager *>(data);

        const unsigned char *protocols;
        unsigned int protocolsLength;

        if (tlsManager->enabledProtocols.http1 &&
                tlsManager->enabledProtocols.http2) {
            protocols = ALPNs::HTTP_1_AND_2;
            protocolsLength = sizeof(ALPNs::HTTP_1_AND_2);
        } else if (tlsManager->enabledProtocols.http1) {
            protocols = ALPNs::HTTP_1_ONLY;
            protocolsLength = sizeof(ALPNs::HTTP_1_ONLY);
        } else if (tlsManager->enabledProtocols.http2) {
            protocols = ALPNs::HTTP_2_ONLY;
            protocolsLength = sizeof(ALPNs::HTTP_2_ONLY);
        } else {
            throw std::runtime_error("Illegal ALPN state! No ALPN mode selected");
        }

        auto result = SSL_select_next_proto(const_cast<unsigned char **>(out), outlen, protocols,
                                            protocolsLength, in, inlen);

        return result == OPENSSL_NPN_NEGOTIATED ?
                              SSL_TLSEXT_ERR_OK :
                              SSL_TLSEXT_ERR_NOACK;
    }

    // TODO The failure-checking of this function can be improved.
    TLSManager::TLSManager(const Base::TLSConfiguration &configuration,
                           const Base::EnabledProtocols &enabledProtocols)
            : configuration(configuration)
            , enabledProtocols(enabledProtocols) {
        context = SSL_CTX_new(SSLv23_method());

        if (!context) {
            throw TLSManagerException{"[OpenSSL] Failed to create context!"};
        }

        if (SSL_CTX_use_certificate_file(context,
                                         configuration.certificateFile.c_str(),
                                         SSL_FILETYPE_PEM
                ) != 1) {
            ERR_print_errors_fp(stderr);
            throw TLSManagerException{"[OpenSSL] Failed to load certificate file!"};
        }

        if (SSL_CTX_use_PrivateKey_file(context, configuration.privateKeyFile.c_str(), SSL_FILETYPE_PEM) <= 0) {
            ERR_print_errors_fp(stderr);
            throw TLSManagerException{"[OpenSSL] Failed to load private-key file!"};
        }

        FILE *file = std::fopen(configuration.certificateChainFile.c_str(), "r");

        if (!file) {
            throw TLSManagerException{"[OpenSSL] Failed to load chain file!"};
        }

        while (X509 *cert = PEM_read_X509(file, nullptr, nullptr, nullptr)) {
            if (!SSL_CTX_add_extra_chain_cert(context, cert)) {
                ERR_print_errors_fp(stderr);
                fclose(file);
                X509_free(cert);
                throw TLSManagerException{"[OpenSSL] Failed to add extra chain certificate!"};
            }
        }

        // Clear errors from PEM_read_X509 since the error stack probably contains
        // an error regarding an EOF in the chain file.
        ERR_clear_error();

        std::fclose(file);

        SSL_CTX_set_min_proto_version(context, TLS1_2_VERSION);

        SSL_CTX_set_alpn_select_cb(context, OpenSSLALPNSelector, this);

        SSL_CTX_set_cipher_list(context, configuration.ciphers.c_str());

        // The following section is about TLS 1.3.
        //
        // TLS 1.3 is introduced in OpenSSL 1.1.1, and LibreSSL doesn't support
        // it (yet?).
#if !defined(LIBRESSL_VERSION_NUMBER) || OPENSSL_VERSION_NUMBER < 0x1010100
        SSL_CTX_set_ciphersuites(context, configuration.cipherSuites.c_str());
#endif
    }

    TLSManager::~TLSManager() noexcept {
        if (context) {
            SSL_CTX_free(context);
        }

        EVP_cleanup();
    }

    std::unique_ptr<Resources::Connection>
    TLSManager::CreateTLSWrappedConnection(int sockFD, Base::IPAddress ipAddress) {
        SSL *ssl = SSL_new(context);
        if (!ssl) {
            ERR_print_errors_fp(stderr);
            throw TLSManagerException{"[OpenSSL] Failed to SSL_new!"};
        }

        if (SSL_set_fd(ssl, sockFD) == 0) {
            throw TLSManagerException{"[OpenSSL] Failed to initialize fd to SSL*!"};
        }

        return std::make_unique<Resources::TLSConnection>(ssl, sockFD, ipAddress, configuration.printClientErrors);
    }

} // namespace Security

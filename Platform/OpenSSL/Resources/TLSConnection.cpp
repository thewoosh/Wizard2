/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <array>
#include <iostream>
#include <limits>

#include <cassert>
#include <cstring>

#include <openssl/err.h>

#include "Source/Base/Capabilities.hpp"
#include "Source/Base/Platforms.hpp"
#include "Source/Resources/TLSConnectionException.hpp"
#include "Source/Security/TLSManager.hpp"

namespace Resources {

    TLSConnection::~TLSConnection() noexcept {
        SSL_shutdown(data);
        SSL_free(data);
    }

    char
    TLSConnection::ReadChar() {
        if (!Setup()) return AsyncInterruptCharacter;

        char character;

        auto ret = SSL_read(data, &character, 1);
        if (ret != 1) {
            auto error = SSL_get_error(this->data, ret);

            if (error == SSL_ERROR_WANT_READ) {
                ioState = IOState::WANT_READ;
                return Connection::AsyncInterruptCharacter;
            }

            throw TLSConnectionException {
                "[OpenSSL] Failed ReadChar"
            };
        }

        return character;
    }

    std::size_t
    TLSConnection::ReadBlock(char *outData, std::size_t length) {
        std::size_t written{0};

        assert((length <= std::numeric_limits<int>::max()));

        do {
            int ret = SSL_read(this->data, outData, static_cast<int>(length));

            if (ret <= 0) {
                auto err = SSL_get_error(this->data, ret);

                if (err == SSL_ERROR_WANT_READ) {
                    ioState = IOState::WANT_READ;
                    return written;
                }

                throw TLSConnectionException {
                        "[OpenSSL] Failed ReadBlock"
                };
            }

            outData += ret;
            written += static_cast<std::size_t>(ret);
            length -= static_cast<std::size_t>(ret);
        } while (length != 0);

        return written;
    }

    std::size_t
    TLSConnection::Write(const char *srcData, std::size_t length) {
        assert((length <= std::numeric_limits<int>::max()));

        do {
            int ret = SSL_write(this->data, srcData, static_cast<int>(length));

            if (ret <= 0) {
                auto error = SSL_get_error(this->data, ret);

                if (error == SSL_ERROR_WANT_WRITE) {
                    ioState = IOState::WANT_WRITE;
                    return length;
                }

                ioState = IOState::DEAD;
                throw TLSConnectionException{"[OpenSSL] Failed Write"};
            }

            srcData += ret;
            length -= static_cast<std::size_t>(ret);
        } while (length != 0);

        return 0;
    }

    bool
    TLSConnection::SendFileUsingSendFile(int fileSocket, std::size_t size) {
#ifdef TLS_CAP_KTLS
        if (!BIO_get_ktls_send(SSL_get_wbio(this->data))) {
            return false;
        }

        return SSL_sendfile(this->data, fileSocket, 0, size, 0) >= 0;
#else
        static_cast<void>(this);
        static_cast<void>(fileSocket);
        static_cast<void>(size);
        return false;
#endif
    }

    bool
    TLSConnection::Setup() noexcept {
        return DoSetup();
    }

    bool
    TLSConnection::DoSetup() noexcept {
        if (handshakeFinished) return true;

        int status = SSL_accept(this->data);
        if (status == 1) {
            const unsigned char *alpnData;
            unsigned int alpnLen;

            SSL_get0_alpn_selected(this->data, &alpnData, &alpnLen);

            if (data != nullptr && alpnLen != 0) {
                if (alpnLen == 2 && memcmp(alpnData, "h2", 2) == 0) {
                    applicationProtocol = Base::ApplicationProtocol::HTTP_V2;
                } else if (alpnLen == 8 && memcmp(alpnData, "http/1.1", 8) == 0) {
                    applicationProtocol = Base::ApplicationProtocol::HTTP_V1_1;
                } else {
                    if (this->printErrors) {
                        std::cout << "Unknown alpn with len=" << alpnLen << " and data \""
                                  << std::string_view(reinterpret_cast<const char *>(alpnData), alpnLen) << "\"\n";
                    }
                }
            }

            handshakeFinished = true;
            return true;
        }

        auto err = SSL_get_error(this->data, status);

        if (err == SSL_ERROR_WANT_READ) {
            ioState = IOState::WANT_READ;
            return false;
        }

        if (err == SSL_ERROR_WANT_WRITE) {
            ioState = IOState::WANT_WRITE;
            return false;
        }

        ioState = IOState::DEAD;

        if (this->printErrors) {
            ERR_print_errors_fp(stderr);
        }

        return false;
    }

    std::size_t
    TLSConnection::SendFile(int fileSocket, std::size_t size) {
#ifdef BASE_PLATFORM_POSIX
        if (SendFileUsingSendFile(fileSocket, size)) {
            return 0; // TODO
        }

        std::array<char, 1024> buffer{};

        do {
            auto readRet = read(fileSocket, std::data(buffer), std::min(size, std::size(buffer)));
            static_assert(std::is_signed_v<decltype(readRet)>);

            if (readRet == 0) {
                throw TLSConnectionException{"[OpenSSL] EOF in SendFile: read(2)"};
            }

            if (readRet == -1) {
                throw TLSConnectionException{"[OpenSSL] Failed SendFile: read(2)"};
            }

            std::size_t writeRet = Write(std::data(buffer), static_cast<std::size_t>(readRet));
            if (writeRet != 0) {
                return size;
            }

            size -= static_cast<std::size_t>(readRet);
        } while (size > 0);

#else // i.e. not POSIX
        #error "Non-POSIX systems aren't supported at the time."
#endif // BASE_PLATFORM_POSIX

        return 0;
    }

} // namespace Resources

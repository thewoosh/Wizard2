/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Resources/EventQueue.hpp"

#include <array>
#include <thread>
#include <iostream>

#include <cerrno>
#include <cstring>

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

namespace Resources {

    using namespace std::chrono_literals;
    constexpr std::chrono::duration pollTimeout = 15ms;

    EventQueue::EventQueue() {
        kqueuefd = kqueue();
        if (kqueuefd == -1) {
            throw EventQueueException{"[BSD] Failed to create kqueue(2)!"};
        }
    }

    EventQueue::~EventQueue() {
        if (kqueuefd != -1) {
            close(kqueuefd);
            errno = 0;
        }
    }

    void EventQueue::Terminate() {
        terminateFlag = true;

        if (kqueuefd != -1) {
            close(kqueuefd);
            kqueuefd = -1;
        }

        // TODO
    }

    [[nodiscard]] constexpr inline struct kevent
    CreateEvent(int fd, Connection *connection) {
        struct kevent event{};
        event.ident = static_cast<uintptr_t>(fd);

        // may be overwritten by the following if-block
        event.fflags = 0;

        if (connection->ioState == IOState::WANT_READ) {
            // EVFILT_READ returns whenever there is data available to read.
            event.filter = EVFILT_READ;
        } else if (connection->ioState == IOState::WANT_WRITE) {
            // EVFILT_READ returns whenever it is possible to write to the
            // descriptor.
            event.filter = EVFILT_WRITE;
        } else {
            throw std::runtime_error("Illegal State for CreateEvent");
        }

        event.flags =
                EV_ADD |      // add the event to the queue
                EV_CLEAR |    // clear the internal state
                EV_DISPATCH | // disable the event on delivery
                EV_ONESHOT;   // only report the event to a single listener

        event.data = 0;
        event.udata = connection;

        return event;
    }

    void
    EventQueue::Update(Connection *connection) {
        if (connection->ioState == IOState::READY ||
            connection->ioState == IOState::DEAD) {
            return;
        }

        auto fd = connection->GetSocket();

        auto event = CreateEvent(fd, connection);
        connection->isRescheduled = true;

        if (kevent(kqueuefd, &event, 1, nullptr, 0, nullptr) == -1) {
            perror("[BSD] kevent(2) failure information");
        }
    }

    void
    EventQueue::Register(std::unique_ptr<Connection> &&connection) {
        std::lock_guard guard(mapMutex);
        auto socket = connection->GetSocket();
        const auto it = map.emplace(socket, std::move(connection));

        auto event = CreateEvent(socket, it.first->second.connection.get());

        if (kevent(kqueuefd, &event, 1, nullptr, 0, nullptr) == -1) {
            perror("[BSD] kevent(2) failure information");
        }
    }

    EventQueue::Entry *
    EventQueue::Poll() {
        struct kevent event{};

        struct timespec kTimeout{};
        kTimeout.tv_nsec = this->timeout * 1000000;

        int ret;
        do {
            if (terminateFlag) {
                return {};
            }
            ret = kevent(kqueuefd, nullptr, 0, &event, 1, &kTimeout);
        } while (ret == 0);

        if (ret == -1) {
            perror("[BSD] kevent(2) failure information");
            return nullptr;
        }

        std::lock_guard mapGuard(mapMutex);

        auto it = map.find(static_cast<SocketSSLGlue::SocketType>(event.ident));
        if (it == std::end(map)) {
            return nullptr;
        }

        // TODO verify that we shouldn't have to check the result of either
        //      event.events, ret or errno at this point, to check for EOS
        //      or a possible socket/resource error?
        reinterpret_cast<Connection *>(event.udata)->ioState = IOState::READY;

        return std::addressof(it->second);
    }

    void EventQueue::Remove(Connection *connection) {
        const auto fd = connection->GetSocket();
        std::lock_guard mapGuard(mapMutex);

        // remove from map (user space resources)
        auto it = map.find(fd);
        if (it != std::end(map)) {
            map.erase(it);
        }

        struct kevent event{};
        event.ident = static_cast<std::uintptr_t>(fd);
        event.flags = EV_DELETE;

        // remove from kqueue (kernel space resources)
        static_cast<void>(kevent(kqueuefd, &event, 1, nullptr, 0, nullptr));

        // done
    }

} // namespace Resources

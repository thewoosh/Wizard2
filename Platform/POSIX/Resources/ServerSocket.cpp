/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * This file is included by Source/Resources/ServerSocket.cpp
 */

#include <array>
#include <sstream>
#include <iostream>

#include <cerrno>
#include <cstring>
#include <cstdint>

#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#if !defined(AF_INET6) || defined(DONT_USE_IPV6)
#define FORCE_IPV4
#endif

#include "Source/Resources/SocketConnection.hpp"

namespace Resources {

    [[nodiscard]] static std::string
    FormatGenericError(int errnum) {
        std::array<char, 2048> messageLog{};
        strerror_r(errnum, messageLog.data(), messageLog.size());
        return std::string(std::begin(messageLog), std::end(messageLog));
    }

    [[nodiscard]] static std::string
    FormatBindError(int errnum) {
        if (errnum == EACCES)
            return "insufficient permissions";

        if (errnum == EADDRINUSE)
            return "port in use";

        return FormatGenericError(errnum);
    }

    [[nodiscard]] static Base::BasicFailure
    CreateListenFailure(int errnum, int port) {
        Base::BasicFailure failure{"ServerSocket"};

        failure.task = "binding to port " + std::to_string(port);

        if (errnum == EADDRINUSE) {
            failure.message = "port in use";
        } else {
            std::array<char, 2048> messageLog{};
            strerror_r(errnum, messageLog.data(), messageLog.size());
            failure.message = std::string(std::data(messageLog));
        }

        return failure;
    }

    [[nodiscard]] static int
    CreateSocket(Base::BasicFailure &failure) {
#ifdef FORCE_IPV4
        int sockfd = socket(AF_INET, SOCK_STREAM, 0);
#else
        int sockfd = socket(AF_INET6, SOCK_STREAM, 0);
#endif
        if (sockfd == -1) {
            failure = {"ServerSocket", "socket creation", FormatGenericError(errno)};
        }

        return sockfd;
    }

    [[nodiscard]] static Base::BasicFailure
    BindSocket(int sockfd, std::uint16_t port) {
#ifdef FORCE_IPV4
        struct sockaddr_in address{};
        address.sin_family = AF_INET;
        address.sin_port = htons(port);
        address.sin_addr.s_addr = htonl(INADDR_ANY);
#else
        struct sockaddr_in6 address{};
        address.sin6_family = AF_INET6;
        address.sin6_port = htons(port);
        address.sin6_addr = IN6ADDR_ANY_INIT;
#endif

        if (bind(sockfd, reinterpret_cast<struct sockaddr *>(&address), sizeof(address)) == -1) {
            return {"ServerSocket", "socket binding", FormatBindError(errno)};
        }

        return {};
    }

    [[nodiscard]] static Base::BasicFailure
    ConfigureSocket(int sockfd, std::uint16_t port) {
        static_cast<void>(port);

        int reuseOption = 1;

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuseOption, sizeof(reuseOption)) < 0) {
            return {"ServerSocket", "set non-blocking", FormatGenericError(errno)};
        }

        return {};
    }

    [[nodiscard]] static Base::BasicFailure
    ListenSocket(int sockfd, int port, int listenerBacklog) {
        if (listen(sockfd, listenerBacklog) == -1) {
            return CreateListenFailure(sockfd, port);
        }

        return {};
    }

    ServerSocket::ServerSocket(std::uint16_t port, int listenerBacklog) {
        sockfd = CreateSocket(failure);
        if (failure)
            return;
        if ((failure = ConfigureSocket(sockfd, port)))
            return;
        if ((failure = BindSocket(sockfd, port)))
            return;
        failure = ListenSocket(sockfd, port, listenerBacklog);
    }

    ServerSocket::~ServerSocket() noexcept {
        Shutdown();
    }

    ServerClientSocketData
    ServerSocket::AcceptSocket() noexcept {
#ifdef FORCE_IPV4
        struct sockaddr_in address{};
#else
        struct sockaddr_in6 address{};
#endif

        while (true) {
            socklen_t addressLength = sizeof(address);
            const auto fd = accept(sockfd, reinterpret_cast<sockaddr *>(&address), &addressLength);

            if (fd != -1) {
                if (fcntl(fd, F_SETFL, O_NONBLOCK) == -1) {
                    m_latestError = ServerSocketError::ACCEPT_FAILED_TO_SET_NON_BLOCKING;
                    return {};
                }

#ifdef FORCE_IPV4
                union Splitter {
                    std::uint32_t input{};
                    std::array<std::uint8_t, 4> output;
                };
                Splitter splitter;
                splitter.input = address.sin_addr.s_addr;
                return {client, Base::IPAddress::CreateIPv4(splitter.output)};
#else
                return {fd, Base::IPAddress::CreateIPv6(address.sin6_addr.s6_addr16)};
#endif
            }

            switch (errno) {
                case EAGAIN:
#if EAGAIN != EWOULDBLOCK
                case EWOULDBLOCK:
#endif
                case ECONNABORTED:
                    continue;
                case EBADF:
                    if (sockfd == -1) {
                        m_latestError = ServerSocketError::ACCEPT_SERVER_CLOSED_EXPLICITLY;
                        return {};
                    }
                    [[fallthrough]];
                default:
                    // other errors are caused by the OS closing the server socket,
                    // or the syscall indicating the call is malformed in another
                    // way.
                    m_latestError = static_cast<ServerSocketError>(static_cast<int>(ServerSocketError::ERRNO_BOUNDARY) +
                                                                   errno);
                    return {};
            }
        }
    }

    std::unique_ptr<Connection>
    ServerSocket::Accept() noexcept {
        const auto socket = AcceptSocket();
        if (socket.fd == -1)
            return nullptr;
        return std::make_unique<SocketConnection>(socket.fd, socket.ipAddress);
    }

    std::unique_ptr<Connection>
    ServerSocket::AcceptTLS(Security::TLSManager *manager) noexcept {
        const auto socket = AcceptSocket();
        if (socket.fd == -1)
            return nullptr;
        return manager->CreateTLSWrappedConnection(socket.fd, socket.ipAddress);
    }

    void
    ServerSocket::Shutdown() noexcept {
        if (sockfd != -1) {
            int fd = sockfd;
            sockfd = -1;
            close(fd);
        }
    }

}

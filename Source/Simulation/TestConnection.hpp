/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <string>
#include <string_view>
#include <utility>

namespace Simulation {

    struct ConnectionError
            : public std::exception {
        std::string message{};

        ConnectionError() = default;
        ConnectionError(const ConnectionError &) = default;
        ConnectionError(ConnectionError &&) = default;

        inline explicit
        ConnectionError(std::string message)
                : message(std::move(message)) {
        }

        [[nodiscard]] inline constexpr const char *
        what() const noexcept override {
            return std::data(message);
        }
    };

    class TestConnection {
        int sockfd{-1};
        struct sockaddr_in address{};

        inline void
        CreateSocket() {
            sockfd = socket(AF_INET, SOCK_STREAM, 0);

            if (sockfd == -1) {
                perror("socket(AF_INET, SOCK_STREAM, 0)");
                throw std::runtime_error("Failed socket creation");
            }
        }

        inline void
        SetSocketPort() {
            address.sin_family = AF_INET;
            address.sin_port = htons(64800);
        }

        inline void
        SetSocketAddress() {
            if (inet_pton(AF_INET, "127.0.0.1", &address.sin_addr) != 1) {
                perror("inet_pton(AF_INET, \"127.0.0.1\", &address.sin_addr)");
                throw std::runtime_error("Failed socket creation");
            }
        }

        inline void
        Connect() {
            if (connect(sockfd, reinterpret_cast<struct sockaddr *>(&address), sizeof(address)) == -1) {
                perror("connect(2)");
                throw std::runtime_error("Failed socket creation");
            }
        }

    public:
        [[nodiscard]] inline
        TestConnection() {
            CreateSocket();
            SetSocketPort();
            SetSocketAddress();
            Connect();
        }

        inline
        ~TestConnection() noexcept {
            if (sockfd != -1) {
                close(sockfd);
            }
        }

        inline void
        Write(std::string_view sv) const {
            const char *ptr = std::data(sv);
            std::size_t len = std::size(sv);

            do {
                const auto ret = write(sockfd, ptr, len);

                if (ret == -1) {
                    perror("TestConnection::Write -> write(2)");
                    throw ConnectionError{"write(2) failure"};
                }

                ptr += ret;
                len -= static_cast<std::size_t>(ret);
            } while (len > 0);
        }

        [[nodiscard]] inline char
        ReadChar() const {
            char data{};

            const auto ret = read(sockfd, &data, 1);

            if (ret == -1) {
                perror("TestConnection::ReadChar -> read(2)");
                throw ConnectionError{"read(2) failure"};
            }

            if (ret == 0) {
                throw ConnectionError{"read(2) eof"};
            }

            return data;
        }

        [[nodiscard]] inline std::string
        ReadUntilCRLF() const {
            std::string buf{};
            char data{};
            bool prevWasCarriage{false};

            do {
                const auto ret = read(sockfd, &data, 1);
                if (ret == -1) {
                    perror("TestConnection::ReadUntilCRLF -> read(2)");
                    throw ConnectionError{"read(2) failure"};
                }

                if (ret == 0) {
                    return buf;
                }

                if (prevWasCarriage) {
                    if (data == '\n') {
                        return buf;
                    }

                    throw ConnectionError{"CR not followed by LF"};
                }

                if (data == '\r') {
                    prevWasCarriage = true;
                } else {
                    buf += data;
                }
            } while (true);
        }
    };

} // namespace Simulation

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <algorithm>
#include <array>
#include <string_view>

#include <cstdlib>
#include <unistd.h>

namespace Simulation {

    const char fileNameTemplate[] = "/tmp/test-wizard-XXXXXXXX";

    class TempFile {

        static constexpr std::size_t FileNameSize =
                (sizeof(fileNameTemplate) / sizeof(fileNameTemplate[0])) - 1;

        std::array<char, FileNameSize> name{};
        int fd;

    public:
        [[nodiscard]] inline
        TempFile() {
            std::copy(std::cbegin(fileNameTemplate), std::cend(fileNameTemplate),
                      std::begin(name));
            fd = mkstemp(&*std::begin(name));

            if (fd == -1) {
                perror("mkstemp");
                throw std::runtime_error("TempFile: mkstemp failure");
            }
        }

        inline
        ~TempFile() noexcept {
            close(fd);
        }


        [[nodiscard]] inline int
        FD() const noexcept {
            return fd;
        }

        [[nodiscard]] inline std::string_view
        Path() const noexcept {
            return std::string_view(std::data(name), std::size(name));
        }

    };

} // namespace Simulation

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Content/FileTypeDetector.hpp"

namespace Content {

    FileTypeDetector::FileTypeDetector() noexcept
        : singleExtensionMap{
            // Main Text-Based Web Infrastructure
            {"css",  {"text",        "css",  true}},
            {"html", {"text",        "html", true}},
            {"json", {"application", "json", true}},
            {"js", {"application", "javascript", true}},

            // Images
            {"gif",  {"image", "gif",  false}},
            {"ico", {"image", "vnd.microsoft.icon", false}},
            {"jpeg", {"image", "jpeg", false}},
            {"jpg",  {"image", "jpeg", false}},
            {"png",  {"image", "png",  false}},
            {"svg",  {"image", "svg+xml", true}},

            // Fonts
            {"otf",   {"font", "otf",   false}},
            {"ttf",   {"font", "ttf",   false}},
            {"woff",  {"font", "woff",  false}},
            {"woff2", {"font", "woff2", false}},

            // Other text/* formats
            {"csv",  {"text", "csv", true}},
            {"md",  {"text", "markdown", true}},
            {"txt",  {"text", "plain", true}},

            // XML(-based) types
            {"xml", {"application", "xml", true}},
            {"xaml", {"application", "xaml", true}},

            // Office types
            {"pdf", {"application", "pdf", false}},
    } {
    }

    MediaType
    FileTypeDetector::DetectByFileName(std::string_view fileName) const noexcept {
        auto pos = fileName.find_last_of('.');
        if (pos == std::string::npos) return {};

        // convert the string view to a lower cased string
        std::string ext(fileName.substr(pos + 1));
        for (std::size_t i = 0; i < std::size(ext); ++i) {
            auto val = ext[i];
            if (val >= 'A' && val <= 'Z')
                ext[i] = static_cast<char>(val + 0x20);
        }

        auto it = singleExtensionMap.find(ext);

        if (it == std::end(singleExtensionMap)) {
            return {};
        }

        return it->second;
    }

} // namespace Content

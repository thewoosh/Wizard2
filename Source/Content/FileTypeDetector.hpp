/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Content/MediaType.hpp"

#include <map>
#include <string_view>

namespace Content {

    class FileTypeDetector {
        /**
         * This map contains keys which are extensions that don't have sub-
         * extension.
         *
         * E.g. "txt", "html", "css"
         * Not: "tar.gz", "tar.bz2"
         */
        std::map<std::string_view, MediaType> singleExtensionMap{};

    public:
        FileTypeDetector() noexcept;

        [[nodiscard]] MediaType
        DetectByFileName(std::string_view fileName) const noexcept;
    };

} // namespace Content

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Provider/IProvider.hpp"

#include <string>
#include <string_view>

namespace Content {

    /**
     * A media type conveys the type of resource transferred between the client
     * and the server. It helps the receiving end to interpret the data they've
     * received.
     *
     * A media type, in textual form, has the following format:
     * text/subtype
     * text/subtype;parameter=value
     * type/subtype;parameter=value;parameter=value;...
     *
     * Further Reading:
     *     IANA Media Type Registry
     *         https://www.iana.org/assignments/media-types/media-types.xhtml
     *     RFC 2046 "Multipurpose Internet Mail Extensions (MIME) Part Two:
     *               Media Types"
     *         https://tools.ietf.org/html/rfc2046
     *     RFC 6657 "Update to MIME regarding "charset" Parameter Handling in
                     Textual Media Types"
               https://tools.ietf.org/html/rfc6657
     *     RFC 6838 "Media Type Specifications and Registration Procedures"
     *         https://tools.ietf.org/html/rfc6838
     *
     */
    struct MediaType {

        /**
         * The  top-level  media type, or simply type, conveys the general type
         * of data.
         *
         * The top-level type can be any of the following:
         * - application
         * - audio
         * - example
         * - font
         * - image
         * - message
         * - model
         * - multipart
         * - text
         * - video
         */
        std::string_view type{};

        /**
         * The subtype specifies the format of the data.
         */
        std::string_view subtype{};

        bool includeCharset{false};

        [[nodiscard]]
        MediaType() = default;

        [[nodiscard]] constexpr
        MediaType(std::string_view type, std::string_view subtype) noexcept
            : type(type)
            , subtype(subtype) {
        }

        [[nodiscard]] constexpr
        MediaType(std::string_view type, std::string_view subtype,
                  bool includeCharset) noexcept
                : type(type)
                , subtype(subtype)
                , includeCharset(includeCharset) {
        }

        MediaType(const MediaType &) = default;
        MediaType(MediaType &&) = default;

        MediaType &operator=(const MediaType &) = default;
        MediaType &operator=(MediaType &&) = default;

    };

} // namespace Content

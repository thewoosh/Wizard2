/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <memory>

#include "Source/Base/Configuration.hpp"
#include "Source/Resources/Connection.hpp"

#ifdef TLS_USE_OPENSSL
    #include <openssl/ssl.h>
#else
    #error "Unknown TLS implementation!"
#endif

namespace Security {

    class TLSManagerException : public std::exception {
    private:
        const char *const info;

    public:
        explicit
        TLSManagerException(const char *info)
                : info(info) {
        }

        [[nodiscard]] const char *
        what() const noexcept override {
            return info;
        }
    };

    class TLSManager {
#ifdef TLS_USE_OPENSSL
        SSL_CTX *context{};
#endif
    public:
        const Base::TLSConfiguration &configuration;
        const Base::EnabledProtocols &enabledProtocols;

        TLSManager(const Base::TLSConfiguration &, const Base::EnabledProtocols &);
        ~TLSManager() noexcept;

        [[nodiscard]] std::unique_ptr<Resources::Connection>
        CreateTLSWrappedConnection(int, Base::IPAddress);
    };

} // namespace Security

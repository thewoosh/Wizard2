/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Security/PlatformFeatures.hpp"

#include <array>

#include <cstdio>
#include <cerrno>
#include <unistd.h>

#include "Source/Base/Configuration.hpp"
#include "Source/Base/Globals.hpp"
#include "Source/Base/Platforms.hpp"

namespace Security {

    std::array<const char *, 3> initializationPaths{};

    [[maybe_unused]]
    static void FailWith(const char *feature,
                         const char *string,
                         const char *value = nullptr) {
        if (isatty(STDOUT_FILENO)) {
            std::printf("\033[37m[\033[31mSecurity\033[37m] Feature \033[31m%s\033[37m failed: \033[31m%s",
                        feature, string);

            if (value) {
                std::printf("\033[37m with value: \033[31m%s", value);
            }

            std::puts("\033[0m");
        } else {
            std::printf("[Security] Feature '%s' failed: %s", feature, string);

            if (value) {
                std::printf(" with value: %s\n", value);
            } else {
                std::putc('\n', stdout);
            }
        }
    }

    [[maybe_unused]]
    [[nodiscard]] const char *
    ResolvePledgeFailure() noexcept {
        switch (errno) {
            case EFAULT: return "memory corruption";
            case EINVAL: return "malformed promises";
            case EPERM:  return "process is attempting to increase permissions";
            default:     return "(invalid errno for pledge)";
        }
    }

    [[maybe_unused]]
    [[nodiscard]] const char *
    ResolveUnveilFailure() noexcept {
        switch (errno) {
            case E2BIG:  return "path list is too large";
            case ENOENT: return "path doesn't exist";
            case EINVAL: return "invalid permissions entered";
            case EPERM:  return "process is attempting to increase permissions";
            default:     return "(invalid errno for unveil)";
        }
    }

    bool
    ApplyPlatformFeaturesBeforeConfiguration() {
#ifdef BASE_PLATFORM_OPENBSD
        const char *pledgePromises =
#ifndef NDEBUG
            "error "
#endif
            "stdio rpath tmppath inet unveil";

        if (pledge(pledgePromises, nullptr) == -1) {
            FailWith("pledge", ResolvePledgeFailure());
            return false;
        }
#endif
        return true;
    }

    bool
    ApplyPlatformFeaturesAfterConfiguration() {
#ifdef BASE_PLATFORM_OPENBSD
        initializationPaths[0] = Base::Globals::configuration->tlsConfiguration.certificateFile.c_str();
        initializationPaths[1] = Base::Globals::configuration->tlsConfiguration.certificateChainFile.c_str();
        initializationPaths[2] = Base::Globals::configuration->tlsConfiguration.privateKeyFile.c_str();

        for (const char *path : initializationPaths) {
            if (unveil(path, "r") == -1) {
                FailWith("unveil", ResolveUnveilFailure(), path);
                return false;
            }
        }
#endif

        return true;
    }

    bool
    ApplyPlatformFeaturesAfterInitialization() {
#ifdef BASE_PLATFORM_OPENBSD
        if (unveil("/var/www/html", "r") == -1) {
            FailWith("unveil", ResolveUnveilFailure(), "/var/www/html");
            return false;
        }

        for (const char *path : initializationPaths) {
            if (unveil(path, "") == -1) {
                FailWith("unveil-veil-after-configuration", ResolveUnveilFailure(), path);
                return false;
            }
        }

        if (unveil(nullptr, nullptr) == -1) {
            FailWith("unveil-lock", ResolveUnveilFailure());
            return false;
        }
#endif
        return true;
    }

} // namespace Security

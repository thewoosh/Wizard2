/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Security {

    enum class Violation {

        NONE,
        PATH_NAME_ESCAPES_PWD,

    };

    [[nodiscard]] inline constexpr const char *
    TranslateViolationToString(Violation violation) noexcept {
        switch (violation) {
            case Violation::NONE:
                return "none";
            case Violation::PATH_NAME_ESCAPES_PWD:
                return "path-name-escapes-pwd";
            default:
                return "illegal-value";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, Violation violation) {
        stream << TranslateViolationToString(violation);
        return stream;
    }

} // namespace Security

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <iterator>
#include <string_view>

#include <unistd.h>

#include "Source/Base/IPAddress.hpp"
#include "Source/Base/Protocol.hpp"
#include "Source/Resources/IOState.hpp"

namespace Resources {

    /**
     * Abstract interface for connections.
     *
     * Made abstract to support:
     * - raw platform-dependent streams (e.g. UNIX file descriptors)
     * - TLS-wrapped streams
     * - Mock streams for testing.
     *
     * Currently the interfaces use raw pointers for passing data, but in the
     * future we might want to port this to use
     */
    class Connection {
    public:
        // Returned by ReadChar() when the IO is waiting for data to come in.
        // Return type should be compared to this value, and then check the
        // waitForRead flag.
        static constexpr char AsyncInterruptCharacter = 0x00;

        [[nodiscard]] inline constexpr explicit
        Connection(int socketFD, Base::IPAddress address) noexcept
                : m_socket(socketFD)
                , m_ipAddress(address) {
        }

        IOState ioState{IOState::READY};
        bool isRescheduled{false};

        Base::ApplicationProtocol applicationProtocol{Base::ApplicationProtocol::HTTP_V1_1};

        //
        // Abstract interface functions to be implemented by the derived class.
        //

        virtual ~Connection() noexcept {
            close(m_socket);
        }

        virtual bool
        Setup() noexcept = 0;

        [[nodiscard]] virtual char
        ReadChar() = 0;

        [[nodiscard]] virtual std::size_t
        ReadBlock(char *, std::size_t) = 0;

        // returns how many octets weren't written.
        [[nodiscard]] virtual std::size_t
        Write(const char *, std::size_t) = 0;

        [[nodiscard]] virtual std::size_t
        SendFile(int, std::size_t) = 0;

        [[nodiscard]] inline constexpr int
        GetSocket() const noexcept {
            return m_socket;
        }

        [[nodiscard]] inline constexpr const Base::IPAddress &
        IPAddress() const noexcept {
            return m_ipAddress;
        }

        //
        // Template functions for ease and errorless use of the interface
        // functions.
        //

        template<typename T>
        [[nodiscard]] std::size_t
        ReadBlock(T &result) {
            return ReadBlock(std::begin(result), std::distance(std::begin(result), std::end(result)));
        }

        // returns how many octets weren't written.
        [[nodiscard]] std::size_t
        WriteStringView(std::string_view string) {
            return Write(string.data(), string.size());
        }

        template<typename Iterator>
        // returns how many octets weren't written.
        [[nodiscard]] std::size_t
        WriteIterators(const Iterator &begin, const Iterator &end) {
            return Write(static_cast<const char *>(&(*begin)), static_cast<std::size_t>(std::distance(begin, end)));
        }

        template<typename T>
        // returns how many octets weren't written.
        [[nodiscard]] std::size_t
        WriteContainer(const T &value) {
            return WriteIterators(std::cbegin(value), std::cend(value));
        }

        template<typename T>
        // returns how many octets weren't written.
        [[nodiscard]] std::size_t
        WriteContainer(T &value) {
            return WriteIterators(std::begin(value), std::end(value));
        }

    protected:
        int m_socket;
        Base::IPAddress m_ipAddress;
    };

} // namespace Resources

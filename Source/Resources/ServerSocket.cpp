/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Base/Platforms.hpp"

#include "Source/Resources/ServerSocket.hpp"

#ifdef BASE_PLATFORM_POSIX
    #include "Platform/POSIX/Resources/ServerSocket.cpp"
#else
    #error "Platform not supported!"
#endif

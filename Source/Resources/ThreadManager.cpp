/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "ThreadManager.hpp"

#include <cstdio>

#include "Source/Base/Thread.hpp"
#include "Source/HTTP/AbstractClient.hpp"
#include "Source/HTTP/v1/Client.hpp"
#include "Source/HTTP/v2/Client.hpp"
#include "Source/Resources/TLSConnectionException.hpp"

namespace Resources {

    [[nodiscard]] static std::unique_ptr<HTTP::AbstractClient>
    CreateClient(Connection *connection) {
        switch (connection->applicationProtocol) {
            case Base::ApplicationProtocol::HTTP_V1_1:
                return std::make_unique<HTTP::v1::Client>(connection);
            case Base::ApplicationProtocol::HTTP_V2:
                return std::make_unique<HTTP::v2::Client>(connection);
            default:
                return nullptr;
        }
    }

    void ThreadManager::ThreadEntrypoint(std::size_t number) {
        {
            std::string threadName{"JobThread-" + std::to_string(number)};
            Base::Thread::SetName(threadName);
        }

        while (!terminateFlag) {
            EventQueue::Entry *entry;

            try {
                entry = eventQueue.Poll();
                if (entry == nullptr) {
                    continue;
                }

                switch (entry->connectionStage) {
                    case ConnectionStage::SETUP:
                        if (!entry->connection->Setup()) {
                            if (entry->connection->ioState == Resources::IOState::DEAD) {
                                eventQueue.Remove(entry->connection.get());
                                continue;
                            }

                            eventQueue.Update(entry->connection.get());
                            continue;
                        }

                        // Setup was OK
                        entry->client = CreateClient(entry->connection.get());
                        entry->connectionStage = ConnectionStage::CONTINUE;
                        [[fallthrough]];
                    case ConnectionStage::CONTINUE:
                        do {
                            entry->client->Continue();
                        } while (entry->connection->ioState == IOState::READY);

                        if (entry->connection->ioState == IOState::DEAD) {
                            eventQueue.Remove(entry->connection.get());
                        } else {
                            eventQueue.Update(entry->connection.get());
                        }
                        break;
                    default:
                        break;
                }
            } catch (const Resources::TLSConnectionException &exception) {
                // An uncaught TLS exception is always bad news; terminate the
                // connection
                eventQueue.Remove(entry->connection.get());
            } catch (const std::exception &) {
                // Print information here that the exception escaped?
            }
        }
    }

    ThreadManager::~ThreadManager() noexcept {
        terminateFlag = true;
        eventQueue.Terminate();

        // Timeout of 500 ms per thread
        constexpr const timespec threadStopTimeout {
            0,
            500 * 1000000
        };

        // Join all threads.
        for (auto &thread : pool) {
#ifdef BASE_PLATFORM_POSIX
            auto result = pthread_timedjoin_np(thread.native_handle(), nullptr, &threadStopTimeout);
            if (result == 0)
                continue;
#ifndef NDEBUG
            if (result != ETIMEDOUT) {
                std::cout << "[ThreadManager] Warning: pthread_timedjoin_np return code " << result << '\n';
            } else {
                std::array<char, 128> buffer{};

#ifdef __FreeBSD__
                pthread_get_name_np(thread.native_handle(), std::data(buffer), std::size(buffer));
#elif defined(__linux__)
                pthread_getname_np(thread.native_handle(), std::data(buffer), std::size(buffer));
#else
                std::snprintf(std::data(buffer), std::size(buffer), "%llu",
                              static_cast<unsigned long long>(thread.native_handle()));
#endif

                std::cout << "[ThreadManager] Debug: Killing non-stopping thread \"" << std::data(buffer) << "\"\n";
            }
#endif // NDEBUG
            pthread_cancel(thread.native_handle());
#endif // BASE_PLATFORM_POSIX
            thread.join();
        }

        pool.clear();
    }

    void ThreadManager::ScheduleConnection(std::unique_ptr<Connection> &&connection) {
        eventQueue.Register(std::move(connection));
    }

} // namespace Resources

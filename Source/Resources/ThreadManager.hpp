/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <condition_variable>
#include <iostream>
#include <queue>
#include <thread>
#include <vector>

#include "Source/Resources/EventQueue.hpp"

namespace Resources {

    class ThreadManager {
        std::vector<std::thread> pool{};

        bool terminateFlag{false};

        EventQueue eventQueue{};

    public:
        explicit
        ThreadManager(std::size_t threadCount) {
            pool.reserve(threadCount);
            for (std::size_t i = 0; i < threadCount; i++) {
                pool.emplace_back([this, i]() { ThreadEntrypoint(i); });
            }
        }

        ThreadManager(const ThreadManager &) = delete;
        ~ThreadManager() noexcept;

        void
        ThreadEntrypoint(std::size_t);

        void ScheduleConnection(std::unique_ptr<Connection> &&);
    };

} // namespace Resources

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * TODO Implement a limit for the map.
 */

#pragma once

#include <exception>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <queue>

#include "Source/Base/Platforms.hpp"
#include "Source/HTTP/AbstractClient.hpp"
#include "Source/Resources/Connection.hpp"
#include "Source/Resources/SSLImplementation.hpp"

#ifdef BASE_PLATFORM_LINUX
#   define RESOURCES_EVENT_QUEUE_LINUX
#elif defined (BASE_PLATFORM_FREEBSD) || defined (BASE_PLATFORM_NETBSD) \
   || defined (BASE_PLATFORM_OPENBSD) || defined (BASE_PLATFORM_DRAGONFLYBSD) \
   || defined (BASE_PLATFORM_MACOS)
#   define RESOURCES_EVENT_QUEUE_BSD
#else
#   error "The platform-depended EventQueue component of this Software isn't supported for your platform."
#endif

namespace Resources {

    struct EventQueueException : public std::exception {
        std::string message{};

        template<typename T>
        explicit
        EventQueueException(const T &m) : message(std::forward<const T>(m)) {
        }

        [[nodiscard]] const char *
        what() const noexcept override {
            return message.c_str();
        }
    };

    enum class ConnectionStage {
        SETUP,
        CONTINUE
    };

    class EventQueue {
    public:
        struct Entry {
            std::unique_ptr<Connection> connection;
            std::unique_ptr<HTTP::AbstractClient> client{nullptr};
            ConnectionStage connectionStage{ConnectionStage::SETUP};

            [[nodiscard]] explicit
            Entry(std::unique_ptr<Connection> &&connection) noexcept
                    : connection(std::move(connection)) {
            }
        };

    private:

        std::mutex mapMutex{};
        int timeout{125};

        std::map<int, Entry> map{};

#ifdef RESOURCES_EVENT_QUEUE_LINUX
        /**
         * The epoll(7) file descriptor.
         */
        int eventfd{-1};
#elif defined (RESOURCES_EVENT_QUEUE_BSD)
        /**
         * The kqueue(2) file descriptor.
         */
        int kqueuefd;
#endif

        volatile bool terminateFlag{false};

    public:
        EventQueue();
        ~EventQueue();

        void Register(std::unique_ptr<Connection> &&);

        /**
         * Update the Connection entry to wait for events specified in the
         * Connection::ioState.
         */
        void Update(Connection *);

        [[nodiscard]] Entry *
        Poll();

        void
        Terminate();

        void
        Remove(Connection *);
    };

} // namespace Resources

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */
#pragma once

#include <cstdint>

#include <exception>
#include <memory>
#include <string>

#include "Source/Base/BasicFailure.hpp"
#include "Source/Base/IPAddress.hpp"
#include "Source/Resources/Connection.hpp"
#include "Source/Resources/ServerSocketError.hpp"
#include "Source/Security/TLSManager.hpp"

namespace Resources {

    struct ServerClientSocketData {
        int fd{-1};
        Base::IPAddress ipAddress{};
    };

    class ServerSocket {
    public:
        ServerSocket(std::uint16_t port, int listenerBacklog);

        ServerSocket(const ServerSocket &) = delete;

        ~ServerSocket() noexcept;

        [[nodiscard]] std::unique_ptr<Connection>
        Accept() noexcept;

        [[nodiscard]] std::unique_ptr<Connection>
        AcceptTLS(Security::TLSManager *) noexcept;

        [[nodiscard]] inline constexpr ServerSocketError
        LatestError() const noexcept {
            return m_latestError;
        }

        /**
         * Shutdown the socket manually (i.e. non-destruct), if it isn't
         * already shutdown.
         */
        void Shutdown() noexcept;

        Base::BasicFailure failure{"ServerSocket", "", ""};

    private:
#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
        volatile int sockfd{};
#else
#error "Platform not supported!"
#endif

        [[nodiscard]] ServerClientSocketData
        AcceptSocket() noexcept;

        ServerSocketError m_latestError{};
    };

}

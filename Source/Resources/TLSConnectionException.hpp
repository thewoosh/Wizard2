/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>

#include "Source/Security/TLSException.hpp"

namespace Resources {

    class TLSConnectionException
            : public Security::TLSException {
    private:
        const char *const info;

    public:
        [[nodiscard]] inline explicit
        TLSConnectionException(const char *info)
                : info(info) {
        }

        [[nodiscard]] const char *
        what() const noexcept override {
            return info;
        }
    };

} // namespace Resources

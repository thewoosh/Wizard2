/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace Resources {

    enum class ServerSocketError {
        NONE,

        ACCEPT_FAILED_TO_SET_NON_BLOCKING,
        ACCEPT_SERVER_CLOSED_EXPLICITLY,

        ERRNO_BOUNDARY = 1000,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(ServerSocketError serverSocketError) noexcept {
        switch (serverSocketError) {
            case ServerSocketError::NONE: return "none";
            case ServerSocketError::ACCEPT_FAILED_TO_SET_NON_BLOCKING: return "accept-failed-to-set-non-blocking";
            case ServerSocketError::ACCEPT_SERVER_CLOSED_EXPLICITLY: return "accept-server-closed-explicitly";
            default: return "(invalid)";
        }
    }

} // namespace Resources

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

namespace Resources {

    // The IO state is used to communicate abstractly with the scheduler about
    // continuing when data is ready to be read (WANT_READ) or written
    // (WANT_WRITE), or to reschedule when the requested state is reached. When
    // the connection is deemed unusable (DEAD), the scheduler can free up the
    // resources associated with the connection.
    //
    // The following example demonstrates such a mechanism:
    // HTTPv1 client - reading the header name
    //       |
    // Connection - reading a character
    //       |
    // Operating system - tells us no data is ready
    //       |
    // Connection - registers the IO state as the WANT_READ state.
    //       |
    // HTTPv1 client - notices the IO state mutation
    //       |
    // HTTPv1 client - register this event in the thread manager
    //       |
    // Thread manager - adds the requested event to the event queue.
    //       |
    // ...
    //
    enum class IOState {

        // The client can continue processing the request(s).
        READY,

        // Dead means the connection was closed/unrecoverable.
        DEAD,

        // A write was requested but the internal call resulted in EAGAIN or
        // something alike.
        WANT_READ,

        // A read was requested but the internal call resulted in EAGAIN or
        // something alike.
        WANT_WRITE

    };

    /**
     * Gets an string literal representation associated with the given state.
     */
    [[nodiscard]] inline constexpr const char *
    TranslateIOStateToUppercaseString(IOState state) noexcept {
        switch (state) {
            case IOState::READY: return "READY";
            case IOState::DEAD: return "DEAD";
            case IOState::WANT_READ: return "WANT_READ";
            case IOState::WANT_WRITE: return "WANT_WRITE";
            default: return "ILLEGAL_VALUE";
        }
    }

    /**
     * Gets an string literal representation associated with the given state.
     */
    [[nodiscard]] inline constexpr const char *
    TranslateIOStateToLowercaseString(IOState state) noexcept {
        switch (state) {
            case IOState::READY: return "ready";
            case IOState::DEAD: return "dead";
            case IOState::WANT_READ: return "want-read";
            case IOState::WANT_WRITE: return "want-write";
            default: return "illegal-value";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, IOState state) {
        if (stream.flags() & std::ios::uppercase) {
            stream << std::uppercase << TranslateIOStateToUppercaseString(state);
        } else {
            stream << TranslateIOStateToLowercaseString(state);
        }

        return stream;
    }

} // namespace Resources

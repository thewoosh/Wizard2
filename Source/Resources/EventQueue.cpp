/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Base/Platforms.hpp"

#include "Source/Resources/EventQueue.hpp"

#ifdef BASE_PLATFORM_LINUX
    #include "Platform/Linux/Resources/EventQueue.cpp"
#elif defined (BASE_PLATFORM_FREEBSD) || defined (BASE_PLATFORM_NETBSD) \
   || defined (BASE_PLATFORM_OPENBSD) || defined (BASE_PLATFORM_DRAGONFLYBSD) \
   || defined (BASE_PLATFORM_MACOS)
    #include "Platform/BSD/Resources/EventQueue.cpp"
#else
    #error "The platform-depended EventQueue component of this Software isn't supported for your platform."
#endif

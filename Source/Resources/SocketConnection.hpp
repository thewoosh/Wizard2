/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * TODO:
 *   The  functionality  of  this class should be separated using the Platform/
 *   directories,  thus  splitting  the implementation of the code by platform,
 *   thus avoiding the ugly #ifdef BASE_PLATFORM_... stuff.
 */

#pragma once

#include <exception>
#include <vector>

#ifdef __linux__
#include <sys/sendfile.h>
#endif

#include <unistd.h>

#include "Connection.hpp"

namespace Resources {

    class SocketConnection : public Connection {
    public:
        class SocketConnectionException : public std::exception {
        private:
            const char *const info;

        public:
            explicit
            SocketConnectionException(const char *info)
                    : info(info) {
            }

            [[nodiscard]] const char *
            what() const noexcept override {
                return info;
            }
        };

        inline constexpr explicit
        SocketConnection(int sockfd, Base::IPAddress address)
                : Connection(sockfd, address) {
            // Initially, we know that the protocol is HTTP/1.1. Any other
            // protocol is switched to in Upgrade or, when the HTTP version
            // was found to be lower than 1.1, downgraded.
            applicationProtocol = Base::ApplicationProtocol::HTTP_V1_1;

            // Because we know the application protocol is HTTP/1.1, we can put
            // the ioState to WANT_READ, because the client sends us stuff
            // before we tell them even anything.
            ioState = IOState::WANT_READ;
        }

        [[nodiscard]] bool
        Setup() noexcept override {
            return true;
        }

        [[nodiscard]] char
        ReadChar() override {
            char character;
            ssize_t ret = read(m_socket, &character, 1);

            if (ret == -1) {
                if (errno == EAGAIN
#if EWOULDBLOCK != EAGAIN
                    || errno == EWOULDBLOCK
#endif
                ) {
                    ioState = IOState::WANT_READ;
                    return AsyncInterruptCharacter;
                }

                perror("what errno ReadChar");
                throw SocketConnectionException("unknown error in read char");
            }

            if (ret == 0) {
                ioState = IOState::DEAD;
                return AsyncInterruptCharacter;
            }

            return character;
        }

        std::size_t
        ReadBlock(char *data, std::size_t length) override {
            std::size_t written{0};

            do {
                ssize_t ret = read(m_socket, data + written, length);
                if (ret == -1)
                    throw SocketConnectionException("error in read block");
                if (ret == 0)
                    throw SocketConnectionException("eof in read block");

                length -= static_cast<std::size_t>(ret);
                written += static_cast<std::size_t>(ret);
            } while (length > 0);

            return written;
        }

        std::size_t
        Write(const char *data, std::size_t dataLength) override {
            do {
                ssize_t ret = write(m_socket, data, dataLength);

                if (ret == -1) {
                    if (errno == EAGAIN
#if EWOULDBLOCK != EAGAIN
                        || errno == EWOULDBLOCK
#endif
                    ) {
                        ioState = IOState::WANT_WRITE;
                        return dataLength;
                    }
                    throw SocketConnectionException("error in write");
                }

                if (ret == 0)
                    throw SocketConnectionException("EOF in write");

                data += ret;
                dataLength -= static_cast<std::size_t>(ret);
            } while (dataLength > 0);

            return 0;
        }

        /**
         * TODO the implementation of this function sucks.
         */
        [[nodiscard]] std::size_t
        SendFile(int fd, std::size_t size) override {
#ifdef BASE_PLATFORM_LINUX
            do {
                ssize_t ret = sendfile(m_socket, fd, nullptr, size);
                if (ret <= 0) {
                    throw SocketConnectionException("error in sendfile(linux)");
                }
#elif defined(__FreeBSD__)
            do {
                int ret = sendfile(fd, this->sockfd, 0, size, nullptr, nullptr, 0);
                if (ret <= 0) {
                    throw SocketConnectionException("error in sendfile(FreeBSD)");
                }
#else
#pragma message "SendFile doesn't use platform-specific sendfile syscall!"
            std::array<char, 1024> buf;
            do {
                const int ret = read(fd, buf.data(), buf.size());
                if (ret <= 0) {
                    throw SocketConnectionException("error in sendfile(generic-read)");
                }

                int writeCnt = 0;
                do {
                    int a = write(this->sockfd, buf.data(), ret);
                    if (a <= 0) {
                        throw SocketConnectionException("error in sendfile(generic-write)");
                    }
                    writeCnt -= a;
                } while (writeCnt != ret);
#endif
                size -= static_cast<std::size_t>(ret);
            } while (size > 0);

            return 0;
        }
    };

} // namespace Connection

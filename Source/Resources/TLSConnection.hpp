/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <array>
#include <type_traits>
#include <vector>

#include "Source/Base/Configuration.hpp"
#include "Source/Resources/Connection.hpp"
#include "Source/Resources/SSLImplementation.hpp"

namespace Resources {

    class TLSConnection : public Connection {
    private:
        Resources::TLSObjectType data;

        const bool printErrors{false};
        bool handshakeFinished{false};

        [[nodiscard]] bool
        DoSetup() noexcept;

    public:
        inline
        TLSConnection(Resources::TLSObjectType data,
                      int sockFD,
                      Base::IPAddress ipAddress,
                      bool printErrors)
#ifdef __clang__ // GCC fails with "constraints on a non-templated function"
                requires(std::is_trivially_copyable_v<Resources::TLSObjectType>)
#endif
                : Connection(sockFD, ipAddress)
                , data(data)
                , printErrors(printErrors) {
            static_cast<void>(DoSetup());
        }

        inline
        TLSConnection(Resources::TLSObjectType &&data,
                      int sockFD,
                      Base::IPAddress ipAddress,
                      bool printErrors)
#ifdef __clang__ // GCC fails with "constraints on a non-templated function"
                requires(!std::is_trivially_copyable_v<Resources::TLSObjectType>)
#endif
                : Connection(sockFD, ipAddress)
                , data(std::forward<Resources::TLSObjectType>(data))
                , printErrors(printErrors) {
            static_cast<void>(Setup());
        }

        ~TLSConnection() noexcept override;

        [[nodiscard]] bool
        Setup() noexcept override;

        [[nodiscard]] char
        ReadChar() override;

        [[nodiscard]] std::size_t
        ReadBlock(char *data, std::size_t length) override;

        /**
         * Tries to write 'dataLength' amount of 'data' to the connection. When
         * an  error occurs, ioState is set to DEAD and an exception is thrown.
         * When  the  write buffer is full, the function returns with amount of
         * data it hasn't written. E.g. dataLength minus the data written.
         *
         * @returns the amount of data that wasn't written.
         */
        [[nodiscard]] std::size_t
        Write(const char *data, std::size_t dataLength) override;

        /**
         * Tries to write 'dataLength' amount of 'data' to the connection. When
         * an  error occurs, ioState is set to DEAD and an exception is thrown.
         * When  the  write buffer is full, the function returns with amount of
         * data it hasn't written. E.g. dataLength minus the data written.
         *
         * @returns the amount of data that wasn't written.
         */
        [[nodiscard]] std::size_t
        SendFile(int, std::size_t) override;

    private:
        /**
         * Try to send the file using a sendfile function, which is faster than
         * reading from the file socket and then writing to the TCP socket.
         */
        [[nodiscard]] bool
        SendFileUsingSendFile(int, std::size_t);
    };

} // namespace Connection

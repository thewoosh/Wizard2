/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <exception>
#include <vector>

#include "Connection.hpp"

namespace Resources {

    class MockConnection : public Connection {
    public:
        const std::vector<char> input{};
        std::vector<char> output{};

        std::vector<char>::const_iterator inputIt;

        class MockConnectionException : public std::exception {
        private:
            const char *const info;

        public:
            explicit
            MockConnectionException(const char *info)
                : info(info) {
            }

            [[nodiscard]] const char *
            what() const noexcept override {
                return info;
            }
        };

        explicit
        MockConnection(std::vector<char> &&input)
            : input(std::move(input)), inputIt(std::cbegin(this->input)) {
        }


        [[nodiscard]] bool
        Setup() override {
            return true;
        }

        [[nodiscard]] char
        ReadChar() override {
            if (inputIt >= std::end(input)) {
                ioState = IOState::WANT_READ;
                return AsyncInterruptCharacter;
            }
            return *(inputIt++);
        }

        [[nodiscard]] inline std::size_t
        ReadBlock(char *data, std::size_t length) override {
            if (inputIt + length >= std::end(input)) {
                ioState = IOState::WANT_READ;
                return AsyncInterruptCharacter;
            }
            std::copy(inputIt, inputIt + length, data);
            inputIt += length;

            return 0;
        }

        [[nodiscard]] inline std::size_t
        Write(const char *data, std::size_t dataLength) override {
            // TODO pointer arithmetic
            output.insert(std::end(output), data, data + dataLength);
            return 0;
        }

        [[nodiscard]] inline std::size_t
        SendFile(SocketSSLGlue::SocketType fd, std::size_t size) override {
            // TODO use platform-respecting code
            output.reserve(output.size() + size);
            char *ptr = &(*std::end(output));

            do {
                ssize_t ret = read(fd, ptr, size);
                if (ret <= 0) {
                    throw MockConnectionException("SendFile (POSIX) error");
                }

                size -= ret;
                ptr += ret;
            } while (size != 0);

            return 0;
        }

        [[nodiscard]] Resources::SocketSSLGlue::SocketType
        GetSocket() override {
            return -1;
        }

    };

} // namespace Connection

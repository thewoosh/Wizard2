/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Provider/FileProvider.hpp"

#include <cassert>
#include <cstring>

#include <sys/stat.h>
#include <fcntl.h>

#include <fmt/format.h>

#include "Source/Base/DateTime.hpp"
#include "Source/Base/Globals.hpp"
#include "Source/Provider/MasterProvider.hpp"

namespace Provider {

    enum class FileError {
        UNSET,
        NO_ERROR,

        UNSAFE,

        // Related to open(2)
        OPEN_ACCESS_DENIED,
        OPEN_LOW_RESOURCES,
        OPEN_NOT_FOUND,
        OPEN_UNKNOWN_ERROR,

        STAT_FAILURE,
        NOT_A_FILE,

    };

    struct FileResult {

        int fd{};

        struct stat status{};

        std::string fileName{};

        FileError error{FileError::UNSET};
    };

    /**
     * Resolves the path with the directory, which will following . and ..
     * references and follow symbolic links.
     *
     * If the returning path is not a child of the actual directory -
     * e.g.
     * (1) path is "../../../../../../../../etc/shadow"
     * (2) path encounters symbolic link pointing to outside the directory. if
     *     such behavior is desired, a better provider structure is ought to be
     *     set up.
     * - this function will return false.
     *
     * Otherwise returns true.
     */
    [[nodiscard]] bool
    IsSafePath(const std::string &constructedPath,
               const std::string &expectedParentDirectory) noexcept {
        std::array<char, PATH_MAX> buffer{};
        static_cast<void>(realpath(constructedPath.c_str(), buffer.data()));

        std::size_t compareRange = expectedParentDirectory.size();
        if (expectedParentDirectory.length() - strlen(buffer.data()) == 1 &&
                expectedParentDirectory.back() == '/') {
            // realpath() doesn't include the trailing slash '/' when returning
            // a directory, so we should account for that.
            // whether or not the path is a directory isn't decided by this
            // function, and valid file paths that are directories aren't
            // unsafe as per this function.
            compareRange -= 1;
        }

        return std::string_view(buffer.data(), compareRange) == std::string_view(expectedParentDirectory.data(), compareRange);
    }

    /**
     * Returns a negative integer if the file couldn't be opened, otherwise the
     * file descriptor. If the return value is an file descriptor the status
     * struct is filled in, otherwise is undefined.
     */
    [[nodiscard]] FileResult
    TryLoadFile(const std::string &path) noexcept {
        FileResult result{};

        result.fd = open(path.c_str(), O_RDONLY);
        if (result.fd == -1) {
            switch (errno) {
                case EACCES:
                case EPERM:
                    result.error = FileError::OPEN_ACCESS_DENIED;
                    break;
                case ENOENT:
                // hide special files for security:
                case ENOTDIR:
                case ENXIO:
                    result.error = FileError::OPEN_NOT_FOUND;
                    break;
                case EDQUOT:
                case EMFILE:
                case ENFILE:
                case ENOMEM:
                    result.error = FileError::OPEN_LOW_RESOURCES;
                    break;
                default:
                    result.error = FileError::OPEN_UNKNOWN_ERROR;
                    break;
            }

            return result;
        }

        if (fstat(result.fd, &result.status) == -1) {
            close(result.fd);
            result.fd = -1;
            result.error = FileError::STAT_FAILURE;
            return result;
        }

        mode_t fileType = result.status.st_mode & S_IFMT;
        if (fileType != S_IFREG) {
            // TODO decide whether or not to allow symlinks
            close(result.fd);
            result.fd = -1;
            result.error = FileError::NOT_A_FILE;
            return result;
        }

        result.error = FileError::NO_ERROR;
        return result;
    }

    /**
     * Find a file with the specified path.
     *
     * If the directory + path is a directory, the function will look for an
     * index.html file.
     *
     * If no file at the path can be found and the paragraph above doesn't
     * compute, -1 is returned meaning FileNotFound.
     */
    [[nodiscard]] FileResult
    ConstructFile(const std::string &directory, std::string_view path) {
        auto proposedString = fmt::format(fmt::runtime(directory.back() == '/' ? "{}{}" : "{}/{}"), directory, path);

        //
        // WARNING
        //
        // We check the first constructed path to be safe, since we expect the
        // code path hereinafter is only diving further in the directory tree.
        // If at any point the code path goes up the tree, we should check the
        // path again.
        //
        // This should be tested in the unit test.
        //

        if (Base::Globals::configuration->securitySettings.verifyPathUsingRealpath &&
                !IsSafePath(proposedString, directory)) {
            return {-1, {}, std::move(proposedString), FileError::UNSAFE};
        }

        FileResult result = TryLoadFile(proposedString);
        if (result.error == FileError::NO_ERROR) {
            result.fileName = std::move(proposedString);
            return result;
        }

        // check if the file was a directory
        if (result.status.st_mode != 0 && (result.status.st_mode & S_IFMT) == S_IFDIR) {
            if (proposedString.back() == '/')
                proposedString += "index.html";
            else
                proposedString += "/index.html";
            FileResult indexResult = TryLoadFile(proposedString);

            if (indexResult.error == FileError::NO_ERROR) {
                indexResult.fileName = std::move(proposedString);
                return indexResult;
            }
        }

        result.fileName = std::move(proposedString);
        return result;
    }

    template<std::uint8_t A, std::uint8_t B, std::uint8_t C>
    [[nodiscard]] HandleResult
    HandleError(MasterProvider *masterProvider,
                HTTP::AbstractClient *client,
                std::string &&reasonPhrase,
                std::string_view message) {
        auto &response = client->GetResponse();
        response.SetStatusCode<A, B, C>();
        response.reasonPhrase = std::move(reasonPhrase);
        response.version.major = 1;
        response.version.minor = 1;

        response.headerList.Add("Content-Length", std::to_string(message.size()));
        response.headerList.Add("Content-Type", "text/plain;charset=utf-8");
        response.headerList.Add("Cache-Control", "max-age=0");

        if (!masterProvider->FinalizeResponseMetadata(client))
            return HandleResult::FAILED_SENDING_METADATA;
        if (!client->SendResponseMetadata())
            return HandleResult::FAILED_SENDING_METADATA;
        if (!client->SendResponseString(message))
            return HandleResult::FAILED_SENDING_BODY;

        return HandleResult::OK;
    }

    [[nodiscard]] HandleResult
    Send405MethodNotAllowed(MasterProvider *masterProvider, HTTP::AbstractClient *client) {
        auto &response = client->GetResponse();
        response.SetStatusCode<4, 0, 5>();
        response.reasonPhrase = "Method Not Allowed";
        response.version.major = 1;
        response.version.minor = 1;

        std::string_view message = "Method Not Allowed";

        response.headerList.Add("Content-Length", std::to_string(message.size()));
        response.headerList.Add("Allow", "GET, HEAD");

        if (!masterProvider->FinalizeResponseMetadata(client))
            return HandleResult::FAILED_SENDING_METADATA;

        if (!client->SendResponseMetadata())
            return HandleResult::FAILED_SENDING_METADATA;

        if (!client->SendResponseString(message))
            return HandleResult::FAILED_SENDING_BODY;

        return HandleResult::OK;
    }

    HandleResult
    FileProvider::HandleRequest(HTTP::AbstractClient *client) noexcept {
        const auto &request = client->GetRequest();

        bool isGetRequest = request.method == "GET";
        bool isHeadRequest = !isGetRequest && request.method == "HEAD";

        if (!isGetRequest && !isHeadRequest) {
            return Send405MethodNotAllowed(m_masterProvider, client);
        }

        const FileResult result = ConstructFile(this->m_directory, request.path);

        if (result.error != FileError::NO_ERROR) {
            switch (result.error) {
                case FileError::OPEN_NOT_FOUND:
                case FileError::NOT_A_FILE:
                    return HandleError<4, 0, 4>(m_masterProvider, client, "Not Found", fmt::format("File wasn't found: {}", request.path));
                case FileError::OPEN_LOW_RESOURCES:
                    return HandleError<5, 0, 3>(m_masterProvider, client, "Service Unavailable", "Service Unavailable");
                case FileError::OPEN_ACCESS_DENIED:
                case FileError::UNSAFE:
                    return HandleError<4, 0, 3>(m_masterProvider, client, "Forbidden", "Forbidden");
                case FileError::STAT_FAILURE:
                default:
                    return HandleError<5, 0, 0>(m_masterProvider, client, "Internal Server Error", "Internal Server Error");
            }
        }

        std::string lastModifiedContent = Base::DateTime::CreateHTTPDate(result.status.st_mtim.tv_sec);

        const auto &ifModifiedSinceHeader = request.headerList["if-modified-since"];
        if (!std::empty(ifModifiedSinceHeader) &&
                ifModifiedSinceHeader == lastModifiedContent) {
            close(result.fd);

            auto &response = client->GetResponse();
            response.SetStatusCode<3, 0, 4>();
            response.reasonPhrase = "Not Modified";
            response.version.major = 1;
            response.version.minor = 1;

            if (!m_masterProvider->FinalizeResponseMetadata(client))
                return HandleResult::FAILED_SENDING_METADATA;

            if (!client->SendResponseMetadata())
                return HandleResult::FAILED_SENDING_METADATA;

            if (!client->SendEmptyResponse())
                return HandleResult::FAILED_SENDING_BODY;

            return HandleResult::OK;
        }

        auto &response = client->GetResponse();
        response.SetStatusCode<2, 0, 0>();
        response.reasonPhrase = "OK";
        response.version.major = 1;
        response.version.minor = 1;

        response.headerList.Add("Content-Length", std::to_string(result.status.st_size));
        response.headerList.Add("Last-Modified", lastModifiedContent);

        std::string fileType("application/octet-stream");

        auto mediaType = m_fileTypeDetector.DetectByFileName(result.fileName);
        if (!std::empty(mediaType.type) && !std::empty(mediaType.subtype)) {
            if (mediaType.includeCharset) {
                fileType = fmt::format("{}/{};charset=utf-8", mediaType.type, mediaType.subtype);
            } else {
                fileType = fmt::format("{}/{}", mediaType.type, mediaType.subtype);
            }
        }

        response.headerList.Add("Content-Type", std::move(fileType));

        if (!m_masterProvider->FinalizeResponseMetadata(client))
            return HandleResult::FAILED_SENDING_METADATA;

        if (!client->SendResponseMetadata())
            return HandleResult::FAILED_SENDING_METADATA;

        if (isHeadRequest) {
            close(result.fd);
            if (!client->SendEmptyResponse())
                return HandleResult::FAILED_SENDING_BODY;
        } else {
            assert(result.status.st_size > 0);
            if (!client->SendResponseFile(result.fd, static_cast<std::size_t>(result.status.st_size)))
                return HandleResult::FAILED_SENDING_BODY;
        }

        return HandleResult::OK;
    }

} // namespace Provider

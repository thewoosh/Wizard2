/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "MasterProvider.hpp"

#include <iostream>

#include "Source/Base/DateTime.hpp"
#include "Source/Provider/FileProvider.hpp"

namespace Provider {

    MasterProvider::MasterProvider(const Base::Configuration &configuration)
            : configuration(configuration) {
        providers.emplace_back("/", std::make_unique<FileProvider>(this, configuration.rootDirectory));
    }

    HandleResult
    MasterProvider::HandleRequest(HTTP::AbstractClient *client) noexcept {
        const ProviderEntry *chosenProvider{nullptr};

        const auto &request = client->GetRequest();

        for (const auto &entry : providers) {
            if (request.path.starts_with(entry.prefix)) {
                chosenProvider = &entry;
            }
        }

        if (chosenProvider == nullptr) {
            if (configuration.verboseFlag)
                std::cerr << "[MasterProvider] No chosen provider found with path=\"" << request.path << "\"!\n";
            return HandleResult::NO_PROVIDER_FOUND;
        }

        return chosenProvider->instance->HandleRequest(client);
    }

    bool
    MasterProvider::FinalizeResponseMetadata(HTTP::AbstractClient *client) noexcept {
        auto &response = client->GetResponse();

        //
        // HTTP version
        //
        response.version.major = 1;
        response.version.minor = 1;

        //
        // General Metadata
        //
        if (!configuration.httpSettings.server.empty()) {
            response.headerList.Add("Server", configuration.httpSettings.server);
        } else {
            response.headerList.Add("Server", "Wizard2");
        }

        response.headerList.Add("Date", Base::DateTime::CreateHTTPDate());

        // TODO detect whether or not the client has set the Connection header
        //      for better compliance
        response.headerList.Add("Connection", "keep-alive");

        //
        // Privacy Metadata
        //
        response.headerList.Add("Referrer-Policy", "no-referrer");

        if (!configuration.httpSettings.strictTransportSecurity.empty()) {
            response.headerList.Add("Strict-Transport-Security",
                    configuration.httpSettings.strictTransportSecurity);
        }

        return true;
    }

} // namespace Provider

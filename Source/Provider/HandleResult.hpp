/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace Provider {

    enum class HandleResult {
        OK,

        FAILED_SENDING_METADATA,
        FAILED_SENDING_BODY,

        NO_PROVIDER_FOUND,
    };

    [[nodiscard]] inline constexpr std::string_view
    ToString(HandleResult handleResult) noexcept {
        switch (handleResult) {
            case HandleResult::OK: return "ok";

            case HandleResult::FAILED_SENDING_METADATA: return "failed-sending-metadata";
            case HandleResult::FAILED_SENDING_BODY: return "failed-sending-body";

            case HandleResult::NO_PROVIDER_FOUND: return "no-provider-found";

            default: return "(invalid)";
        }
    }

} // namespace Provider

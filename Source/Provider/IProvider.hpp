/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * The provider provides a response, which it gets from a generator, the file
 * system, CGI, etc.
 */

#pragma once

#include "Source/HTTP/AbstractClient.hpp"
#include "Source/HTTP/Request.hpp"
#include "Source/Provider/HandleResult.hpp"

namespace Provider {

    class IProvider {
    public:
        [[nodiscard]] virtual HandleResult
        HandleRequest(HTTP::AbstractClient *) noexcept = 0;

        virtual ~IProvider() = default;
    };

} // namespace Provider

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Content/FileTypeDetector.hpp"
#include "Source/Provider/IProvider.hpp"

#include <string>
#include <string_view>

namespace Provider {

    class MasterProvider;

    class FileProvider : public IProvider {
        MasterProvider *const m_masterProvider;
        const std::string m_directory;

        const Content::FileTypeDetector m_fileTypeDetector{};

    public:
        inline
        FileProvider(MasterProvider *masterProvider,
                     std::string &&directory)
                 : m_masterProvider(masterProvider)
                 , m_directory(std::move(directory)) {
        }

        inline
        FileProvider(MasterProvider *masterProvider,
                     std::string_view directory)
                : m_masterProvider(masterProvider)
                , m_directory(directory) {
        }

        [[nodiscard]] HandleResult
        HandleRequest(HTTP::AbstractClient *) noexcept override;
    };

}

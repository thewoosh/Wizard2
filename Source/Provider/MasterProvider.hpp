/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <map>
#include <memory>

#include "Source/Base/Configuration.hpp"
#include "Source/HTTP/AbstractClient.hpp"
#include "Source/HTTP/Request.hpp"
#include "Source/Provider/IProvider.hpp"

namespace Provider {

    class MasterProvider {
        struct ProviderEntry {
            std::string prefix;
            std::unique_ptr<IProvider> instance;

            [[nodiscard]] inline
            ProviderEntry(std::string prefix, std::unique_ptr<IProvider> instance) noexcept
                : prefix(std::move(prefix)), instance(std::move(instance)) {
            }
        };

        std::vector<ProviderEntry> providers{};

        const Base::Configuration &configuration;

    public:
        /**
         * TODO use configuration to load the providers
         */
        explicit
        MasterProvider(const Base::Configuration &);

        [[nodiscard]] HandleResult
        HandleRequest(HTTP::AbstractClient *) noexcept;

        [[nodiscard]] bool
        FinalizeResponseMetadata(HTTP::AbstractClient *) noexcept;
    };

} // namespace MasterProvider

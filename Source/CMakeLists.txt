add_executable(Wizard2 main.cpp)
add_sanitizers(Wizard2)

find_package(Threads REQUIRED)
target_link_libraries(Wizard2 Threads::Threads fmt::fmt)

#
# BaseLibrary
#
add_library(BaseLibrary OBJECT)
file(GLOB_RECURSE BaseResources Base/*.cpp)

foreach(BaseFile ${BaseResources})
    target_sources(BaseLibrary PRIVATE ${BaseFile})
endforeach()

#
# ContentLibrary
#
add_library(ContentLibrary OBJECT)
file(GLOB_RECURSE ContentFileList Content/*.cpp)

foreach(ContentFile ${ContentFileList})
    target_sources(ContentLibrary PRIVATE ${ContentFile})
endforeach()

#
# HTTPLibrary
#
add_library(HTTPLibrary OBJECT)
file(GLOB_RECURSE HTTPSources HTTP/*.cpp)

foreach(HTTPFile ${HTTPSources})
    target_sources(HTTPLibrary PRIVATE ${HTTPFile})
endforeach()

#
# ProviderLibrary
#
add_library(ProviderLibrary OBJECT)
file(GLOB_RECURSE ProviderSources Provider/*.cpp)

foreach(ProviderFile ${ProviderSources})
    target_sources(ProviderLibrary PRIVATE ${ProviderFile})
endforeach()

#
# ResourcesLibrary
#
add_library(ResourcesLibrary OBJECT)
file(GLOB_RECURSE ResourcesFile Resources/*.cpp)

foreach(ResourcesFile ${ResourcesFile})
    target_sources(ResourcesLibrary PRIVATE ${ResourcesFile})
endforeach()

#
# SecurityLibrary
#
add_library(SecurityLibrary OBJECT)
file(GLOB_RECURSE SecurityFile Security/*.cpp)

foreach(SecurityFile ${SecurityFile})
    target_sources(SecurityLibrary PRIVATE ${SecurityFile})
endforeach()

#
# Link the libraries to the main program
#
target_link_libraries(Wizard2 BaseLibrary ContentLibrary HTTPLibrary ProviderLibrary ResourcesLibrary SecurityLibrary)
target_link_libraries(Wizard2 ${TLS_LIBARIES})

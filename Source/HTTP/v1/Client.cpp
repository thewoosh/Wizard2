/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <iostream>

#include <cstring>

#include <fmt/format.h>

#include "Source/Base/Characters.hpp"
#include "Source/Base/Configuration.hpp"
#include "Source/Base/Globals.hpp"
#include "Source/Base/Platforms.hpp"
#include "Source/HTTP/v1/Client.hpp"
#include "Source/Provider/MasterProvider.hpp"

using namespace Base::Globals;

#ifdef BASE_PLATFORM_POSIX
#    include <syslog.h>
#endif // BASE_PLATFORM_POSIX

namespace HTTP::v1 {

    namespace StringReserves {
        // In regular requests, I've found that the biggest header name is
        // 'Upgrade-Insecure-Requests', which is 25 characters long.
        constexpr const std::size_t HeaderNameSize = 25;

        // Accepts headers are usually quite long, so a HTML accept header
        // value is usually around 80 characters long.
        constexpr const std::size_t HeaderValueSize = 80;
    }

    void Client::Continue() {
        bool inReadingState;

        while ((!(inReadingState = IsInReadingHandlingState()) &&
                    connection->ioState != Resources::IOState::DEAD) ||
                connection->ioState == Resources::IOState::READY) {
            char character{};

            if (inReadingState) {
                character = connection->ReadChar();

                if (character == Resources::Connection::AsyncInterruptCharacter
                    && connection->ioState != Resources::IOState::READY) {
                    return;
                }
            }

            try {
                switch (clientHandlingState) {
                    case ClientHandlingState::READING_METHOD:
                        ReadMethod(character);
                        break;
                    case ClientHandlingState::READING_REQUEST_TARGET:
                        ReadRequestTarget(character);
                        break;
                    case ClientHandlingState::READING_HTTP_VERSION:
                        ReadHTTPVersion(character);
                        break;
                    case ClientHandlingState::READING_HEADER_NAME:
                        ReadHeaderName(character);
                        break;
                    case ClientHandlingState::READING_HEADER_VALUE:
                        ReadHeaderValue(character);
                        break;
                    case ClientHandlingState::AFTER_HEADERS:
                        ProcessHeaders();
                        break;
                    case ClientHandlingState::READING_BODY:
                        ReadBody(character);
                        break;
                    case ClientHandlingState::VERIFY_REQUEST:
                        VerifyRequest();
                        break;
                    case ClientHandlingState::HANDLING_REQUEST:
                        HandleRequest();
                        break;
                    case ClientHandlingState::MALFORMED_REQUEST:
                        ProcessMalformedRequest();
                        break;
                    case ClientHandlingState::AFTER_REQUEST:
                        AfterRequest();
                        break;
                    case ClientHandlingState::ABORTING:
                        connection->ioState = Resources::IOState::DEAD;
                        return;
                    default:
                        std::cerr << "[WARN] Unhandled state: " << clientHandlingState << '\n';
                        break;
                }
            } catch (const ViolationException &exception) {
                HandleRaisedViolation(exception.violation);
            }
        }
    }

    void Client::ReadMethod(char data) {
        if (data == ' ') {
            clientHandlingState = ClientHandlingState::READING_REQUEST_TARGET;
            return;
        }

        if (!Characters::IsTChar(data)) {
            clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
            malformedRequestMotivation = MalformedRequestMotivation::INVALID_CHARACTERS_IN_METHOD;
            return;
        }

        request.method += data;
    }

    void Client::ReadRequestTarget(char data) {
        if (data == ' ') {
            clientHandlingState = ClientHandlingState::READING_HTTP_VERSION;
            return;
        }

        if (!Characters::IsVChar(data)) {
            clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
            malformedRequestMotivation = MalformedRequestMotivation::INVALID_CHARACTERS_IN_REQUEST_TARGET;
            return;
        }

        request.requestTarget += data;
    }


    void Client::ReadHTTPVersion(char data) {
        bool valid = false;
        switch (versionCounter++) {
            case 0: valid = data == 'H'; break;
            case 1: case 2: valid = data == 'T'; break;
            case 3: valid = data == 'P'; break;
            case 4: valid = data == '/'; break;
            case 5:
                valid = data >= '0' && data <= '9';
                request.version.major = Characters::ConvertCharacterToDigit(data);
                break;
            case 6: valid = data == '.'; break;
            case 7:
                valid = data >= '0' && data <= '9';
                request.version.minor = Characters::ConvertCharacterToDigit(data);
                break;
            case 8: valid = data == '\r'; break;
            case 9:
                valid = data == '\n';
                versionCounter = 0;
                break;
        }

        if (!valid) {
            clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
            if (versionCounter == 9 && data == '\n') {
                malformedRequestMotivation = MalformedRequestMotivation::INCORRECT_LINE_ENDING_AFTER_HTTP_VERSION;
            } else {
                malformedRequestMotivation = MalformedRequestMotivation::INVALID_CHARACTERS_IN_HTTP_VERSION;
            }
            return;
        }

        if (data == '\n') {
            clientHandlingState = ClientHandlingState::READING_HEADER_NAME;
            stringBuffer.reserve(StringReserves::HeaderNameSize);
        }
    }

    void Client::ReadHeaderName(char character) {
        if (stringBuffer.back() == '\r') {
            if (std::size(stringBuffer) == 1 && character == '\n') {
                stringBuffer.clear();
                clientHandlingState = ClientHandlingState::AFTER_HEADERS;
                return;
            }

            clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
            malformedRequestMotivation = MalformedRequestMotivation::UNEXPECTED_CARRIAGE_RETURN_IN_HEADER_NAME;
            return;
        }

        if (character == ':') {
            clientHandlingState = ClientHandlingState::READING_HEADER_VALUE;
            headerNameBuffer = std::move(stringBuffer);
            stringBuffer.reserve(StringReserves::HeaderValueSize);
            return;
        }

        if (!Characters::IsTChar(character) && character != '\r') {
            clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
            malformedRequestMotivation = MalformedRequestMotivation::INVALID_CHARACTER_IN_HEADER_NAME;
            return;
        }

        if (character >= 'A' && character <= 'Z') {
            // Make  header names lowercase internally by design, so the header
            // lookup is easier.
            stringBuffer += static_cast<char>(character + 0x20);
        } else {
            stringBuffer += character;
        }
    }

    void Client::ReadHeaderValue(char character) {
        bool isWhitespace = character == ' ' ||
                            character == '\t';

        if (stringBuffer.empty() && isWhitespace) {
            return;
        }

        bool previousWasCarriageReturn = !stringBuffer.empty() &&
                                         stringBuffer.back() == '\r';

        if (previousWasCarriageReturn) {
            if (character == '\n') {
                stringBuffer.pop_back(); // remove the carriage return
                request.headerList.Add(std::move(headerNameBuffer), std::move(stringBuffer));
                clientHandlingState = ClientHandlingState::READING_HEADER_NAME;
                stringBuffer.reserve(StringReserves::HeaderNameSize);
                return;
            }

            clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
            malformedRequestMotivation = MalformedRequestMotivation::UNEXPECTED_CARRIAGE_RETURN_IN_HEADER_VALUE;
            return;
        }

        if (!isWhitespace && !Characters::IsVChar(character) && character != '\r') {
            clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
            malformedRequestMotivation = MalformedRequestMotivation::INVALID_CHARACTER_IN_HEADER_VALUE;
            return;
        }

        stringBuffer += character;
    }

    // Currently, this function only parses the Content-Length header when its
    // available.
    void Client::ProcessHeaders() {
        std::string_view contentLength = request.headerList["content-length"];

        if (std::empty(contentLength)) {
            request.contentLength = 0;
        } else {
            try {
                request.contentLength = std::stoull(contentLength.data());
            } catch (...) {
                clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
                malformedRequestMotivation = MalformedRequestMotivation::INVALID_CONTENT_LENGTH_HEADER;
                return;
            }
        }

        if (request.contentLength == 0) {
            clientHandlingState = ClientHandlingState::VERIFY_REQUEST;
        } else {
            clientHandlingState = ClientHandlingState::READING_BODY;
        }
    }

    void Client::ReadBody(char) {
        clientHandlingState = ClientHandlingState::VERIFY_REQUEST;
    }

    void Client::VerifyRequest() {
        if (!configuration->httpSettings.skipHostValidation &&
                !std::empty(configuration->hostname)
                && !connection->IPAddress().IsLocalhost()) {
            if (!VerifyHostHeader()) {
                clientHandlingState = ClientHandlingState::MALFORMED_REQUEST;
                malformedRequestMotivation = MalformedRequestMotivation::INCORRECT_HOST_HEADER;
                return;
            }
        }

        // Verification is complete & successful, handle request
        clientHandlingState = ClientHandlingState::HANDLING_REQUEST;
    }

    void Client::HandleRequest() {
        request.path = ParseRequestTarget();

        const auto result = masterProvider->HandleRequest(this);
        if (result != Provider::HandleResult::OK) {
            std::cout << "[Client] MasterProvider returned \"" << Provider::ToString(result) << "\"\n";
            clientHandlingState = ClientHandlingState::ABORTING;
            return;
        }

        clientHandlingState = ClientHandlingState::AFTER_REQUEST;
    }

    std::size_t
    Client::CalculateResponseSize() const {
        constexpr std::size_t httpVersionLength = 8;
        constexpr std::size_t colonLength = 1;
        constexpr std::size_t spaceLength = 1;
        constexpr std::size_t crlfLength = 2;

        const std::size_t startLineLength =
                httpVersionLength + spaceLength +
                response.statusCode.size() + spaceLength +
                response.reasonPhrase.size();

        std::size_t headersLength = 0;

        const auto addHeader = [&headersLength] (const auto &header) mutable {
            return headersLength +=
                           header.name.length() +
                           colonLength + spaceLength +
                           header.value.length() +
                           crlfLength;
        };

        std::for_each(std::cbegin(response.headerList.strings),
                      std::cend(response.headerList.strings),
                      addHeader);

        std::for_each(std::cbegin(response.headerList.stringViews),
                      std::cend(response.headerList.stringViews),
                      addHeader);

        return startLineLength + headersLength + crlfLength;
    }

    void
    Client::FinishResponse() {
        if (!configuration->httpSettings.server.empty()) {
            response.headerList.Add("Server", configuration->httpSettings.server);
        }

        if (!configuration->httpSettings.strictTransportSecurity.empty()) {
            response.headerList.Add("Strict-Transport-Security", configuration->httpSettings.strictTransportSecurity);
        }
    }

    [[nodiscard]] std::string_view
    Client::ParseRequestTarget() {
        if (request.requestTarget.empty()) {
            return {};
        }

        if (request.requestTarget.starts_with('/')) {
            // origin form.

            if (!configuration->securitySettings.verifyPathUsingFolderEscapeDetection) {
                return request.requestTarget;
            }

            std::string_view sv{
                std::data(request.requestTarget) + 1,
                // omit first solidus                  ^^^
                std::size(request.requestTarget) - 1
            };

            std::size_t segmentStack{0};

            std::string_view segment{};
            bool doContinue{true};
            while (doContinue) {
                auto nextSolidus = sv.find('/');

                if (nextSolidus == std::string_view::npos) {
                    segment = sv;
                    doContinue = false;
                } else {
                    segment = sv.substr(0, nextSolidus);
                    sv = sv.substr(nextSolidus + 1);
                }

                if (segment == "..") {
                    if (segmentStack == 0) {
                        RaiseViolation(Security::Violation::PATH_NAME_ESCAPES_PWD);
                        return "";
                    }

                    --segmentStack;
                } else if (segment != ".") {
                    ++segmentStack;
                }
            }

            return request.requestTarget;
        }

        return "";
    }

    void Client::SendBadRequest(std::string_view reason) {
        response.SetStatusCode<4, 0, 0>();
        response.reasonPhrase = "Bad Request";
        response.headerList.Add("Content-Type", "text/plain;charset=us-ascii");

        FinishResponse();

        if (!SendResponseMetadata() || !SendResponseString(reason))
            clientHandlingState = ClientHandlingState::ABORTING;
    }

    [[nodiscard]] inline constexpr std::string_view
    getMalformedRequestMotivationDescription(MalformedRequestMotivation malformedRequestMotivation) noexcept {
        switch (malformedRequestMotivation) {
            case MalformedRequestMotivation::UNSPECIFIED:
                return "Motivation for bad request trigger is unspecified, sorry!";
            case MalformedRequestMotivation::INVALID_CHARACTERS_IN_METHOD:
                return "Invalid character(s) in method.";
            case MalformedRequestMotivation::INVALID_CHARACTERS_IN_REQUEST_TARGET:
                return "Invalid character(s) in request-target.";
            case MalformedRequestMotivation::INVALID_CHARACTERS_IN_HTTP_VERSION:
                return "Invalid character(s) in HTTP-version.";
            case MalformedRequestMotivation::INVALID_CHARACTER_IN_HEADER_NAME:
                return "Invalid character(s) in header field-name.";
            case MalformedRequestMotivation::INVALID_CHARACTER_IN_HEADER_VALUE:
                return "Invalid character(s) in header field-value.";
            case MalformedRequestMotivation::UNEXPECTED_CARRIAGE_RETURN_IN_HEADER_NAME:
                return "Unexpected U+000D CARRIAGE RETURN in header field-name.";
            case MalformedRequestMotivation::UNEXPECTED_CARRIAGE_RETURN_IN_HEADER_VALUE:
                return "Unexpected U+000D CARRIAGE RETURN in header field-value.";
            case MalformedRequestMotivation::MULTIPLE_CONTENT_LENGTH_HEADERS:
                return "Multiple \"Content-Length\" headers.";
            case MalformedRequestMotivation::INVALID_CONTENT_LENGTH_HEADER:
                return "Invalid \"Context-Length\" header.";
            case MalformedRequestMotivation::INCORRECT_HOST_HEADER:
                return "Invalid or incorrect Host header. \n"
                       "Validation is required per RFC 7230 Section 5.4";
            case MalformedRequestMotivation::INCORRECT_LINE_ENDING_AFTER_HTTP_VERSION:
                return "Incorrect line ending after HTTP-version! HTTP lines are \n"
                       "terminated with a CRLF, not just a single U+000A LINE FEED.\n"
                       "\n"
                       "https://httpwg.org/specs/rfc7230.html#request.line";
            default:
                return {};
        }
    }

    void Client::ProcessMalformedRequest() {
        switch (Base::Globals::configuration->httpSettings.errorExplanationLevel) {
            case Base::HTTPSettings::ExplainLevel::HIDE_ERROR:
                SendBadRequest("Malformed Request");
                break;
            case Base::HTTPSettings::ExplainLevel::SHOW_ERROR_NAME:
                SendBadRequest("Error Code: " + std::to_string(static_cast<std::size_t>(malformedRequestMotivation)));
                break;
            case Base::HTTPSettings::ExplainLevel::EVERYTHING: {
                const auto description = getMalformedRequestMotivationDescription(malformedRequestMotivation);

                if (std::empty(description)) {
                    if (configuration->verboseFlag)
                        std::cout << "Unknown error: " << static_cast<std::uint32_t>(malformedRequestMotivation) << '\n';
                    SendBadRequest("Motivation for bad request trigger is unknown: " +
                    std::to_string(static_cast<std::size_t>(malformedRequestMotivation)));
                } else {
                    SendBadRequest(description);
                }
            } break;
        }
        clientHandlingState = ClientHandlingState::AFTER_REQUEST;
    }

    bool
    Client::SendResponseFile(int fd, std::size_t size) noexcept {
        socketMessage.data = fd;
        socketMessage.size = size;
        return true;
    }

    bool
    Client::SendResponseMetadata() noexcept {
        metadataBuilder.Reserve(CalculateResponseSize());

        metadataBuilder
                .Append("HTTP/")
                .Append(Characters::ConvertDigitToCharacter(response.version.major))
                .Append('.')
                .Append(Characters::ConvertDigitToCharacter(response.version.minor))

                .Append(' ')
                .Append(response.statusCode[0]).Append(response.statusCode[1]).Append(response.statusCode[2])
                .Append(' ')
                .Append(response.reasonPhrase)
                .Append("\r\n");

        auto addHeader = [this] (const auto &header) mutable {
            metadataBuilder.Append(header.name).Append(": ").Append(header.value).Append("\r\n");
        };

        std::for_each(std::cbegin(response.headerList.strings),
                      std::cend(response.headerList.strings),
                      addHeader);

        std::for_each(std::cbegin(response.headerList.stringViews),
                      std::cend(response.headerList.stringViews),
                      addHeader);

        metadataBuilder.Append("\r\n");
        return true;
    }

    [[nodiscard]] bool
    Client::SendResponseString(std::string_view string) noexcept {
        messageStringBuffer = string;
        return true;
    }

    void Client::AfterRequest() {
        if (!metadataBuilder.Empty() && !ForwardMetadata()) {
            return;
        }

        if (!std::empty(messageStringBuffer) && !ForwardStringMessageBody()) {
            return;
        }

        if (socketMessage.data != 0 && !ForwardFileMessageBody()) {
            return;
        }

        if (!configuration->silentFlag) {
            RecordStatistics();
        }

        ResetConversation();

        // Malformed  request  handling  uses  this path to send an explanation
        // response.  This uses the same path, but instead of going to the next
        // request,  the  connection  is closed. This prevents the next request
        // being  parsed  from  garbled  data, since the parsing of the current
        // request may have been cut off in the middle.
        if (malformedRequestMotivation != MalformedRequestMotivation::UNSPECIFIED) {
            connection->ioState = Resources::IOState::DEAD;
            return;
        }

        if (this->securityViolation != Security::Violation::NONE) {
            connection->ioState = Resources::IOState::DEAD;
            return;
        }

        // We're ready to handle another request, so start with the method.
        clientHandlingState = ClientHandlingState::READING_METHOD;
    }

    bool Client::ForwardMetadata() {
        std::size_t leftToWrite = connection->Write(&*metadataBuilder.begin() + writePosition,
                                                    metadataBuilder.Size() - writePosition);

        if (leftToWrite != 0) {
            writePosition += leftToWrite;
            return false;
        }

        metadataBuilder = {};
        writePosition = 0;

        return true;
    }

    bool Client::ForwardStringMessageBody() {
        std::size_t leftToWrite = connection->Write(std::data(messageStringBuffer) + writePosition,
                                                    std::size(messageStringBuffer) - writePosition);

        if (leftToWrite != 0) {
            writePosition += leftToWrite;
            return false;
        }

        messageStringBuffer = {};
        writePosition = 0;

        return true;
    }

    bool Client::ForwardFileMessageBody() {
        std::size_t leftToWrite = connection->SendFile(socketMessage.data, socketMessage.size);
        if (leftToWrite != 0) {
            socketMessage.size -= leftToWrite;
            return false;
        }

        messageStringBuffer = {};
        writePosition = 0;
        socketMessage = {}; // destructor calls close()

        return true;
    }

    void Client::RecordStatistics() {
        if (!configuration->logRequests) {
            return;
        }

        const auto timePointNow = std::chrono::high_resolution_clock::now();

        auto duration = timePointNow - requestStartTimePoint;
        requestStartTimePoint = timePointNow;

        std::cout << "req> " << request.method << ' ';

        if (!std::empty(request.path))
            std::cout << '"' << request.path << '"';
        else if (!std::empty(request.requestTarget))
            std::cout << '"' << request.requestTarget << '"';
        else
            std::cout << "unknown-path";

        std::cout << " => "
                  << response.statusCode[0] << response.statusCode[1] << response.statusCode[2]
                  << " in " << std::chrono::duration_cast<std::chrono::microseconds>(duration).count() << "µs"
                  << '\n';
    }

    void Client::HandleRaisedViolation(Security::Violation inViolation) {
        if (inViolation == Security::Violation::NONE) {
#ifdef BASE_PLATFORM_POSIX
            syslog(LOG_DAEMON | LOG_WARNING,
                   "Security Violation with violation = NONE"
            );
#endif // BASE_PLATFORM_POSIX

            connection->ioState = Resources::IOState::DEAD;
            return;
        }

        if (securityViolation != Security::Violation::NONE) {
#ifdef BASE_PLATFORM_POSIX
            syslog(LOG_DAEMON | LOG_WARNING,
                   "Security Violation thrown whilst handling another violation! cur=%s, new=%s",
                   Security::TranslateViolationToString(this->securityViolation),
                   Security::TranslateViolationToString(inViolation)
            );
#endif // BASE_PLATFORM_POSIX

            connection->ioState = Resources::IOState::DEAD;
            return;
        }

        this->securityViolation = inViolation;
        response.SetStatusCode<4, 0, 0>();
        response.reasonPhrase = "Bad Request";

        messageStringBuffer = fmt::format("Security Violation: {}", securityViolation);

        FinishResponse();
        if (!SendResponseMetadata()) {
            clientHandlingState = ClientHandlingState::ABORTING;
        } else {
            clientHandlingState = ClientHandlingState::AFTER_REQUEST;
        }
    }

    void Client::ResetConversation() {
        versionCounter = 0;

        AbstractClient::ResetConversation();
    }

} // namespace HTTP

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <chrono>
#include <memory>
#include <utility>

#include "Source/HTTP/v1/ClientHandlingState.hpp"
#include "Source/HTTP/AbstractClient.hpp"
#include "Source/HTTP/Request.hpp"
#include "Source/HTTP/Response.hpp"
#include "Source/Resources/Connection.hpp"
#include "Source/Security/SecurityViolation.hpp"
#include "Source/Text/StringBuilder.hpp"

namespace HTTP::v1 {

    enum class MalformedRequestMotivation {
        UNSPECIFIED,
        INVALID_CHARACTERS_IN_METHOD,
        INVALID_CHARACTERS_IN_REQUEST_TARGET,
        INVALID_CHARACTERS_IN_HTTP_VERSION,

        INVALID_CHARACTER_IN_HEADER_NAME,
        INVALID_CHARACTER_IN_HEADER_VALUE,
        UNEXPECTED_CARRIAGE_RETURN_IN_HEADER_NAME,
        UNEXPECTED_CARRIAGE_RETURN_IN_HEADER_VALUE,

        MULTIPLE_CONTENT_LENGTH_HEADERS,
        INVALID_CONTENT_LENGTH_HEADER,

        INCORRECT_HOST_HEADER,

        INCORRECT_LINE_ENDING_AFTER_HTTP_VERSION,
    };

    struct SocketMessage {
        int data{};
        std::size_t size{};

        inline ~SocketMessage() noexcept {
            if (data > 0)
                close(data);
        }
    };

    struct ViolationException
            : public std::exception {

        const Security::Violation violation{};

        inline explicit
        ViolationException(Security::Violation violation) noexcept
                : violation(violation) {
        }

        [[nodiscard]] inline constexpr const char *
        what() const noexcept override {
            return Security::TranslateViolationToString(violation);
        }

    };

    class Client : public AbstractClient {
        Resources::Connection *connection;

        //
        // Sending Buffers
        //
        // TODO this design is kinda bad
        //
        std::size_t writePosition{0};
        Text::StringBuilder metadataBuilder{};

        std::string messageStringBuffer{};

        SocketMessage socketMessage{};

        // TODO
//        bool lastRequestWasMalformed{false};
//        bool lastRequestConnectionClose{false};

        ClientHandlingState clientHandlingState
            {ClientHandlingState::READING_METHOD};

        MalformedRequestMotivation malformedRequestMotivation
            {MalformedRequestMotivation::UNSPECIFIED};

        std::size_t versionCounter{0};
        std::string stringBuffer{};
        std::string headerNameBuffer{};

        std::chrono::time_point<std::chrono::high_resolution_clock> requestStartTimePoint;

        [[nodiscard]] constexpr inline bool
        IsInReadingHandlingState() const noexcept {
            return clientHandlingState == ClientHandlingState::READING_METHOD ||
                    clientHandlingState == ClientHandlingState::READING_REQUEST_TARGET ||
                    clientHandlingState == ClientHandlingState::READING_HTTP_VERSION ||
                    clientHandlingState == ClientHandlingState::READING_HEADER_NAME ||
                    clientHandlingState == ClientHandlingState::READING_HEADER_VALUE ||
                    clientHandlingState == ClientHandlingState::READING_BODY;
        }

        void ResetConversation() override;

    public:
        [[nodiscard]] inline explicit
        Client(Resources::Connection *connection)
                : connection(connection)
                , requestStartTimePoint(std::chrono::high_resolution_clock::now()) {
        }

        ~Client() override = default;

        void Continue() override;

    private:
        [[nodiscard]] std::size_t
        CalculateResponseSize() const;

        /**
         * Writes general information to the response.
         */
        void
        FinishResponse();

        [[nodiscard]] std::string_view
        ParseRequestTarget();

        void
        SendBadRequest(std::string_view reason);
        void ProcessMalformedRequest();

        void ReadMethod(char);
        void ReadRequestTarget(char);
        void ReadHTTPVersion(char);
        void ReadHeaderName(char);
        void ReadHeaderValue(char);

        void ProcessHeaders();

        void ReadBody(char);

        void VerifyRequest();

        void HandleRequest();

        void AfterRequest();
        [[nodiscard]] bool ForwardMetadata();
        [[nodiscard]] bool ForwardStringMessageBody();
        [[nodiscard]] bool ForwardFileMessageBody();
        void RecordStatistics();

    public:
        [[nodiscard]] bool
        SendResponseFile(int, std::size_t) noexcept override;

        [[nodiscard]] bool
        SendResponseMetadata() noexcept override;

        [[nodiscard]] bool
        SendResponseString(std::string_view) noexcept override;

    private:
        //
        // Security
        //
        Security::Violation securityViolation{};

        [[noreturn]] inline static void
        RaiseViolation(Security::Violation violation) {
            throw ViolationException(violation);
        }

        // Called by Continue() on a ViolationException
        // DO NOT CALL when the conditions for a violation is reached, use
        // RaiseViolation() instead.
        void HandleRaisedViolation(Security::Violation);
    };

} // namespace HTTP

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

namespace HTTP::v1 {

    enum class ClientHandlingState {
        READING_METHOD,
        READING_REQUEST_TARGET,
        READING_HTTP_VERSION,
        READING_HEADER_NAME,
        READING_HEADER_VALUE,
        AFTER_HEADERS,
        READING_BODY,

        VERIFY_REQUEST,
        HANDLING_REQUEST,
        AFTER_REQUEST,

        ABORTING,
        MALFORMED_REQUEST,
    };

    [[nodiscard]] constexpr inline const char *
    TranslateClientHandlingStateToString(ClientHandlingState clientHandlingState) noexcept {
        switch (clientHandlingState) {
            case ClientHandlingState::READING_METHOD: return "reading-method";
            case ClientHandlingState::READING_REQUEST_TARGET: return "reading-request-target";
            case ClientHandlingState::READING_HTTP_VERSION: return "reading-http-version";
            case ClientHandlingState::READING_HEADER_NAME: return "reading-header-name";
            case ClientHandlingState::READING_HEADER_VALUE: return "reading-header-value";
            case ClientHandlingState::AFTER_HEADERS: return "after-headers";
            case ClientHandlingState::READING_BODY: return "reading-body";

            case ClientHandlingState::VERIFY_REQUEST: return "verify-request";
            case ClientHandlingState::HANDLING_REQUEST: return "handling-request";
            case ClientHandlingState::AFTER_REQUEST: return "after-request";

            case ClientHandlingState::ABORTING: return "aborting";
            case ClientHandlingState::MALFORMED_REQUEST: return "malformed-request";

            default: return "illegal-value";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, ClientHandlingState clientHandlingState) {
        stream << TranslateClientHandlingStateToString(clientHandlingState);
        return stream;
    }

} // namespace HTTP::v1

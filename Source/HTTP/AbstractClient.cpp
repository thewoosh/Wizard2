/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/HTTP/AbstractClient.hpp"

#include "Source/Base/Configuration.hpp"
#include "Source/Base/Globals.hpp"

namespace HTTP {

    void AbstractClient::ResetConversation() {
        request.requestTarget.clear();
        request.method.clear();
        request.version.major = 0;
        request.version.minor = 0;
        request.headerList.Clear();
        request.messageBody.clear();

        request.path = {};

        response.headerList.Clear();
        response.messageBody.clear();
    }

    bool AbstractClient::VerifyHostHeader() const noexcept {
        std::string_view hostHeader = request.headerList["host"];

        if (std::empty(hostHeader)) {
            return Base::Globals::configuration->httpSettings.allowMissingHostHeader;
        }

        auto colonPosition = hostHeader.find(':');

        if (colonPosition != std::string_view::npos) {
            hostHeader = hostHeader.substr(0, colonPosition);
        }

        return hostHeader == Base::Globals::configuration->hostname;
    }

} // namespace HTTP

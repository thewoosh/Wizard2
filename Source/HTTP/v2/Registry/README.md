# Registry
This registry contains IANA-registered parameters that apply to HTTP/2.

[IANA HTTP/2 Parameters](https://www.iana.org/assignments/http2-parameters/http2-parameters.xhtml)

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace HTTP::v2::Registry {

    // https://www.iana.org/assignments/http2-parameters/http2-parameters.xhtml#frame-type
    enum class FrameType {
        DATA,
        HEADERS,
        PRIORITY,
        RST_STREAM,
        SETTINGS,
        PUSH_PROMISE,
        PING,
        GOAWAY,
        WINDOW_UPDATE,
        CONTINUATION,
        ALTSVC,
        // 0xb is unassigned
        ORIGIN = 0xc,
    };

    template<typename T>
    [[nodiscard]] constexpr inline FrameType
    ConvertFrameType(T type) noexcept {
        return static_cast<FrameType>(type);
    }

    [[nodiscard]] constexpr inline std::uint8_t
    ConvertFrameType(FrameType type) noexcept {
        return static_cast<std::uint8_t>(type);
    }

    [[nodiscard]] constexpr inline const char *
    TranslateFrameTypeToString(FrameType frameType) noexcept {
        switch (frameType) {
            case FrameType::DATA: return "DATA";
            case FrameType::HEADERS: return "HEADERS";
            case FrameType::PRIORITY: return "PRIORITY";
            case FrameType::RST_STREAM: return "RST_STREAM";
            case FrameType::SETTINGS: return "SETTINGS";
            case FrameType::PUSH_PROMISE: return "PUSH_PROMISE";
            case FrameType::PING: return "PING";
            case FrameType::GOAWAY: return "GOAWAY";
            case FrameType::WINDOW_UPDATE: return "WINDOW_UPDATE";
            case FrameType::CONTINUATION: return "CONTINUATION";
            case FrameType::ALTSVC: return "ALTSVC";
            case FrameType::ORIGIN: return "ORIGIN";
            default:
                break;
        }

        std::uint8_t type = ConvertFrameType(frameType);

        if (type == 0xb || (type >= 0xd && type <= 0xef))
            return "unassigned";

        return "ReservedForExperimentalUse";
    }

    inline std::ostream &
    operator<<(std::ostream &stream, FrameType frameType) {
        stream << TranslateFrameTypeToString(frameType);
        return stream;
    }

} // namespace HTTP::v2::Registry

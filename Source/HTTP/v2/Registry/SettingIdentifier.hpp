/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace HTTP::v2::Registry {

    // https://www.iana.org/assignments/http2-parameters/http2-parameters.xhtml#settings
    enum class SettingIdentifier {
        /* 0x0 is reserved */
        HEADER_TABLE_SIZE = 0x1,
        ENABLE_PUSH,
        MAX_CONCURRENT_STREAMS,
        INITIAL_WINDOW_SIZE,
        MAX_FRAME_SIZE,
        MAX_HEADER_LIST_SIZE,
    };

    [[nodiscard]] constexpr inline SettingIdentifier
    ConvertSettingIdentifier(std::uint16_t type) noexcept {
        return static_cast<SettingIdentifier>(type);
    }

    [[nodiscard]] constexpr inline std::uint16_t
    ConvertSettingIdentifier(SettingIdentifier type) noexcept {
        return static_cast<std::uint16_t>(type);
    }

} // namespace HTTP::v2::Registry

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

namespace HTTP::v2::Registry {

    // https://www.iana.org/assignments/http2-parameters/http2-parameters.xhtml#error-code
    enum class H2ErrorCode {
        NO_ERROR,
        PROTOCOL_ERROR,
        INTERNAL_ERROR,
        FLOW_CONTROL_ERROR,
        SETTINGS_TIMEOUT,
        STREAM_CLOSED,
        FRAME_SIZE_ERROR,
        REFUSED_STREAM,
        CANCEL,
        COMPRESSION_ERROR,
        CONNECT_ERROR,
        ENHANCE_YOUR_CALM,
        INADEQUATE_SECURITY,
        HTTP_1_1_REQUIRED
    };

    [[nodiscard]] constexpr inline H2ErrorCode
    ConvertH2ErrorCode(std::uint32_t type) noexcept {
        return static_cast<H2ErrorCode>(type);
    }

    [[nodiscard]] constexpr inline std::uint32_t
    ConvertH2ErrorCode(H2ErrorCode errorCode) noexcept {
        return static_cast<std::uint32_t>(errorCode);
    }

    [[nodiscard]] constexpr inline const char *
    TranslateH2ErrorCodeToString(H2ErrorCode errorCode) noexcept {
        switch (errorCode) {
            case H2ErrorCode::NO_ERROR: return "NO_ERROR";
            case H2ErrorCode::PROTOCOL_ERROR: return "PROTOCOL_ERROR";
            case H2ErrorCode::INTERNAL_ERROR: return "INTERNAL_ERROR";
            case H2ErrorCode::FLOW_CONTROL_ERROR: return "FLOW_CONTROL_ERROR";
            case H2ErrorCode::SETTINGS_TIMEOUT: return "SETTINGS_TIMEOUT";
            case H2ErrorCode::STREAM_CLOSED: return "STREAM_CLOSED";
            case H2ErrorCode::FRAME_SIZE_ERROR: return "FRAME_SIZE_ERROR";
            case H2ErrorCode::REFUSED_STREAM: return "REFUSED_STREAM";
            case H2ErrorCode::CANCEL: return "CANCEL";
            case H2ErrorCode::COMPRESSION_ERROR: return "COMPRESSION_ERROR";
            case H2ErrorCode::CONNECT_ERROR: return "CONNECT_ERROR";
            case H2ErrorCode::ENHANCE_YOUR_CALM: return "ENHANCE_YOUR_CALM";
            case H2ErrorCode::INADEQUATE_SECURITY: return "INADEQUATE_SECURITY";
            case H2ErrorCode::HTTP_1_1_REQUIRED: return "HTTP_1_1_REQUIRED";
            default:
                break;
        }

        return "unassigned";
    }

    inline std::ostream &
    operator<<(std::ostream &stream, H2ErrorCode errorCode) {
        stream << TranslateH2ErrorCodeToString(errorCode);
        return stream;
    }

} // namespace HTTP::v2::Registry

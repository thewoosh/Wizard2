/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

namespace HTTP::v2::Registry::Limits {

    constexpr std::uint32_t maximumFlowControlWindowSize = 2147483647; // (2 ^ 31) - 1

    constexpr std::uint32_t minimumFrameSizeSetting = 16384; // 2^14
    constexpr std::uint32_t maximumFrameSizeSetting = 16'777'215; // 2^24 - 1

} // namespace HTTP::v2::Registry::Limits

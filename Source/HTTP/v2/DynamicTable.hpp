/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <iostream>
#include <list>
#include <memory>
#include <numeric>
#include <string>

#include "Source/Base/DoNothing.hpp"
#include "Source/Base/Portability.hpp"

#if defined (__GNUC__) && !defined (__clang__)
#   define H2DBG_DYNAMIC_TABLE_PRINT(...) DoNothing(__VA_ARGS__)
#else
#   define H2DBG_DYNAMIC_TABLE_PRINT(...) 0
#endif

namespace HTTP::v2 {

    /**
     * We could possibly optimize the DynamicTable usage. Since the entries are
     * at least used once, we can share the data between the Client::HeaderList
     * and the DynamicTable::Entry using reference counting (shared_ptr).
     *
     * Using simple techniques such as std::string_view to reference objects in
     * the  dynamic  table doesn't work, since evictions later in the very same
     * request  might remove them from the table, leaving a dangling pointer in
     * the header list.
     *
     * Using a shared_ptr would require a third vector in the HeaderList, which
     * results in more memory, and architectural changes in both  HTTPv1 and v2
     * clients.
     */
    struct DynamicTable {
        struct Entry {
            std::shared_ptr<std::string> name;
            std::shared_ptr<std::string> value;

            [[nodiscard]] inline
            Entry() noexcept
                    : name(nullptr)
                    , value(nullptr) {
            }

            [[nodiscard]] inline
            Entry(std::shared_ptr<std::string> &&name,
                  std::shared_ptr<std::string> &&value) noexcept
                    : name(std::move(name))
                    , value(std::move(value)) {
            }

        };

        std::size_t maxSize{4096};

        std::deque<Entry> storage{};

        /**
         * Returns the size of the header list. This is different from the
         * count. This function returns the length of the strings and
         * string_views inside the header list.
         */
        [[nodiscard]] inline std::size_t
        Size() const noexcept {
            H2DBG_DYNAMIC_TABLE_PRINT("[DynamicTable] Size() invocation\n");

            const auto sum = [](std::size_t value, const auto &header) {
                return value + std::size(*header.name) + std::size(*header.value);
            };

            const auto accumulate = [=](const auto &container) {
                return std::accumulate<decltype(std::cbegin(container)), std::size_t>(
                        std::cbegin(container), std::end(container), 0, sum);
            };

            return accumulate(storage);
        }

        /**
         * Possibly inserts the entry.
         *
         * RFC 7541 § 4.4 explains how to handle insertions, even when the
         * table is full, and/or the entry is bigger than the max size.
         * https://tools.ietf.org/html/rfc7541#section-4.4
         *
         */
        [[nodiscard]] inline const Entry &
        Insert(std::shared_ptr<std::string> &&name, std::shared_ptr<std::string> &&value) noexcept {
            H2DBG_DYNAMIC_TABLE_PRINT("[DynamicTable] Insert() invocation\n");

            const auto entrySize = std::size(*name) + std::size(*value);
            H2DBG_DYNAMIC_TABLE_PRINT("               entrySize=%zu maxSize=%zu\n", entrySize, maxSize);

            if (entrySize > maxSize) {
                H2DBG_DYNAMIC_TABLE_PRINT("               EntrySize>MaxSize thus storage cleared\n");
                storage.clear();
                static const Entry emptyEntry{};
                return emptyEntry;
            }

            while (Size() + entrySize > maxSize) {
                H2DBG_DYNAMIC_TABLE_PRINT("               Popping back \"%s\" = \"%s\"\n", storage.back().name, storage.back().value);
                storage.pop_back();
                H2DBG_DYNAMIC_TABLE_PRINT("               Element Popped (%zu left)\n", std::size(storage));
            }

            H2DBG_DYNAMIC_TABLE_PRINT("               Element \"%s\" = \"%s\" emplaced at position %zu\n", *name, *value, std::size(storage));
            return storage.emplace_back(std::move(name), std::move(value));
        }

        [[nodiscard]] inline const Entry &
        operator[](std::size_t index) const {
            return storage[std::size(storage) - 1 - index];
        }
    };

} // namespace HTTP::v2

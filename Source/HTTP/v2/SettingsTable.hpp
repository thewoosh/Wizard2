/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <limits>

#include <cstdint>

namespace HTTP::v2 {

    struct SettingsTable {
        constexpr static std::uint32_t Infinity = std::numeric_limits<std::uint32_t>::infinity();

        std::uint32_t headerTableSize{4096};
        bool enablePush{true};
        std::uint32_t maxConcurrentStreams{Infinity};
        std::uint32_t initialWindowSize{65535};
        std::uint32_t maxFrameSize{16384};
        std::uint32_t maxHeaderListSize{Infinity};
    };

} // namespace HTTP::v2

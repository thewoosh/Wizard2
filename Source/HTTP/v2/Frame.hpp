/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include <cstdint>

#include "Source/HTTP/v2/Registry/FrameType.hpp"

namespace HTTP::v2 {

    struct Frame {
        unsigned int length : 24;
        Registry::FrameType type;
        std::uint8_t flags;
        std::uint32_t streamID;
        std::vector<char> payload;

        [[nodiscard]] constexpr inline std::size_t
        Length() const noexcept {
            return length;
        }
    };

}

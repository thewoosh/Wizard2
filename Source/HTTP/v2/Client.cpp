/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/HTTP/v2/Client.hpp"

#include <algorithm>
#include <sstream>
#include <string_view>

#include <cassert>
#include <strings.h>

#include <fmt/format.h>

#include "Source/Base/Characters.hpp"
#include "Source/Base/DoNothing.hpp"
#include "Source/Base/Globals.hpp"
#include "Source/HTTP/v2/Huffman.hpp"
#include "Source/HTTP/v2/Registry/Limits.hpp"
#include "Source/HTTP/v2/Registry/SettingIdentifier.hpp"
#include "Source/HTTP/v2/StaticTable.hpp"

#if defined (__GNUC__) && !defined (__clang__)
#   define H2DBG_PRINTF_VERBOSE_INFO(...) DoNothing(__VA_ARGS__)
#   define WIZ_BEGIN_ASSIGN_LEN \
        #pragma GCC diagnostic push                   \
        #pragma GCC diagnostic ignored "-Wconversion" \
#   define WIZ_END_ASSIGN_LEN \
        #pragma GCC diagnostic pop

#   define ASSIGN_FRAME_LENGTH(dst, src) \
        WIZ_BEGIN_ASSIGN_LEN             \
        ((dst) = (src));                 \
        WIZ_END_ASSIGN_LEN

#else
#   define H2DBG_PRINTF_VERBOSE_INFO(...) 0
#   define ASSIGN_FRAME_LENGTH(dst, src) ((dst) = (src));
#endif

#define H2DBG_PRINTF_HEADER_PARSING(...) do {                                  \
    if (Base::Globals::configuration->http2Settings.debugPrintHeaderFieldTypes) \
        std::printf(__VA_ARGS__);                                                \
} while (0)
#define H2DBG_PRINTF_HEADER_PARSING_DETAILED(...) do {                           \
    if (Base::Globals::configuration->http2Settings.debugPrintHeaderFieldVerbose) \
        std::printf(__VA_ARGS__);                                                  \
} while (0)
//#define H2DBG_PRINTF_HEADER_PARSING(...) std::printf(__VA_ARGS__)

#define H2DBG_DO_PRINT_HEADERS       0
#define H2DBG_DO_PRINT_DYNAMIC_TABLE 0

using namespace std::string_literals;
using namespace std::string_view_literals;

namespace HTTP::v2 {

    void Client::Continue() {
        static_cast<void>(sentServerSettings);
        static_cast<void>(receivedClientSettings);

        if (connectionPrefaceCounter != 24) {
            ReadConnectionPreface();
        }

        if (connection->ioState == Resources::IOState::READY &&
                !sentServerSettings) {
            SendSettingsServer();
        }

        while (connection->ioState == Resources::IOState::READY) {
            TrySendingEnqueuedFrames();

            switch (clientHandlingState) {
                case ClientHandlingState::READING_METADATA:
                    ReceiveFrameMetadata();
                    break;
                case ClientHandlingState::READING_PAYLOAD:
                    ReceiveFramePayload();
                    break;
                case ClientHandlingState::HANDLING:
                    HandleFrame();
                    break;
                case ClientHandlingState::RESET_FRAME:
                    ResetFrame();
                    break;
                case ClientHandlingState::HANDLE_REQUEST:
                    ProcessRequest();
                    break;
                case ClientHandlingState::GOAWAY_ENQUEUED:
                    // Because we send the enqueued frames before entering this
                    // switch, we can return here and unregister/destroy the
                    // connection.
                    connection->ioState = Resources::IOState::DEAD;
                    return;
                default:
                    break;
            }
        }
    }

    void Client::ReadConnectionPreface() {
        const char prefaceString[] = "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n";

        for (; connectionPrefaceCounter < 24; ++connectionPrefaceCounter) {
            char character = connection->ReadChar();

            if (character == Resources::Connection::AsyncInterruptCharacter &&
                    connection->ioState != Resources::IOState::READY) {
                return;
            }

            if (character != prefaceString[connectionPrefaceCounter]) {
                Frame frame{};
                frame.streamID = 0x0;
                HandleError(frame, ErrorCode::INCORRECT_CONNECTION_PREFACE_STRING);
                return;
            }
        }

        H2DBG_PRINTF_VERBOSE_INFO("[h2] Connection preface OK\n");
    }

    void Client::ReceiveFrameMetadata() {
        std::size_t requestedDataIntake = std::size(frameMetadataBuffer) - currentFramePosition;
        const auto ret = connection->ReadBlock(frameMetadataBuffer.data() + currentFramePosition,
                                               requestedDataIntake);
        currentFramePosition += static_cast<std::uint32_t>(ret);
        if (ret != requestedDataIntake) {
            return;
        }

        // Convert the buffer to the frame
        ASSIGN_FRAME_LENGTH(currentReceivingFrame.length, (Base::Conversions::Construct<decltype(currentReceivingFrame.length), 3>(
                std::cbegin(frameMetadataBuffer))));

        currentReceivingFrame.type = Registry::ConvertFrameType(frameMetadataBuffer[3]);
        currentReceivingFrame.flags = static_cast<std::uint8_t>(frameMetadataBuffer[4]);
        currentReceivingFrame.streamID = Base::Conversions::Construct<decltype(currentReceivingFrame.length), 4>(
                std::cbegin(frameMetadataBuffer) + 5);

        lastStreamID = currentReceivingFrame.streamID;

        // Verify frame length
        if (currentReceivingFrame.Length() > serverSettingsTable.maxFrameSize) {
            currentFramePosition = 0;
            if (Base::Globals::configuration->verboseFlag)
                std::cout << "[h2] Illegal frame size for frame with type " << currentReceivingFrame.type << '\n';
            HandleError(currentReceivingFrame, ErrorCode::FRAME_SIZE_EXCEEDS_SETTING);
            return;
        }

        // Prepare for FrameReceivePayload()
        currentFramePosition = 0;
        currentReceivingFrame.payload.resize(currentReceivingFrame.length);
        clientHandlingState = ClientHandlingState::READING_PAYLOAD;
    }

    void Client::ReceiveFramePayload() {
        const std::size_t requestedDataIntake =
                currentReceivingFrame.length - currentFramePosition;

        std::size_t ret = connection->ReadBlock(
                &currentReceivingFrame.payload[currentFramePosition],
                requestedDataIntake);

        currentFramePosition += static_cast<std::uint32_t>(ret);

        if (ret != requestedDataIntake) {
            return;
        }

        currentFramePosition = 0;

        // switch to handling state
        clientHandlingState = ClientHandlingState::HANDLING;
    }

    void Client::HandleFrame() {
        if (Base::Globals::configuration->http2Settings.debugPrintIncomingFrames) {
            std::cout << "[h2] Handling Frame"
                      << " t=" << currentReceivingFrame.type << "(0x" << std::hex
                      << static_cast<std::uint16_t>(currentReceivingFrame.type) << std::dec << ')'
                      << " l=" << currentReceivingFrame.Length()
                      << " f=0x" << std::hex << static_cast<std::uint16_t>(currentReceivingFrame.flags)
                      << " s=0x" << currentReceivingFrame.streamID
                      << '\n';
        }

        if (!receivedClientSettings && currentReceivingFrame.type != Registry::FrameType::SETTINGS) {
            HandleError(currentReceivingFrame, ErrorCode::FIRST_FRAME_NOT_SETTINGS);
            return;
        }

        clientHandlingState = ClientHandlingState::RESET_FRAME;

        switch (currentReceivingFrame.type) {
            case Registry::FrameType::HEADERS:
                ProcessHeadersFrame();
                break;
            case Registry::FrameType::SETTINGS:
                ProcessSettingsFrame();
                break;
            case Registry::FrameType::PUSH_PROMISE:
                ProcessPushPromiseFrame();
                break;
            case Registry::FrameType::PING:
                ProcessPingFrame();
                break;
            case Registry::FrameType::GOAWAY:
                ProcessGoawayFrame();
                break;
            default:
                break;
        }
    }

    void Client::ResetFrame() {
        // currentReceivingFrame and receivingFrameBuffer don't have to be
        // reset, since the function ReceiveFrameMetadata() overwrites this
        // data anyway.

        // reset the frame counter, otherwise the ReceivingFrameMetadata() is
        // confused about where in the frame the function should operate.
        currentFramePosition = 0;

        // after reset, we can consume another frame again.
        clientHandlingState = ClientHandlingState::READING_METADATA;
    }

    void Client::HandleError(const Frame &frame, const ErrorInformation &info) {
        if (Base::Globals::configuration->http2Settings.debugPrintErrors)
            std::cout << "[h2] Handling error code " << info.errorCode << '\n';

        if (IsBadRequestError(info.errorCode)) {
            SendBadRequestResponse(info);
        }

        if (IsStreamError(info.errorCode)) {
            SendRstStream(frame.streamID, info.errorCode);
        } else {
            // it's a connection error
            std::string_view buffer = TranslateErrorCodeToString(info.errorCode);
            SendGoaway(TranslateErrorCodeToH2ErrorCode(info.errorCode), std::cbegin(buffer), std::cend(buffer));
        }
    }

    void Client::ProcessSettingsFrame() {
        this->receivedClientSettings = true;

        const Frame &frame = currentReceivingFrame;

        if (frame.streamID != 0) {
            HandleError(frame, ErrorCode::SETTINGS_NOT_STREAM_0);
            return;
        }

        if (frame.flags & 0x1) { // ACK
            ProcessSettingsFrameWithAckFlag(frame);
            return;
        }

        if (frame.Length() % 6 != 0) {
            HandleError(frame, ErrorCode::SETTINGS_INVALID_LENGTH);
            return;
        }

        if (frame.Length() != 0) {
            for (std::size_t i = 0; i < frame.length; i += 6) {
                const auto identifier = Base::Conversions::Construct<std::uint16_t, 2>(&frame.payload[i]);
                const auto value = Base::Conversions::Construct<std::uint32_t, 4>(&frame.payload[i + 2]);

                switch (Registry::ConvertSettingIdentifier(identifier)) {
                    case Registry::SettingIdentifier::HEADER_TABLE_SIZE:
                        // TODO maybe enforce a lower-limit for this value
                        clientSettingsTable.headerTableSize = value;
                        if (Base::Globals::configuration->http2Settings.debugPrintSettings)
                            std::cout << "[h2] (Settings) Client wants their HEADER_TABLE_SIZE to be " << value << '\n';
                        break;
                    case Registry::SettingIdentifier::ENABLE_PUSH:
                        if (value == 1) {
                            clientSettingsTable.enablePush = true;
                        } else if (value == 0) {
                            clientSettingsTable.enablePush = false;
                        } else {
                            HandleError(frame, ErrorCode::SETTINGS_PARAMETER_ENABLE_PUSH_NOT_BOOLEAN);
                            return;
                        }
                        if (Base::Globals::configuration->http2Settings.debugPrintSettings)
                            std::cout << "[h2] (Settings) Client wants their ENABLE_PUSH to be " << value << '\n';
                        break;
                    case Registry::SettingIdentifier::MAX_CONCURRENT_STREAMS:
                        // 0 is allowed. it means the server just cannot create
                        // new streams.
                        clientSettingsTable.maxConcurrentStreams = value;
                        if (Base::Globals::configuration->http2Settings.debugPrintSettings)
                            std::cout << "[h2] (Settings) Client wants their MAX_CONCURRENT_STREAMS to be " << value << '\n';
                        break;
                    case Registry::SettingIdentifier::INITIAL_WINDOW_SIZE:
                        if (value > Registry::Limits::maximumFlowControlWindowSize) {
                            HandleError(frame, ErrorCode::SETTINGS_PARAMETER_INITIAL_WINDOW_SIZE_EXCEEDS_MAXIMUM_FLOW_CONTROL_WINDOW_SIZE);
                            return;
                        }
                        clientSettingsTable.initialWindowSize = value;
                        if (Base::Globals::configuration->http2Settings.debugPrintSettings)
                            std::cout << "[h2] (Settings) Client wants their INITIAL_WINDOW_SIZE to be " << value << '\n';
                        break;
                    case Registry::SettingIdentifier::MAX_FRAME_SIZE:
                        if (value < Registry::Limits::minimumFrameSizeSetting) {
                            HandleError(frame, ErrorCode::SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_SMALL);
                            return;
                        }
                        if (value > Registry::Limits::maximumFrameSizeSetting) {
                            HandleError(frame, ErrorCode::SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_LARGE);
                            return;
                        }
                        clientSettingsTable.maxFrameSize = value;
                        if (Base::Globals::configuration->http2Settings.debugPrintSettings)
                            std::cout << "[h2] (Settings) Client wants their MAX_FRAME_SIZE to be " << value << '\n';
                        break;
                    case Registry::SettingIdentifier::MAX_HEADER_LIST_SIZE:
                        // No limits enforced, since the initial size is
                        // infinite anyway.
                        clientSettingsTable.maxHeaderListSize = value;
                        if (Base::Globals::configuration->http2Settings.debugPrintSettings)
                            std::cout << "[h2] (Settings) Client wants their MAX_HEADER_LIST_SIZE to be " << value << '\n';
                        break;
                    default:
                        // Unknown/unsupported settings identifier MUST be
                        // ignored. (i.e. don't trigger an error).
                        break;
                }
            }
        }

        SendSettingsWithAckFlag();
    }

    void Client::ProcessSettingsFrameWithAckFlag(const Frame &frame) {
        if (frame.Length() != 0) {
            HandleError(frame, ErrorCode::SETTINGS_ACK_NOT_LENGTH_ZERO);
            return;
        }

        if (!lastSentFrameNotYetAcknowledged) {
            HandleError(frame, ErrorCode::SETTINGS_ACK_WITHOUT_CORRESPONDING_FRAME);
            return;
        }

        lastSentFrameNotYetAcknowledged = false;
    }

    Frame &
    Client::CreateSendFrame(Registry::FrameType type) {
        if (Base::Globals::configuration->http2Settings.debugPrintOutgoingFrames)
            std::cout << "[h2] Create frame of type " << type << '\n';

        auto &frame = frameSendQueue.emplace_back();
        frame.type = type;
        return frame;
    }

    void Client::SendRstStream(std::uint32_t streamID, ErrorCode errorCode) {
        if (Base::Globals::configuration->http2Settings.debugPrintOutgoingErrorFrames)
            std::cout << "[h2] RST_STREAM errorCode=" << errorCode << '\n';

        auto &frame = CreateSendFrame(Registry::FrameType::RST_STREAM);
        frame.streamID = streamID;
        frame.length = 4;
        frame.payload.resize(4);

        const auto h2ErrorCode = TranslateErrorCodeToH2ErrorCode(errorCode);
        const auto integer = ConvertH2ErrorCode(h2ErrorCode);

        Base::Conversions::Destruct<4>(integer, std::begin(frame.payload));
    }

    void Client::SendSettingsServer() {
        sentServerSettings = true;

        // Send a zero'd frame with the settings type. Since we're not changing
        // the data of the frame, we're casting the return type to void.
        static_cast<void>(
                CreateSendFrame(Registry::FrameType::SETTINGS)
        );
    }

    void Client::SendSettingsWithAckFlag() {
        auto &frame = CreateSendFrame(Registry::FrameType::SETTINGS);
        frame.flags = 0x1; // ACK
        // other values are 0 initialized, which conveniently are the correct
        // values :)
    }

    template<std::uint8_t FirstOctetMask>
    std::size_t
    ParseHPACKInteger(const char *stream, size_t &updatedIndex) {
        constexpr std::size_t next_n_bits = FirstOctetMask-1;
        constexpr auto mask = static_cast<std::uint8_t>(~FirstOctetMask);
        std::size_t result = stream[0] & mask;

        if (result < next_n_bits) {
            updatedIndex += 1;
            return result;
        }

        std::size_t m = 0, b, position = 1;
        do {
            b = stream[position++] & 0xFF;
            result += (b & 127) * (1 << m);
            m += 7;
        } while ((b & 128) == 128);

        updatedIndex += position;
        return result;
    }

    void Client::ProcessHeadersFrame() {
        const Frame &frame = currentReceivingFrame;

        if (frame.streamID == 0) {
            constexpr std::string_view message{"HEADERS can't be on stream 0x0"};
            SendGoaway(Registry::H2ErrorCode::PROTOCOL_ERROR, std::begin(message), std::end(message));
            return;
        }

        if (!GetReadonlyStream(frame.streamID).AllowsHeaders()) {
            constexpr std::string_view message{"stream doesn't allow remote HEADERS"};
            SendGoaway(Registry::H2ErrorCode::PROTOCOL_ERROR, std::begin(message), std::end(message));
        }

        std::size_t headerBlockStart{0};
        std::size_t headerBlockEnd{frame.length};

        if (frame.flags & 0x8) {
            headerBlockStart += 1; // Pad Length
            // TODO check if padding is less then the payload size - PAD - E+SD - Weight
            headerBlockEnd -= static_cast<std::size_t>(frame.payload[0]);
        }

        if (frame.flags & 0x20) {
            headerBlockStart += 4; // E + Stream Dependency
            headerBlockStart += 1; // Weight
        }

        for (std::size_t i = headerBlockStart; i < headerBlockEnd;) {
            const auto octet = static_cast<std::uint8_t>(frame.payload[i]);

            H2DBG_PRINTF_HEADER_PARSING("[HPACK] #%zu (0x%hhx) ", i, octet);

            bool status{false};

            if (octet & 0x80) {
                H2DBG_PRINTF_HEADER_PARSING("Field Type: Indexed Header Field\n");
                status = ProcessHeadersIndexedHeaderField(i);
            } else if (octet & 0x40) {
                if (octet == 0x40) {
                    H2DBG_PRINTF_HEADER_PARSING("Field Type: Incremental Indexing -- New Name\n");
                    status = ProcessHeadersIncrementalIndexedHeaderFieldNewName(i);
                } else {
                    H2DBG_PRINTF_HEADER_PARSING("Field Type: Incremental Indexing -- Indexed Name\n");
                    status = ProcessHeadersIncrementalIndexedHeaderFieldIndexedName(i);
                }
            } else if (octet & 0x20) {
                H2DBG_PRINTF_HEADER_PARSING("Field Type: Dynamic Table Size Change\n");
                status = ProcessHeadersDynamicTableSizeChange(i);
            } else if (octet == 0x10) {
                H2DBG_PRINTF_HEADER_PARSING("Field Type: Never Indexed -- New Name\n");
                status = ProcessHeadersWithoutIndexingNewName(i);
            } else if (octet & 0x10) {
                H2DBG_PRINTF_HEADER_PARSING("Field Type: Never Indexed -- Indexed Name\n");
                status = ProcessHeadersWithoutIndexingIndexedName(i);
            } else if (octet == 0x00) {
                H2DBG_PRINTF_HEADER_PARSING("Field Type: Without Indexing -- New Name\n");
                status = ProcessHeadersWithoutIndexingNewName(i);
            } else {
                H2DBG_PRINTF_HEADER_PARSING("Field Type: Without Indexing -- Indexed Name\n");
                status = ProcessHeadersWithoutIndexingIndexedName(i);
            }

            if (!status) {
                std::cout << "[h2] Header field representation-specific failure occurred.\n";
                return;
            }
        }

#if H2DBG_DO_PRINT_HEADERS == 1
        for (const auto &entry : request.headerList.strings) {
            std::cout << "Header[Own] \"" << entry.name << "\" = \"" << entry.value << "\"\n";
        }

        for (const auto &entry : request.headerList.stringViews) {
            std::cout << "Header[Ref] \"" << entry.name << "\" = \"" << entry.value << "\"\n";
        }

        for (const auto &entry : request.headerList.sharedStrings) {
            std::cout << "Header[Shr] \"" << *entry.name << "\" = \"" << *entry.value << "\"\n";
        }
#endif

#if H2DBG_DO_PRINT_DYNAMIC_TABLE == 1
        std::cout << "DynamicTable has " << std::dec << std::size(dynamicTable.storage) << " entries.\n";
        for (std::size_t i = 0; i < std::size(dynamicTable.storage); ++i) {
            const auto &ent = dynamicTable[i];
            std::cout << "DynamicTable[Own, Pos=" << i << ", Refs=" << ent.name.use_count() << "] \"" << *ent.name
                      << "\" = \"" << *ent.value << "\"\n";
        }
#endif

        // TODO we should have a stage here where we read the DATA frame, when
        // expected to be present.
        clientHandlingState = ClientHandlingState::HANDLE_REQUEST;
    }

    bool Client::ProcessHeadersDynamicTableSizeChange(std::size_t &index) {
        const auto size = ParseHPACKInteger<0x20>(&currentReceivingFrame.payload[index], index);
        if (size > serverSettingsTable.headerTableSize) {
            return false;
        }

        H2DBG_PRINTF_HEADER_PARSING_DETAILED("    New size: %zu\n", size);
        return true;
    }

    bool Client::ProcessHeadersIndexedHeaderField(std::size_t &index) {
        if (static_cast<std::uint8_t>(currentReceivingFrame.payload[index]) == 0x80) {
            HandleError(currentReceivingFrame, ErrorCode::COMPRESSION_ILLEGAL_INDEX_0);
            return false;
        }

        std::size_t position = ParseHPACKInteger<0x80>(&currentReceivingFrame.payload[index], index);

        H2DBG_PRINTF_HEADER_PARSING_DETAILED("    Position: %zu\n", position);

        if (position >= std::size(StaticTable::table)) {
            position -= std::size(StaticTable::table);
            if (position > std::size(dynamicTable.storage)) {
                HandleError(currentReceivingFrame, ErrorCode::COMPRESSION_INDEX_OUTSIDE_INDEX_ADDRESS_SPACE);
                return false;
            }

            const auto &entry = dynamicTable[position];
            H2DBG_PRINTF_HEADER_PARSING_DETAILED("    HeaderName (Dynamic): \"%s\"\n", entry.name->c_str());
            H2DBG_PRINTF_HEADER_PARSING_DETAILED("    HeaderValue(Dynamic): \"%s\"\n", entry.value->c_str());

            // Have to make explicit copies to prevent string_views, which are
            // disallowed because of possibly entry eviction.
            request.headerList.AddShared(entry.name, entry.value);
            return true;
        }

        const auto &entry = StaticTable::table[position];

        H2DBG_PRINTF_HEADER_PARSING_DETAILED("    HeaderName (Static):  \"%s\"\n", std::data(entry.headerName));
        H2DBG_PRINTF_HEADER_PARSING_DETAILED("    HeaderValue(Static):  \"%s\"\n", std::data(entry.headerValue));
        request.headerList.Add(entry.headerName, entry.headerValue);

        return true;
    }

    bool Client::ProcessHeadersIncrementalIndexedHeaderFieldNewName(std::size_t &index) {
        // Skip the first index, since it is 0x40 always
        ++index;

        auto name = ParseString(index, Base::Globals::configuration->requestLimits.maximumHeaderNameLength, ErrorCode::HEADER_NAME_TOO_LONG);
        if (!name.second) {
            return false;
        }

        if (std::empty(name.first)) {
            HandleError(currentReceivingFrame, ErrorCode::HEADER_NAME_EMPTY);
            return false;
        }

        auto value = ParseString(index, Base::Globals::configuration->requestLimits.maximumHeaderValueLength, ErrorCode::HEADER_VALUE_TOO_LONG);
        if (!value.second) {
            return false;
        }

        auto &entry = dynamicTable.Insert(std::make_shared<std::string>(name.first), std::make_shared<std::string>(value.first));
        request.headerList.AddShared(entry.name, entry.value);

        return true;
    }

    bool Client::ProcessHeadersIncrementalIndexedHeaderFieldIndexedName(std::size_t &index) {
        auto position = ParseHPACKInteger<0x40>(&currentReceivingFrame.payload[index], index);

        if (position == 0) {
            HandleError(currentReceivingFrame, ErrorCode::COMPRESSION_ILLEGAL_INDEX_0);
            return false;
        }

        auto value = ParseString(index, Base::Globals::configuration->requestLimits.maximumHeaderValueLength, ErrorCode::HEADER_VALUE_TOO_LONG);
        if (!value.second) {
            return false;
        }

        if (position > std::size(StaticTable::table)) {
            position -= std::size(StaticTable::table);
            if (position > std::size(dynamicTable.storage)) {
                HandleError(currentReceivingFrame, ErrorCode::COMPRESSION_INDEX_OUTSIDE_INDEX_ADDRESS_SPACE);
                return false;
            }

            const auto &indexedName = dynamicTable[position].name;

            auto &entry = dynamicTable.Insert(std::shared_ptr<std::string>(indexedName), std::make_shared<std::string>(value.first));
            request.headerList.AddShared(entry.name, entry.value);
            return true;
        }

        const auto &staticTableEntry = StaticTable::table[position];
        auto &entry = dynamicTable.Insert(std::make_shared<std::string>(staticTableEntry.headerName), std::make_shared<std::string>(value.first));
        request.headerList.AddShared(entry.name, entry.value);
        return true;
    }

    bool Client::ProcessHeadersWithoutIndexingIndexedName(std::size_t &index) {
        auto position = ParseHPACKInteger<0x40>(&currentReceivingFrame.payload[index], index);
        if (position == 0) {
            HandleError(currentReceivingFrame, ErrorCode::COMPRESSION_ILLEGAL_INDEX_0);
            return false;
        }

        auto value = ParseString(index, Base::Globals::configuration->requestLimits.maximumHeaderValueLength, ErrorCode::HEADER_VALUE_TOO_LONG);
        if (!value.second) {
            return false;
        }

        if (position > std::size(StaticTable::table)) {
            position -= std::size(StaticTable::table);
            if (position > std::size(dynamicTable.storage)) {
                HandleError(currentReceivingFrame, ErrorCode::COMPRESSION_INDEX_OUTSIDE_INDEX_ADDRESS_SPACE);
                return false;
            }

            const auto &indexedName = dynamicTable[position].name;

            auto &entry = dynamicTable.Insert(std::shared_ptr<std::string>(indexedName), std::make_shared<std::string>(value.first));
            request.headerList.AddShared(entry.name, entry.value);
            return true;
        }

        const auto &entry = StaticTable::table[position];

        request.headerList.Add(entry.headerName, std::move(value.first));

        return true;
    }

    bool Client::ProcessHeadersWithoutIndexingNewName(std::size_t &index) {
        // Skip the first index, since it is 0x40 always
        ++index;

        auto name = ParseString(index, Base::Globals::configuration->requestLimits.maximumHeaderNameLength, ErrorCode::HEADER_NAME_TOO_LONG);
        if (!name.second) {
            return false;
        }

        if (std::empty(name.first)) {
            HandleError(currentReceivingFrame, ErrorCode::HEADER_NAME_EMPTY);
            return false;
        }

        auto value = ParseString(index, Base::Globals::configuration->requestLimits.maximumHeaderValueLength, ErrorCode::HEADER_VALUE_TOO_LONG);
        if (!value.second) {
            return false;
        }

        request.headerList.Add(std::move(name.first), std::move(value.first));
        return true;
    }

    std::pair<std::string, bool>
    Client::ParseString(std::size_t &index, std::size_t maxLength, ErrorCode errorCodeWhenTooLong) {
        const auto data = currentReceivingFrame.payload.data();
        const bool isHuffmanEncoded = data[index] & 0x80;

        const auto length = ParseHPACKInteger<0x80>(data + index, index);
        if (length == 0) {
            return {{}, true};
        }

        if (length > maxLength) {
            if (Base::Globals::configuration->httpSettings.errorExplanationLevel == Base::HTTPSettings::ExplainLevel::EVERYTHING) {
                HandleError(currentReceivingFrame, ErrorInformation{
                        errorCodeWhenTooLong,
                        "",
                        fmt::format("Length({}) is greater than the maximum({})", length, maxLength)
                });
            } else {
                HandleError(currentReceivingFrame, errorCodeWhenTooLong);
            }
            return {{}, false};
        }

        if (!isHuffmanEncoded) {
            std::string result(data + index, length);
            index += length;
            return {std::move(result), true};
        }

        auto result = Base::Globals::huffmanCoding->Decode(std::string_view(
                data + index, length
        ));

        index += length;

        if (result.failed) {
            HandleError(currentReceivingFrame, ErrorInformation{
                result.errorCode,
                "parsing an Huffman-encoded string"
            });
            return {};
        }

        return {std::move(result.output), true};
    }

    void Client::ProcessRequest() {
        request.version.major = 2;
        request.version.minor = 0;

        const auto hasUppercaseLetters = [](const auto &name) {
            return std::any_of(std::cbegin(name),
                               std::cend(name),
                               [](char character) {
                return character >= 'A' && character <= 'Z';
            });
        };

        /**
         * Processes an header. If true is returned, HandleError was already
         * called.
         *
         * Returns true if the header was invalid
         */
        auto handleHeader = [=, this](const auto &header) mutable {
            if (hasUppercaseLetters(header.name)) {
                HandleError(currentReceivingFrame, ErrorCode::UPPERCASE_LETTERS_IN_HEADER_NAME);
                return true;
            }

            if (header.name[0] == ':') {
                const std::string_view part{std::data(header.name)+1, std::size(header.name) - 1};

                if (part == "path") {
                    ProcessPseudoHeaderPath(header.value);
                } else if (part == "method") {
                    ProcessPseudoHeaderMethod(header.value);
                } else if (part == "scheme") {
                    // We don't care about this header
                } else if (part == "authority") {
                    // TODO process this header
                } else {
                    HandleError(currentReceivingFrame, ErrorCode::PSEUDO_INVALID_HEADER_NAME);
                    return true;
                }
            }

            return false;
        };

        if (std::any_of(std::cbegin(request.headerList.strings), std::cend(request.headerList.strings), handleHeader) ||
            std::any_of(std::cbegin(request.headerList.stringViews), std::cend(request.headerList.stringViews), handleHeader) ||
            std::any_of(std::cbegin(request.headerList.sharedStrings), std::cend(request.headerList.sharedStrings), [handleHeader](const auto &header) mutable {
                return handleHeader(Header<std::string_view>(*header.name, *header.value));
            })) {
            return;
        }

        if (IsRequestInvalid()) {
            return;
        }

        const auto result = Base::Globals::masterProvider->HandleRequest(this);
        if (result != Provider::HandleResult::OK) {
            std::cout << "[h2] HandleResult of MasterProvider is: " << Provider::ToString(result) << '\n';
            connection->ioState = Resources::IOState::DEAD;
            return;
        }

        if (Base::Globals::configuration->http2Settings.debugPrintRequestsAfterHandling)
            std::cout << "[h2] Request is handled \"" << request.path << "\"\n";

        ResetConversation();

        // Reset back to the initial state
        clientHandlingState = ClientHandlingState::RESET_FRAME;
    }

    bool Client::IsRequestInvalid() {
        if (std::empty(request.path)) {
            HandleError(currentReceivingFrame, ErrorCode::PSEUDO_PATH_MISSING);
            return true;
        }

        if (std::empty(request.method)) {
            HandleError(currentReceivingFrame, ErrorCode::PSEUDO_METHOD_MISSING);
            return true;
        }

        return false;
    }

    void Client::SendBadRequestResponse(const ErrorInformation &info) {
        response.SetStatusCode<4, 0, 0>();
        response.headerList.Add("Content-Type"sv, "text/plain;charset=usascii"sv);

        const auto sendBody = [=, this](std::string_view body) {
            response.headerList.Add(std::string("Content-Length"), std::to_string(std::size(body)));
            if (!Base::Globals::masterProvider->FinalizeResponseMetadata(this)
                    || !SendResponseMetadata() || !SendResponseString(body)) {
                connection->ioState = Resources::IOState::DEAD;
            }
        };

        switch (Base::Globals::configuration->httpSettings.errorExplanationLevel) {
            case Base::HTTPSettings::ExplainLevel::HIDE_ERROR: {
                sendBody("Malformed Request");
            } break;
            case Base::HTTPSettings::ExplainLevel::SHOW_ERROR_NAME: {
                sendBody(std::string("Error Code: ") + TranslateErrorCodeToString(info.errorCode));
            } break;
            case Base::HTTPSettings::ExplainLevel::EVERYTHING: {
                // TODO In the future, we might want a helper function in
                //      ErrorCode.hpp that can give a full description of the
                //      error.
                // TODO We also might profit from having a string passed to
                //      this function (or HandleError in general) which explains
                //      where in the program this error was triggered, to give
                //      more context to an experienced user.
                std::stringstream stream{};
                stream << "(!) Malformed Request:"
                          "\n      Error Code: "
                          "\n        " << info.errorCode;

                if (!std::empty(info.condition)) {
                    stream << "\n      Failed Condition:"
                              "\n        " << info.condition;
                }

                if (!std::empty(info.task)) {
                    stream << "\n      During Task:"
                              "\n        " << info.task;
                }

                if (TranslateErrorCodeToH2ErrorCode(info.errorCode) != Registry::H2ErrorCode::NO_ERROR) {
                    stream << "\n      HTTP/2 Error Code:"
                              "\n        " << TranslateErrorCodeToH2ErrorCode(info.errorCode);
                }

                stream << "\n      Website:"
                          "\n        https://gitlab.com/thewoosh/Wizard2/-/blob/master/Source/HTTP/v2/ErrorCode.hpp"
                          "\n\n";
                sendBody(stream.str());
            } break;
        }
    }

    void Client::ProcessPseudoHeaderPath(std::string_view value) {
        if (!std::empty(request.path)) {
            HandleError(currentReceivingFrame, ErrorCode::PSEUDO_PATH_DUPLICATE);
            return;
        }

        if (std::empty(value)) {
            HandleError(currentReceivingFrame, ErrorCode::PSEUDO_PATH_EMPTY);
            return;
        }

        request.path = value;
    }

    void Client::ProcessPseudoHeaderMethod(std::string_view value) {
        if (!std::empty(request.method)) {
            HandleError(currentReceivingFrame, ErrorCode::PSEUDO_METHOD_DUPLICATE);
            return;
        }

        if (std::empty(value)) {
            HandleError(currentReceivingFrame, ErrorCode::PSEUDO_METHOD_EMPTY);
            return;
        }

        request.method = value;
    }

    void Client::TrySendingEnqueuedFrames() {
        while (!std::empty(frameSendQueue) && connection->ioState == Resources::IOState::READY) {
            const auto &frame = frameSendQueue.front();

            // Prepare the metadata buffer.
            if (currentFramePosition == 0) {
                Base::Conversions::Destruct<3>(frame.length, std::data(frameMetadataBuffer));
                frameMetadataBuffer[3] = static_cast<char>(Registry::ConvertFrameType(frame.type));
                frameMetadataBuffer[4] = static_cast<char>(frame.flags);
                Base::Conversions::Destruct<4>(frame.streamID, std::data(frameMetadataBuffer) + 5);
            }

            // Sending out the frame metadata
            if (currentFramePosition < 9) {
                do {
                    const auto ret = connection->WriteIterators(
                            std::cbegin(frameMetadataBuffer) + currentFramePosition,
                            std::cend(frameMetadataBuffer)
                    );

                    if (connection->ioState != Resources::IOState::READY) {
                        return;
                    }

                    if (ret == 0) {
                        currentFramePosition = 9;
                        break;
                    }

                    currentFramePosition = static_cast<std::uint32_t>(ret);
                } while (currentFramePosition < 9);
            }

            // Sending the frame payload
            if (frame.Length() != 0) {
                do {
                    const auto ret = connection->WriteIterators(
                            std::cbegin(frame.payload) + currentFramePosition - 9,
                            std::cend(frame.payload)
                    );

                    if (connection->ioState != Resources::IOState::READY) {
                        return;
                    }

                    if (ret == 0) {
                        break;
                    }

                    currentFramePosition = static_cast<std::uint32_t>(ret);
                } while (currentFramePosition - 9 != frame.Length());
            }

            // Frame is sent completely; clean up
            currentFramePosition = 0;
            frameSendQueue.pop_front();

            if (Base::Globals::configuration->http2Settings.debugPrintOutgoingFrames) {
                std::cout << "[h2] Sent frame (" << std::addressof(frame) << ')'
                          << " t=" << frame.type
                          << " l=" << frame.Length()
                          << " f=0x" << std::hex << static_cast<std::uint16_t>(frame.flags & 0xFF)
                          << " s=0x" << frame.streamID << std::dec
                          << "\n";
            }
        }
    }

    bool
    Client::SendResponseMetadata() noexcept {
        auto &frame = CreateSendFrame(Registry::FrameType::HEADERS);
        frame.flags = 0x4; // END_HEADERS
        frame.streamID = currentReceivingFrame.streamID;

        if (!AddStatusCodeToHeaderList(frame.payload)) {
            HandleError(currentReceivingFrame, ErrorCode::COMPRESSION_FAILED_ENCODE_STATUS_CODE);
            return true;
        }

        auto addHeader = [&frame](const auto &header) mutable {
            const auto doHeaderNamesMatch = [&header](const StaticTableDefinition &entry) {
                return std::equal(std::cbegin(entry.headerName), std::cend(entry.headerName),
                                  std::cbegin(header.name), std::cend(header.name),
                                  [](char a, char b) {
                                      return std::tolower(a) == std::tolower(b);
                                  });
            };

            if (doHeaderNamesMatch({"connection"})) {
                // TODO other names are prohibited, too
                return;
            }

            const auto it = std::find_if(std::cbegin(StaticTable::table),
                                         std::cend(StaticTable::table),
                                         doHeaderNamesMatch);

            if (it == std::cend(StaticTable::table)) {
                frame.payload.reserve(std::size(frame.payload)
                                      + 3
                                      + std::size(header.name)
                                      + std::size(header.value));
                // TODO make sure the length of name and value fit into a
                // single octet (len < 256)
                frame.payload.push_back(0x40);
                frame.payload.push_back(static_cast<char>(std::size(header.name)));
                for (auto c : header.name) {
                    frame.payload.push_back(static_cast<char>(std::tolower(c)));
                }
                frame.payload.push_back(static_cast<char>(std::size(header.value)));
                std::copy(std::cbegin(header.value), std::cend(header.value), std::back_inserter(frame.payload));
                return;
            }

            const auto index = std::distance(std::cbegin(StaticTable::table), it);
            assert(index != 0);

            if (it->headerValue == header.value) {
                frame.payload.push_back(static_cast<char>(0x80 + index));
            } else {
                frame.payload.push_back(static_cast<char>(0x40 + index));
                frame.payload.push_back(static_cast<char>(std::size(header.value)));
                frame.payload.reserve(std::size(frame.payload) + std::size(header.value));
                std::copy(std::cbegin(header.value), std::cend(header.value), std::back_inserter(frame.payload));
            }
        };

        std::for_each(std::cbegin(response.headerList.strings),
                      std::cend(response.headerList.strings),
                      addHeader);

        std::for_each(std::cbegin(response.headerList.stringViews),
                      std::cend(response.headerList.stringViews),
                      addHeader);

        frame.length = static_cast<decltype(frame.length)>(std::size(frame.payload));
        return true;
    }

    bool Client::AddStatusCodeToHeaderList(std::vector<char> &vector) const {
        if (response.statusCode[0] == 0 || response.statusCode[0] == '0') {
            // This most likely happened because the request handling didn't
            // change the status code, oops!
            return false;
        }

        constexpr const std::array<std::uint16_t, 7> staticStatusCodes{
            200, 204, 206, 304, 400, 404, 500
        };

        const std::uint16_t computedStatusCode =
                Characters::ConvertCharacterToDigit(response.statusCode[0]) * 100 +
                Characters::ConvertCharacterToDigit(response.statusCode[1]) * 10 +
                Characters::ConvertCharacterToDigit(response.statusCode[2]);

        const auto it = std::find(std::cbegin(staticStatusCodes), std::cend(staticStatusCodes), computedStatusCode);

        if (it != std::cend(staticStatusCodes)) {
            const auto index = std::distance(std::cbegin(staticStatusCodes), it);
            constexpr std::uint8_t indexedHeaderField = 0x88;

            if (indexedHeaderField + index > 0xFF) {
                // A sanity check. This probably won't happen, since the static
                // table isn't event long enough to go beyond the value 0xFF
                // (136 + 62), but who knows!
                return false;
            }

            vector.push_back(static_cast<char>(indexedHeaderField + index));
            return true;
        }

        vector.push_back(static_cast<char>(0x88)); // ":method" with "200", but override the value
        vector.push_back(3);
        std::copy(std::cbegin(response.statusCode), std::cend(response.statusCode), std::back_inserter(vector));
        return true;
    }

    bool
    Client::SendResponseFile(int fd, std::size_t fileSize) noexcept {
        // TODO FreeBSD supports headers/trailers, which probably performs much
        //      better than this, when combined with Kernel-TLS.
        //      See: https://www.freebsd.org/cgi/man.cgi?query=sendfile&sektion=2

        if (fileSize == 0) {
            auto &frame = frameSendQueue.back();
            assert(frame.type == Registry::FrameType::HEADERS);
            frame.flags |= 0x1; // END_STREAM
            return true;
        }

        do {
            auto &frame = CreateSendFrame(Registry::FrameType::DATA);
            frame.streamID = currentReceivingFrame.streamID;
            frame.length = static_cast<decltype(frame.length)>(
                    std::min(fileSize, static_cast<std::size_t>(clientSettingsTable.maxFrameSize)));
            frame.payload.resize(frame.Length());

            std::size_t position = 0;
            do {
                const auto ret = read(fd, std::data(frame.payload) + position, frame.Length() - position);

                if (ret <= 0) {
                    return false;
                }

                position += static_cast<std::size_t>(ret);
            } while (position != frame.Length());

            fileSize -= frame.Length();
        } while (fileSize != 0);

        frameSendQueue.back().flags |= 0x1; // END_STREAM
        return true;
    }

    bool
    Client::SendResponseString(std::string_view sv) noexcept {
        while (!std::empty(sv)) {
            const auto chunkSize = std::min(std::size(sv), static_cast<std::size_t>(clientSettingsTable.maxFrameSize));
            const auto chunk = sv.substr(0, chunkSize);
            sv = sv.substr(chunkSize);

            auto &frame = CreateSendFrame(Registry::FrameType::DATA);
            frame.streamID = currentReceivingFrame.streamID;
            frame.length = static_cast<decltype(frame.length)>(chunkSize);
            frame.payload.reserve(chunkSize);
            std::copy(std::cbegin(chunk), std::cend(chunk), std::back_inserter(frame.payload));
        }

        if (frameSendQueue.back().type == Registry::FrameType::DATA) {
            frameSendQueue.back().flags |= 0x1; // END_STREAM
        }

        return true;
    }

    void Client::ProcessPushPromiseFrame() {
        constexpr std::string_view message{
            "Client cannot send PUSH_PROMISE"
        };

        SendGoaway(Registry::H2ErrorCode::PROTOCOL_ERROR,
                   std::begin(message), std::end(message));
    }

    void Client::ProcessPingFrame() {
        if (currentReceivingFrame.streamID != 0) {
            HandleError(currentReceivingFrame, ErrorCode::PING_FRAME_NOT_ON_STREAM_0);
            return;
        }

        if (currentReceivingFrame.Length() != 8) {
            HandleError(currentReceivingFrame, ErrorCode::PING_FRAME_SIZE_INCORRECT);
            return;
        }

        if (currentReceivingFrame.flags & 0x1) {
            // acknowledgement of previous PING frame sent by server; nothing
            // to do
            return;
        }

        SendPingFrameWithAckFlag();
    }

    void Client::ProcessGoawayFrame() {
        auto it = std::cbegin(currentReceivingFrame.payload);

        const auto clientLastStreamID = Base::Conversions::Construct<std::uint32_t, 4>(it);
        const auto errorCode = Base::Conversions::Construct<std::uint32_t, 4>(it += 4);
        std::string_view data(&*(it += 4), static_cast<std::size_t>(std::distance(it, std::cend(currentReceivingFrame.payload))));

        if (Base::Globals::configuration->http2Settings.debugPrintOutgoingErrorFrames) {
            std::cout << "[h2] GOAWAY lastStream=0x" << std::hex << clientLastStreamID << std::dec
                      << " errorCode=" << Registry::ConvertH2ErrorCode(errorCode)
                      << " data=\"" << data << "\"\n";
        }

        connection->ioState = Resources::IOState::DEAD;
    }

    void Client::SendPingFrame() {
        auto &frame = CreateSendFrame(Registry::FrameType::PING);
        frame.length = 8;
        frame.payload.resize(8);
    }

    void Client::SendPingFrameWithAckFlag() {
        auto &frame = CreateSendFrame(Registry::FrameType::PING);
        frame.flags = 0x1;
        frame.length = 8;
        frame.payload = currentReceivingFrame.payload;
    }

    bool
    Client::SendEmptyResponse() noexcept {
        auto &frame = CreateSendFrame(Registry::FrameType::DATA);
        frame.flags = 0x1; // END_STREAM
        frame.streamID = currentReceivingFrame.streamID;

        return true;
    }

} // namespace HTTP::v2

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>

#include "Source/HTTP/v2/Registry/ErrorCode.hpp"

namespace HTTP::v2 {

    enum class ErrorCode {

        /**
         * The connection preface string is defined as a 24-octet constant. The
         * client apparently didn't send the correct string.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 3.5
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-http-2-connection-preface
         *   RFC 7540 § 3.5
         *     https://tools.ietf.org/html/rfc7540#section-3.5
         */
        INCORRECT_CONNECTION_PREFACE_STRING,

        /**
         * After  the connection preface, both endpoints should send a settings
         * frame.  In  case  the first frame isn't of type SETTINGS (0x4), this
         * error is triggered.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 3.5
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-http-2-connection-preface
         *   RFC 7540 § 3.5
         *     https://tools.ietf.org/html/rfc7540#section-3.5
         */
        FIRST_FRAME_NOT_SETTINGS,

        /**
         * A frame must have a size that the other endpoint can accept. The
         * client didn't acknowledge this fact, so this error was triggered.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type FRAME_SIZE_ERROR (0x6)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 4.2
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#section-4.2-4
         *   RFC 7540 § 4.2
         *     https://httpwg.org/specs/rfc7540.html#rfc.section.4.2
         */
        FRAME_SIZE_EXCEEDS_SETTING,

        /**
         * A settings frame must be acknowledged by the other endpoint. This is
         * done be sending another settings frame, with length = 0, and the ACK
         * flag set.
          *
         * When  this  error is triggered, the server received a settings frame
         * with the ACK flag set, but the length of the frame isn't zero.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type FRAME_SIZE_ERROR (0x6)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.5
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-settings
         *   RFC 7540 § 6.5
         *     https://tools.ietf.org/html/rfc7540#section-6.5
         */
        SETTINGS_ACK_NOT_LENGTH_ZERO,

        /**
         * A settings frame must be acknowledged by the other endpoint. This is
         * done be sending another settings frame, with length = 0, and the ACK
         * flag set.
          *
         * When  this  error is triggered, the server received a settings frame
         * with  the  ACK flag set, but the client has already acknowledged all
         * the settings frame the server has sent them.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x6)
         *   2. Connection closure.
         *
         * Conforming To:
         *   Nothing.  The  specification doesn't explicitly state that this is
         *   is  an error, but since an ACK-settings frame MUST be sent _after_
         *   reception of a non-ACK settings frame, this must be a protocol
         *   error.
         */
        SETTINGS_ACK_WITHOUT_CORRESPONDING_FRAME,

        /**
         * Settings  frames are only sent on the stream with identifier 0. This
         * is  explicitly stated in the spec, because the settings apply to the
         * connection as a whole, not to an individual stream.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.5
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-settings
         *   RFC 7540 § 6.5
         *     https://tools.ietf.org/html/rfc7540#section-6.5
         */
        SETTINGS_NOT_STREAM_0,

        /**
         * Settings frames should have a length of a multiple of 6.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type FRAME_SIZE_ERROR (0x6)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis §§ 6.5 - 6.5.1
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-settings
         *   RFC 7540 §§ 6.5 - 6.5.1
         *     https://tools.ietf.org/html/rfc7540#section-6.5
         */
        SETTINGS_INVALID_LENGTH,

        /**
         * The SETTINGS_ENABLE_PUSH (0x2) should have a value of 1 or 0.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.5.2
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-defined-settings-parameters
         *   RFC 7540 § 6.5.2
         *     https://tools.ietf.org/html/rfc7540#section-6.5.2
         */
        SETTINGS_PARAMETER_ENABLE_PUSH_NOT_BOOLEAN,

        /**
         * The SETTINGS_INITIAL_WINDOW_SIZE (0x4) should have a value less than
         * the maximum flow-control window size of 2^31 - 1.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type FLOW_CONTROL_ERROR (0x3)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.5.2
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-defined-settings-parameters
         *   RFC 7540 § 6.5.2
         *     https://tools.ietf.org/html/rfc7540#section-6.5.2
         */
        SETTINGS_PARAMETER_INITIAL_WINDOW_SIZE_EXCEEDS_MAXIMUM_FLOW_CONTROL_WINDOW_SIZE,

        /**
         * The SETTINGS_MAX_FRAME_SIZE (0x4) should have a value at least 2^14.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.5.2
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-defined-settings-parameters
         *   RFC 7540 § 6.5.2
         *     https://tools.ietf.org/html/rfc7540#section-6.5.2
         */
        SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_SMALL,

        /**
         * The  SETTINGS_MAX_FRAME_SIZE (0x4) should have a value at not higher
         * than 2^31 - 1.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.5.2
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#name-defined-settings-parameters
         *   RFC 7540 § 6.5.2
         *     https://tools.ietf.org/html/rfc7540#section-6.5.2
         */
        SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_LARGE,

        /**
         * The PING frame is always sent on stream 0x0.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.7
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#section-6.7-7
         *   RFC 7540 § 6.7
         *     https://httpwg.org/specs/rfc7540.html#rfc.section.6.7.p.5
         */
        PING_FRAME_NOT_ON_STREAM_0,

        /**
         * The PING frame has a length of exactly 8 octets.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type FRAME_SIZE_ERROR (0x6)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC draft-http2-bis § 6.7
         *     https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#section-6.7-8
         *   RFC 7540 § 6.7
         *     https://httpwg.org/specs/rfc7540.html#rfc.section.6.7.p.6
         */
        PING_FRAME_SIZE_INCORRECT,

        /**
         * An header field with index 0 is not allowed.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type COMPRESSION_ERROR (0x9)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC 7541 § 6.1
         *     https://httpwg.org/specs/rfc7541.html#rfc.section.6.1.p.4
         */
        COMPRESSION_ILLEGAL_INDEX_0,

        /**
         * An header field with index greater than the size of both the dynamic
         * as the static table is not allowed.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type COMPRESSION_ERROR (0x9)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC 7541 § 6.1
         *     https://httpwg.org/specs/rfc7541.html#rfc.section.2.3.3.p.4
         */
        COMPRESSION_INDEX_OUTSIDE_INDEX_ADDRESS_SPACE,

        /**
         * An Huffman-encoded string shouldn't contain the EOS symbol, since it
         * has index 256 and is only used for padding.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type COMPRESSION_ERROR (0x9)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC 7541 § 6.1
         *     https://httpwg.org/specs/rfc7541.html#rfc.section.5.2.p.5
         */
        COMPRESSION_HUFFMAN_STRING_CONTAINS_EOS,

        /**
         * An Huffman-encoded string should be padded by the EOS symbol.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type COMPRESSION_ERROR (0x9)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC 7541 § 6.1
         *     https://httpwg.org/specs/rfc7541.html#rfc.section.5.2.p.5
         */
        COMPRESSION_HUFFMAN_PADDING_IS_NOT_EOS,

        /**
         * An Huffman-encoded string should have at most 7 bits of padding,
         * otherwise it would defeat the purpose of padding.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type COMPRESSION_ERROR (0x9)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC 7541 § 6.1
         *     https://httpwg.org/specs/rfc7541.html#rfc.section.5.2.p.5
         */
        COMPRESSION_HUFFMAN_PADDING_IS_MORE_THAN_7_BITS,

        /**
         * We failed to encode the status code.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type INTERNAL_ERROR (0x2)
         *   2. Connection closure.
         *
         * Conforming To:
         *   Not applicable.
         */
        COMPRESSION_FAILED_ENCODE_STATUS_CODE,

        /**
         * The pseudo header field ":path" cannot be empty.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *   2. RST_STREAM (0x3) of type PROTOCOL_ERROR (0x1)
         *
         * Conforming To:
         *   RFC 7540 § 8.1.2.1
         *     https://httpwg.org/specs/rfc7540.html#rfc.section.8.1.2.1.p.3
         */
        PSEUDO_INVALID_HEADER_NAME,

        /**
         * The pseudo header field ":path" cannot be empty.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *   2. RST_STREAM (0x3) of type PROTOCOL_ERROR (0x1)
         *
         * Conforming To:
         *   RFC 7541 § 8.1.2.3
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.3
         *   RFC 7541 § 8.1.2.6
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.6
         */
        PSEUDO_PATH_EMPTY,

        /**
         * The pseudo header field ":path" must occur exactly once.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *   2. RST_STREAM (0x3) of type PROTOCOL_ERROR (0x1)
         *
         * Conforming To:
         *   RFC 7541 § 8.1.2.3
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.3
         *   RFC 7541 § 8.1.2.6
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.6
         */
        PSEUDO_PATH_DUPLICATE,
        PSEUDO_PATH_MISSING,

        /**
         * The pseudo header field ":method" cannot be empty.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *   2. RST_STREAM (0x3) of type PROTOCOL_ERROR (0x1)
         *
         * Conforming To:
         *   RFC 7541 § 8.1.2.3
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.3
         *   RFC 7541 § 8.1.2.6
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.6
         */
        PSEUDO_METHOD_EMPTY,

        /**
         * The pseudo header field ":method" must occur exactly once.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *   2. RST_STREAM (0x3) of type PROTOCOL_ERROR (0x1)
         *
         * Conforming To:
         *   RFC 7541 § 8.1.2.3
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.3
         *   RFC 7541 § 8.1.2.6
         *     https://tools.ietf.org/html/rfc7540#section-8.1.2.6
         */
        PSEUDO_METHOD_DUPLICATE,
        PSEUDO_METHOD_MISSING,

        /**
         * The name of an HTTP header field cannot be empty.
         *
         * Handling Procedure:
         *   1. GOAWAY (0x7) of type PROTOCOL_ERROR (0x1)
         *   2. Connection closure.
         *
         * Conforming To:
         *   RFC 7230 § 3.2
         *     https://httpwg.org/specs/rfc7230.html#header.fields
         */
        HEADER_NAME_EMPTY,

        /**
         * Uppercase letters in an header name are disallowed.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *   2. RST_STREAM (0x3) of type PROTOCOL_ERROR (0x1)
         *
         * Conforming To:
         *   RFC 7540 § 8.1.2.
         *     https://httpwg.org/specs/rfc7540.html#rfc.section.8.1.2.p.2
         *   RFC 7540 § 8.1.2.6.
         *     https://httpwg.org/specs/rfc7540.html#malformed
         */
        UPPERCASE_LETTERS_IN_HEADER_NAME,

        /**
         * We're imposing restrictions on the length of header names.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *
         * Conforming To:
         *   not applicable
         */
        HEADER_NAME_TOO_LONG,

        /**
         * We're imposing restrictions on the length of header values.
         *
         * Handling Procedure:
         *   1. Send "400 Bad Request" response
         *
         * Conforming To:
         *   not applicable
         */
        HEADER_VALUE_TOO_LONG,
    };

    [[nodiscard]] constexpr inline const char *
    TranslateErrorCodeToString(ErrorCode errorCode) noexcept {
        switch (errorCode) {
            case ErrorCode::INCORRECT_CONNECTION_PREFACE_STRING: return "incorrect-connection-preface-string";
            case ErrorCode::FIRST_FRAME_NOT_SETTINGS: return "first-frame-not-settings";
            case ErrorCode::FRAME_SIZE_EXCEEDS_SETTING: return "frame-size-exceeds-setting";
            case ErrorCode::SETTINGS_ACK_NOT_LENGTH_ZERO: return "settings-ack-not-length-zero";
            case ErrorCode::SETTINGS_ACK_WITHOUT_CORRESPONDING_FRAME: return "settings-ack-without-corresponding-frame";
            case ErrorCode::SETTINGS_NOT_STREAM_0: return "settings-not-stream-0";
            case ErrorCode::SETTINGS_INVALID_LENGTH: return "settings-invalid-length";
            case ErrorCode::SETTINGS_PARAMETER_ENABLE_PUSH_NOT_BOOLEAN: return "settings-parameter-enable-push-not-boolean";
            case ErrorCode::SETTINGS_PARAMETER_INITIAL_WINDOW_SIZE_EXCEEDS_MAXIMUM_FLOW_CONTROL_WINDOW_SIZE: return "settings-parameter-initial-window-size-exceeds-flow-control-window-size";
            case ErrorCode::SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_SMALL: return "settings-parameter-max-frame-size-too-small";
            case ErrorCode::SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_LARGE: return "settings-parameter-max-frame-size-too-large";
            case ErrorCode::PING_FRAME_NOT_ON_STREAM_0: return "ping-frame-not-on-stream-0x0";
            case ErrorCode::PING_FRAME_SIZE_INCORRECT: return "ping-frame-size-incorrect";
            case ErrorCode::COMPRESSION_ILLEGAL_INDEX_0: return "compression-illegal-index-0";
            case ErrorCode::COMPRESSION_INDEX_OUTSIDE_INDEX_ADDRESS_SPACE: return "compression-index-outside-index-address-space";
            case ErrorCode::COMPRESSION_HUFFMAN_STRING_CONTAINS_EOS: return "compression-huffman-string-contains-eos";
            case ErrorCode::COMPRESSION_HUFFMAN_PADDING_IS_NOT_EOS: return "compression-huffman-padding-is-not-eos";
            case ErrorCode::COMPRESSION_HUFFMAN_PADDING_IS_MORE_THAN_7_BITS: return "compression-huffman-padding-is-more-than-7-bits";
            case ErrorCode::COMPRESSION_FAILED_ENCODE_STATUS_CODE: return "compression-failed-encode-status-code";
            case ErrorCode::PSEUDO_INVALID_HEADER_NAME: return "pseudo-invalid-header-name";
            case ErrorCode::PSEUDO_PATH_EMPTY: return "pseudo-path-empty";
            case ErrorCode::PSEUDO_PATH_DUPLICATE: return "pseudo-path-duplicate";
            case ErrorCode::PSEUDO_PATH_MISSING: return "pseudo-path-missing";
            case ErrorCode::PSEUDO_METHOD_EMPTY: return "pseudo-method-empty";
            case ErrorCode::PSEUDO_METHOD_DUPLICATE: return "pseudo-method-duplicate";
            case ErrorCode::PSEUDO_METHOD_MISSING: return "pseudo-method-missing";
            case ErrorCode::HEADER_NAME_EMPTY: return "header-name-empty";
            case ErrorCode::UPPERCASE_LETTERS_IN_HEADER_NAME: return "uppercase-letters-in-header-name";
            case ErrorCode::HEADER_NAME_TOO_LONG: return "header-name-too-long";
            case ErrorCode::HEADER_VALUE_TOO_LONG: return "header-value-too-long";
            default: return "illegal-value";
        }
    }

    [[nodiscard]] constexpr inline Registry::H2ErrorCode
    TranslateErrorCodeToH2ErrorCode(ErrorCode errorCode) noexcept {
        switch (errorCode) {
            case ErrorCode::INCORRECT_CONNECTION_PREFACE_STRING: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::FIRST_FRAME_NOT_SETTINGS: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::FRAME_SIZE_EXCEEDS_SETTING: return Registry::H2ErrorCode::FRAME_SIZE_ERROR;
            case ErrorCode::SETTINGS_ACK_NOT_LENGTH_ZERO: return Registry::H2ErrorCode::FRAME_SIZE_ERROR;
            case ErrorCode::SETTINGS_ACK_WITHOUT_CORRESPONDING_FRAME: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::SETTINGS_NOT_STREAM_0: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::SETTINGS_INVALID_LENGTH: return Registry::H2ErrorCode::FRAME_SIZE_ERROR;
            case ErrorCode::SETTINGS_PARAMETER_ENABLE_PUSH_NOT_BOOLEAN: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::SETTINGS_PARAMETER_INITIAL_WINDOW_SIZE_EXCEEDS_MAXIMUM_FLOW_CONTROL_WINDOW_SIZE: return Registry::H2ErrorCode::FLOW_CONTROL_ERROR;
            case ErrorCode::SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_SMALL: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::SETTINGS_PARAMETER_MAX_FRAME_SIZE_TOO_LARGE: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PING_FRAME_NOT_ON_STREAM_0: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PING_FRAME_SIZE_INCORRECT: return Registry::H2ErrorCode::FRAME_SIZE_ERROR;
            case ErrorCode::COMPRESSION_ILLEGAL_INDEX_0: return Registry::H2ErrorCode::COMPRESSION_ERROR;
            case ErrorCode::COMPRESSION_INDEX_OUTSIDE_INDEX_ADDRESS_SPACE: return Registry::H2ErrorCode::COMPRESSION_ERROR;
            case ErrorCode::COMPRESSION_HUFFMAN_STRING_CONTAINS_EOS: return Registry::H2ErrorCode::COMPRESSION_ERROR;
            case ErrorCode::COMPRESSION_HUFFMAN_PADDING_IS_NOT_EOS: return Registry::H2ErrorCode::COMPRESSION_ERROR;
            case ErrorCode::COMPRESSION_HUFFMAN_PADDING_IS_MORE_THAN_7_BITS: return Registry::H2ErrorCode::COMPRESSION_ERROR;
            case ErrorCode::COMPRESSION_FAILED_ENCODE_STATUS_CODE: return Registry::H2ErrorCode::INTERNAL_ERROR;
            case ErrorCode::PSEUDO_INVALID_HEADER_NAME: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PSEUDO_PATH_EMPTY: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PSEUDO_PATH_DUPLICATE: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PSEUDO_PATH_MISSING: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PSEUDO_METHOD_EMPTY: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PSEUDO_METHOD_DUPLICATE: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::PSEUDO_METHOD_MISSING: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::HEADER_NAME_EMPTY: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::UPPERCASE_LETTERS_IN_HEADER_NAME: return Registry::H2ErrorCode::PROTOCOL_ERROR;
            case ErrorCode::HEADER_NAME_TOO_LONG: return Registry::H2ErrorCode::NO_ERROR;
            case ErrorCode::HEADER_VALUE_TOO_LONG: return Registry::H2ErrorCode::NO_ERROR;
            default: return Registry::H2ErrorCode::INTERNAL_ERROR;
        }
    }

    [[nodiscard]] constexpr inline bool
    IsStreamError(ErrorCode errorCode) noexcept {
        switch (errorCode) {
            case ErrorCode::PSEUDO_PATH_EMPTY:
            case ErrorCode::HEADER_NAME_TOO_LONG:
            case ErrorCode::HEADER_VALUE_TOO_LONG:
                return true;
            default:
                return false;
        }
    }

    [[nodiscard]] constexpr inline bool
    IsBadRequestError(ErrorCode errorCode) {
        switch (errorCode) {
            case ErrorCode::HEADER_NAME_TOO_LONG:
            case ErrorCode::HEADER_VALUE_TOO_LONG:
                return true;
            default:
                return false;
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, ErrorCode errorCode) {
        stream << TranslateErrorCodeToString(errorCode);
        return stream;
    }

} // namespace HTTP::v2

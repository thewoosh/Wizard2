/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <string>
#include <string_view>

#include <cstdint>

#include "Source/HTTP/v2/ErrorCode.hpp"

namespace HTTP::v2 {

    struct HuffmanNode {
        std::unique_ptr<HuffmanNode> left;
        std::unique_ptr<HuffmanNode> right;
        char value;
    };

    struct HuffmanDecodeResult {
        bool failed{false};
        ErrorCode errorCode{};
        std::string output{};
    };

    class HuffmanCoding {
        HuffmanNode rootNode{};

    public:
        HuffmanCoding() noexcept;

        [[nodiscard]] HuffmanDecodeResult
        Decode(std::string_view) const noexcept;
    };

} // namespace

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <array>
#include <string_view>

namespace HTTP::v2 {

    struct StaticTableDefinition {
        std::string_view headerName{};
        std::string_view headerValue{};

        StaticTableDefinition() = default;
        StaticTableDefinition(StaticTableDefinition &&) = default;
        StaticTableDefinition(const StaticTableDefinition &) = default;
        StaticTableDefinition &operator=(const StaticTableDefinition &) = default;
        StaticTableDefinition &operator=(StaticTableDefinition &&) = default;

        [[nodiscard]] inline constexpr // not explicit
        StaticTableDefinition(std::string_view name) noexcept
            : headerName(name) {
        }

        [[nodiscard]] inline constexpr
        StaticTableDefinition(std::string_view name,
                              std::string_view value) noexcept
                : headerName(name)
                , headerValue(value){
        }

    };

    namespace StaticTable {

        constexpr std::array<StaticTableDefinition, 62> table{{
                {},
                {":authority"},
                {":method", "GET"},
                {":method", "POST"},
                {":path", "/"},
                {":path", "/index.html"},
                {":scheme", "http"},
                {":scheme", "https"},

                {":status", "200"},
                {":status", "204"},
                {":status", "206"},
                {":status", "304"},
                {":status", "400"},
                {":status", "404"},
                {":status", "500"},

                {"accept-charset"},
                {"accept-encoding", "gzip, deflate"},
                {"accept-language"},
                {"accept-ranges"},
                {"accept"},

                {"access-control-allow-origin"},
                {"age"},
                {"allow"},
                {"authorization"},
                {"cache-control"},
                {"content-disposition"},
                {"content-encoding"},
                {"content-language"},
                {"content-length"},
                {"content-location"},
                {"content-range"},
                {"content-type"},
                {"cookie"},
                {"date"},
                {"etag"},
                {"expect"},
                {"expires"},
                {"from"},
                {"host"},
                {"if-match"},
                {"if-modified-since"},
                {"if-none-match"},
                {"if-range"},
                {"if-unmodified-since"},
                {"last-modified"},
                {"link"},
                {"location"},
                {"max-forwards"},
                {"proxy-authenticate"},
                {"proxy-authorization"},
                {"range"},
                {"referer"},
                {"refresh"},
                {"retry-after"},
                {"server"},
                {"set-cookie"},
                {"strict-transport-security"},
                {"transfer-encoding"},
                {"user-agent"},
                {"vary"},
                {"via"},
                {"www-authenticate"},
        }};

    } // namespace StaticTable

} // namespace HTTP::v2::HPACK

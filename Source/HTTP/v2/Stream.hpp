/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include <cstdint>

#include "Source/HTTP/v2/Registry/FrameType.hpp"

namespace HTTP::v2 {

    enum class StreamState {
        IDLE,
        RESERVED_LOCAL,
        RESERVED_REMOTE,
        OPEN,
        HALF_CLOSED_LOCAL,
        HALF_CLOSED_REMOTE,
        CLOSED
    };

    struct Stream {
        StreamState state{StreamState::IDLE};

        [[nodiscard]] inline bool
        AllowsHeaders() const noexcept {
            return state == StreamState::IDLE ||
                   state == StreamState::RESERVED_LOCAL ||
                   state == StreamState::OPEN ||
                   state == StreamState::HALF_CLOSED_REMOTE;
        }
    };

}

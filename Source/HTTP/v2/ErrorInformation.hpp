/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <iostream>

#include "Source/HTTP/v2/Client.hpp"

namespace HTTP::v2 {

    struct ErrorInformation {

        ErrorCode errorCode{};

        /**
         * The task we were trying to perform, but this error was encountered.
         */
        std::string_view task{};

        /**
         * A possibly condition that failed.
         */
        std::string condition{};

    };

} // namespace HTTP::v2

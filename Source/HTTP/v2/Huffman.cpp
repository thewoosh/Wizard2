/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/HTTP/v2/Huffman.hpp"

#include <array>

#include <cassert>

namespace HTTP::v2 {

    constinit std::array<std::string_view, 257> HuffmanCode = {
            "1111111111000",
            "11111111111111111011000",
            "1111111111111111111111100010",
            "1111111111111111111111100011",
            "1111111111111111111111100100",
            "1111111111111111111111100101",
            "1111111111111111111111100110",
            "1111111111111111111111100111",
            "1111111111111111111111101000",
            "111111111111111111101010",
            "111111111111111111111111111100",
            "1111111111111111111111101001",
            "1111111111111111111111101010",
            "111111111111111111111111111101",
            "1111111111111111111111101011",
            "1111111111111111111111101100",
            "1111111111111111111111101101",
            "1111111111111111111111101110",
            "1111111111111111111111101111",
            "1111111111111111111111110000",
            "1111111111111111111111110001",
            "1111111111111111111111110010",
            "111111111111111111111111111110",
            "1111111111111111111111110011",
            "1111111111111111111111110100",
            "1111111111111111111111110101",
            "1111111111111111111111110110",
            "1111111111111111111111110111",
            "1111111111111111111111111000",
            "1111111111111111111111111001",
            "1111111111111111111111111010",
            "1111111111111111111111111011",
            "010100",
            "1111111000",
            "1111111001",
            "111111111010",
            "1111111111001",
            "010101",
            "11111000",
            "11111111010",
            "1111111010",
            "1111111011",
            "11111001",
            "11111111011",
            "11111010",
            "010110",
            "010111",
            "011000",
            "00000",
            "00001",
            "00010",
            "011001",
            "011010",
            "011011",
            "011100",
            "011101",
            "011110",
            "011111",
            "1011100",
            "11111011",
            "11111111111110",
            "100000",
            "111111111011",
            "1111111100",
            "1111111111010",
            "100001",
            "1011101",
            "1011110",
            "1011111",
            "1100000",
            "1100001",
            "1100010",
            "1100011",
            "1100100",
            "1100101",
            "1100110",
            "1100111",
            "1101000",
            "1101001",
            "1101010",
            "1101011",
            "1101100",
            "1101101",
            "1101110",
            "1101111",
            "1110000",
            "1110001",
            "1110010",
            "11111100",
            "1110011",
            "11111101",
            "1111111111011",
            "1111111111111110000",
            "1111111111100",
            "11111111111100",
            "100010",
            "111111111111101",
            "00011",
            "100011",
            "00100",
            "100100",
            "00101",
            "100101",
            "100110",
            "100111",
            "00110",
            "1110100",
            "1110101",
            "101000",
            "101001",
            "101010",
            "00111",
            "101011",
            "1110110",
            "101100",
            "01000",
            "01001",
            "101101",
            "1110111",
            "1111000",
            "1111001",
            "1111010",
            "1111011",
            "111111111111110",
            "11111111100",
            "11111111111101",
            "1111111111101",
            "1111111111111111111111111100",
            "11111111111111100110",
            "1111111111111111010010",
            "11111111111111100111",
            "11111111111111101000",
            "1111111111111111010011",
            "1111111111111111010100",
            "1111111111111111010101",
            "11111111111111111011001",
            "1111111111111111010110",
            "11111111111111111011010",
            "11111111111111111011011",
            "11111111111111111011100",
            "11111111111111111011101",
            "11111111111111111011110",
            "111111111111111111101011",
            "11111111111111111011111",
            "111111111111111111101100",
            "111111111111111111101101",
            "1111111111111111010111",
            "11111111111111111100000",
            "111111111111111111101110",
            "11111111111111111100001",
            "11111111111111111100010",
            "11111111111111111100011",
            "11111111111111111100100",
            "111111111111111011100",
            "1111111111111111011000",
            "11111111111111111100101",
            "1111111111111111011001",
            "11111111111111111100110",
            "11111111111111111100111",
            "111111111111111111101111",
            "1111111111111111011010",
            "111111111111111011101",
            "11111111111111101001",
            "1111111111111111011011",
            "1111111111111111011100",
            "11111111111111111101000",
            "11111111111111111101001",
            "111111111111111011110",
            "11111111111111111101010",
            "1111111111111111011101",
            "1111111111111111011110",
            "111111111111111111110000",
            "111111111111111011111",
            "1111111111111111011111",
            "11111111111111111101011",
            "11111111111111111101100",
            "111111111111111100000",
            "111111111111111100001",
            "1111111111111111100000",
            "111111111111111100010",
            "11111111111111111101101",
            "1111111111111111100001",
            "11111111111111111101110",
            "11111111111111111101111",
            "11111111111111101010",
            "1111111111111111100010",
            "1111111111111111100011",
            "1111111111111111100100",
            "11111111111111111110000",
            "1111111111111111100101",
            "1111111111111111100110",
            "11111111111111111110001",
            "11111111111111111111100000",
            "11111111111111111111100001",
            "11111111111111101011",
            "1111111111111110001",
            "1111111111111111100111",
            "11111111111111111110010",
            "1111111111111111101000",
            "1111111111111111111101100",
            "11111111111111111111100010",
            "11111111111111111111100011",
            "11111111111111111111100100",
            "111111111111111111111011110",
            "111111111111111111111011111",
            "11111111111111111111100101",
            "111111111111111111110001",
            "1111111111111111111101101",
            "1111111111111110010",
            "111111111111111100011",
            "11111111111111111111100110",
            "111111111111111111111100000",
            "111111111111111111111100001",
            "11111111111111111111100111",
            "111111111111111111111100010",
            "111111111111111111110010",
            "111111111111111100100",
            "111111111111111100101",
            "11111111111111111111101000",
            "11111111111111111111101001",
            "1111111111111111111111111101",
            "111111111111111111111100011",
            "111111111111111111111100100",
            "111111111111111111111100101",
            "11111111111111101100",
            "111111111111111111110011",
            "11111111111111101101",
            "111111111111111100110",
            "1111111111111111101001",
            "111111111111111100111",
            "111111111111111101000",
            "11111111111111111110011",
            "1111111111111111101010",
            "1111111111111111101011",
            "1111111111111111111101110",
            "1111111111111111111101111",
            "111111111111111111110100",
            "111111111111111111110101",
            "11111111111111111111101010",
            "11111111111111111110100",
            "11111111111111111111101011",
            "111111111111111111111100110",
            "11111111111111111111101100",
            "11111111111111111111101101",
            "111111111111111111111100111",
            "111111111111111111111101000",
            "111111111111111111111101001",
            "111111111111111111111101010",
            "111111111111111111111101011",
            "1111111111111111111111111110",
            "111111111111111111111101100",
            "111111111111111111111101101",
            "111111111111111111111101110",
            "111111111111111111111101111",
            "111111111111111111111110000",
            "11111111111111111111101110",
            "111111111111111111111111111111",			/* EOS */
    };

    HuffmanCoding::HuffmanCoding() noexcept {
        std::uint16_t i{};

        for (const auto &text : HuffmanCode) {
            auto *node = &rootNode;
            node->value = '?';

            for (char character : text) {
                if (character == '0') {
                    if (!node->left) {
                        node->left = std::make_unique<HuffmanNode>();
                    }

                    node = node->left.get();
                } else {
                    assert(character == '1');
                    if (!node->right) {
                        node->right = std::make_unique<HuffmanNode>();
                    }

                    node = node->right.get();
                }
            }

            node->value = i < 0x100 ? static_cast<char>(i) : 0x00;
        }
    }

    HuffmanDecodeResult
    HuffmanCoding::Decode(std::string_view input) const noexcept {
        HuffmanDecodeResult result{};

        // Reserve a reasonable string, to avoid possible reallocation by +=
        // each time a character is appended.
        result.output.reserve(32);

        std::size_t bitPositionInInput{};
        const std::size_t maxBitPosition = std::size(input) * 8;

        // Is the path (like) the EOS path? It doesn't mean whether or not the
        // path is going to be the EOS path. It is just turned false when a 0
        // bit is encountered.
        bool eosPath{true};

        // How deep the path is; the amount of nodes travelled from rootNode.
        std::uint8_t depth{0};

        auto *node = &rootNode;
        for (; bitPositionInInput <= maxBitPosition; ++bitPositionInInput) {
            if (!node->left || !node->right) {
                if (eosPath) {
                    result.failed = true;
                    result.errorCode = ErrorCode::COMPRESSION_HUFFMAN_STRING_CONTAINS_EOS;
                    return result;
                }

                result.output += node->value;
                depth = 0;
                eosPath = true;
                node = &rootNode;
            }

            if (bitPositionInInput == maxBitPosition) {
                // path isn't relevant anymore. we do <= in the 2nd
                // expression in the for loop because we want to process the
                // last node (only applicable if the data doesn't have padding)
                break;
            }

            const std::size_t bitPositionInOctet = 7 - (bitPositionInInput % 8);
            const auto bit = static_cast<std::uint8_t>((input[bitPositionInInput / 8]
                    & (1 << bitPositionInOctet)) >> bitPositionInOctet);

            if (bit) {
                node = node->right.get();
            } else {
                node = node->left.get();
                if (bitPositionInInput + 1 != maxBitPosition) {
                    eosPath = false;
                }
            }

            ++depth;
        }

        if (!eosPath) {
            // The padding bits weren't of the EOS entry.
            result.failed = true;
            result.errorCode = ErrorCode::COMPRESSION_HUFFMAN_PADDING_IS_NOT_EOS;
            return result;
        }

        if ((depth & 0x7F) > 8) {
            puts("[Huffman] Error: Padding is more than 7");
            printf("  = %i\n", (depth ^ 0x80));
            // the padding is strictly 7 or less.
            result.failed = true;
            result.errorCode = ErrorCode::COMPRESSION_HUFFMAN_PADDING_IS_MORE_THAN_7_BITS;
            return result;
        }

        return result;
    }

}

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <queue>
#include <utility>
#include <vector>

#include "Source/Base/Configuration.hpp"
#include "Source/Base/Conversions.hpp"
#include "Source/HTTP/v2/Registry/ErrorCode.hpp"
#include "Source/HTTP/v2/ClientHandlingState.hpp"
#include "Source/HTTP/v2/DynamicTable.hpp"
#include "Source/HTTP/v2/ErrorCode.hpp"
#include "Source/HTTP/v2/ErrorInformation.hpp"
#include "Source/HTTP/v2/Frame.hpp"
#include "Source/HTTP/v2/SettingsTable.hpp"
#include "Source/HTTP/v2/Stream.hpp"
#include "Source/HTTP/AbstractClient.hpp"
#include "Source/HTTP/Request.hpp"
#include "Source/HTTP/Response.hpp"
#include "Source/Provider/MasterProvider.hpp"
#include "Source/Resources/Connection.hpp"

namespace HTTP::v2 {

    class Client : public AbstractClient {

        Resources::Connection *connection;
        ClientHandlingState clientHandlingState{};
        DynamicTable dynamicTable{};

        // TODO
        // https://httpwg.org/http2-spec/draft-ietf-httpbis-http2bis.html#section-6.8-2
        std::uint32_t lastStreamID{0};

        void HandleError(const Frame &, const ErrorInformation &);

        inline void HandleError(const Frame &frame, ErrorCode errorCode) {
            HandleError(frame, ErrorInformation{errorCode});
        }

        void HandleFrame();
        void ResetFrame();

        //
        // Settings
        //
        bool lastSentFrameNotYetAcknowledged = true;

        SettingsTable clientSettingsTable{};
        SettingsTable serverSettingsTable{};

        //
        // Connection Preface
        //
        std::size_t connectionPrefaceCounter{0};
        bool sentServerSettings{false};
        bool receivedClientSettings{false};

        void ReadConnectionPreface();

        //
        // Streams
        //
        std::map<std::uint32_t, Stream> streams{};

        [[nodiscard]] inline const Stream &
        GetReadonlyStream(std::uint32_t identifier) const {
            static const Stream mockedNewStream{};

            auto it = streams.find(identifier);

            if (it == std::end(streams)) {
                return mockedNewStream;
            }

            return it->second;
        }

        [[nodiscard]] inline Stream &
        GetStream(std::uint32_t identifier) {
            auto it = streams.find(identifier);

            if (it == std::end(streams)) {
                return streams.emplace().first->second;
            }

            return it->second;
        }

        //
        // Generic frame data (i.e. both sending & receiving)
        //
        std::array<char, 9> frameMetadataBuffer{};
        std::uint32_t currentFramePosition{0};

        //
        // Frame Receiving
        //
        Frame currentReceivingFrame{};

        void ReceiveFrameMetadata();
        void ReceiveFramePayload();

        //
        // Frame Transmission
        //
        std::deque<Frame> frameSendQueue{};
        void TrySendingEnqueuedFrames();

        [[nodiscard]] Frame &
        CreateSendFrame(Registry::FrameType);

        void SendRstStream(std::uint32_t streamID, ErrorCode);

        void SendSettingsServer();
        void SendSettingsWithAckFlag();

        void SendPingFrame();
        void SendPingFrameWithAckFlag();

        template <typename It>
        inline void
        SendGoaway(Registry::H2ErrorCode errorCode, It additionalDataBegin, It additionalDataEnd) {
            auto &frame = CreateSendFrame(Registry::FrameType::GOAWAY);
            frame.length = 4 + 4 + static_cast<decltype(frame.length)>(
                    std::distance(additionalDataBegin, additionalDataEnd));

            frame.payload.resize(frame.length);
            Base::Conversions::Destruct<4>(this->lastStreamID, std::data(frame.payload));
            Base::Conversions::Destruct<4>(Registry::ConvertH2ErrorCode(errorCode), std::data(frame.payload) + 4);
            std::copy(additionalDataBegin, additionalDataEnd, std::begin(frame.payload) + 8);

            clientHandlingState = ClientHandlingState::GOAWAY_ENQUEUED;
        }

        [[nodiscard]] std::pair<std::string, bool>
        ParseString(std::size_t &,
                    std::size_t maxLength,
                    ErrorCode errorCodeWhenTooLong);

        //
        // Handling the Frames
        //
        void ProcessHeadersFrame();
        [[nodiscard]] bool ProcessHeadersDynamicTableSizeChange(std::size_t &);
        [[nodiscard]] bool ProcessHeadersIndexedHeaderField(std::size_t &);
        [[nodiscard]] bool ProcessHeadersIncrementalIndexedHeaderFieldNewName(std::size_t &);
        [[nodiscard]] bool ProcessHeadersIncrementalIndexedHeaderFieldIndexedName(std::size_t &);
        [[nodiscard]] bool ProcessHeadersWithoutIndexingIndexedName(std::size_t &);
        [[nodiscard]] bool ProcessHeadersWithoutIndexingNewName(std::size_t &);
        void ProcessSettingsFrame();
        void ProcessSettingsFrameWithAckFlag(const Frame &);
        void ProcessPushPromiseFrame();
        void ProcessPingFrame();
        void ProcessGoawayFrame();

        //
        // Request & Response
        //
        void ProcessRequest();
        [[nodiscard]] bool IsRequestInvalid();

        void SendBadRequestResponse(const ErrorInformation &);

        void ProcessPseudoHeaderPath(std::string_view value);
        void ProcessPseudoHeaderMethod(std::string_view value);

        [[nodiscard]] bool
        AddStatusCodeToHeaderList(std::vector<char> &) const;

    public:
        [[nodiscard]] inline explicit
        Client(Resources::Connection *connection) noexcept
                : connection(connection) {
        }

        ~Client() override = default;

        void Continue() override;

        [[nodiscard]] bool
        SendEmptyResponse() noexcept override;

        [[nodiscard]] bool
        SendResponseFile(int, std::size_t) noexcept override;

        [[nodiscard]] bool
        SendResponseMetadata() noexcept override;

        [[nodiscard]] bool
        SendResponseString(std::string_view) noexcept override;
    };

} // namespace HTTP::v2

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string> // for std::string
#include <utility> // for std::move

namespace HTTP {

    /**
     * The structure for HTTP headers.
     *
     * https://svn.tools.ietf.org/svn/wg/httpbis/specs/rfc7230.html#header.fields
     */
    template<typename StringType>
    struct Header {

        StringType name{};
        StringType value{};

        Header() = default;
        ~Header() = default;
        Header(const Header &) noexcept = default;
        Header(Header &&) noexcept = default;
        Header &operator=(const Header &) noexcept = default;
        Header &operator=(Header &&) noexcept = default;

        template<typename NameType, typename ValueType>
        [[nodiscard]] inline
        Header(const NameType &name, const ValueType &value) noexcept
            : name(name), value(value) {
        }

        template<typename NameType, typename ValueType>
        [[nodiscard]] inline
        Header(NameType &&name, ValueType &&value) noexcept
            : name(std::forward<NameType>(name)), value(std::forward<ValueType>(value)) {
        }


    };

} // namespace HTTP

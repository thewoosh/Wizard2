/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <ostream>
#include <vector>

#include "Message.hpp"
#include "HeaderList.hpp"

namespace HTTP {

    /**
     * A response is an HTTP-message with the start line of a status line.
     *
     * https://svn.tools.ietf.org/svn/wg/httpbis/specs/rfc7230.html#response.line
     */
    struct Response : public Message {

        std::array<char, 3> statusCode{};
        std::string reasonPhrase{};

        Response() = default;
        Response(const Response &) = default;

        template<std::uint8_t A, std::uint8_t B, std::uint8_t C>
        inline void
        SetStatusCode() {
            static_assert(A < 10);
            static_assert(B < 10);
            static_assert(C < 10);

            statusCode[0] = '0' + static_cast<char>(A);
            statusCode[1] = '0' + static_cast<char>(B);
            statusCode[2] = '0' + static_cast<char>(C);
        }

        friend
        std::ostream &
        operator<<(std::ostream &output, const Response &response) {
            output << "Response(status-code="
                   << response.statusCode[0]
                   << response.statusCode[1]
                   << response.statusCode[2]
                   << ", reason-phrase: \"" << response.reasonPhrase
                   << "\")";
            return output;
        }
    };

} // namespace HTTP

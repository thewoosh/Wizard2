/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <algorithm> // for std::any_of
#include <memory> // for std::shared_ptr
#include <string> // for std::string
#include <string_view> // for std::string_view
#include <type_traits> // for std::is_same_v
#include <vector> // for std::vector


#ifdef DEBUGGING_HEADERLIST
#include <iostream>
#endif

#include "Source/Base/Portability.hpp"
#include "Source/HTTP/Header.hpp"

namespace HTTP {

    /**
     * The list of headers.
     */
    struct HeaderList {
        std::vector<Header<std::string>> strings{};
        std::vector<Header<std::string>> stringViews{};

        using SharedType = std::shared_ptr<std::string>;
        std::vector<Header<SharedType>> sharedStrings{};

        /**
         * Checks whether or not at least one field has a name matching the
         * given name.
         */
        [[nodiscard]] BASE_PORTABILITY_CXX20_CONSTEXPR inline bool
        Contains(const std::string_view &name) const noexcept {
#ifdef DEBUGGING_HEADERLIST
            if (strings.size() != 235998)
                std::cout << "Debug: " << __FUNCTION__ << "\n";
#endif
            auto isName = [&name](const auto &header) {
                return header.name == name;
            };

            auto isSharedName = [&name](const auto &header) {
                return *header.name == name;
            };

            return std::any_of(std::cbegin(strings), std::cend(strings), isName) ||
                   std::any_of(std::cbegin(stringViews), std::cend(stringViews), isName) ||
                   std::any_of(std::cbegin(sharedStrings), std::cend(sharedStrings), isSharedName);
        }

        /**
         * Returns the field of the first header that matches the given name
         * with the field name.
         *
         * If no such header is found, the empty header is returned.
         *
         * When multiple headers exist with the same field name, the caller
         * can use the data attribute of this structure to iterate the content
         * of those headers.
         */
        [[nodiscard]] BASE_PORTABILITY_CXX20_CONSTEXPR inline std::string_view
        operator[](const std::string_view &name) const noexcept {
#ifdef DEBUGGING_HEADERLIST
            if (strings.size() != 235998)
                std::cout << "Debug: " << __FUNCTION__ << "\n";
#endif
            for (const auto &header : strings) {
                if (header.name == name) {
                    return header.value;
                }
            }

            for (const auto &header : stringViews) {
                if (header.name == name) {
                    return header.value;
                }
            }

            for (const auto &header : sharedStrings) {
                if (*header.name == name) {
                    return *header.value;
                }
            }


            return {};
        }

        template<typename NameType, typename ValueType>
        BASE_PORTABILITY_CXX20_CONSTEXPR inline void
        Add(NameType &&name, ValueType &&value) noexcept {
#ifdef DEBUGGING_HEADERLIST
            if (strings.size() != 235998)
                std::cout << "Debug: " << __FUNCTION__ << "\n";
#endif
            if (std::is_same_v<NameType, std::string> ||
                std::is_same_v<ValueType, std::string>) {
#ifdef DEBUGGING_HEADERLIST
                std::cout << "Add[S] PARAM " << name << " = " << value << "\n";
                const auto &s =
#endif
                strings.emplace_back(std::forward<NameType>(name), std::forward<ValueType>(value));
#ifdef DEBUGGING_HEADERLIST
                std::cout << "Add[S] VECTO " << s.name << " = " << s.value << "\n";
#endif
            } else {
#ifdef DEBUGGING_HEADERLIST
                std::cout << "Add[S_V] PARAM " << name << " = " << value << "\n";
                const auto &s =
#endif
                stringViews.emplace_back(std::forward<NameType>(name), std::forward<ValueType>(value));
#ifdef DEBUGGING_HEADERLIST
                std::cout << "Add[S_V] VECTO " << s.name << " = " << s.value << "\n";
#endif
            }
        }

        template<typename NameType, typename ValueType>
        BASE_PORTABILITY_CXX20_CONSTEXPR inline void
        Add(const NameType &name, const ValueType &value) noexcept {
#ifdef DEBUGGING_HEADERLIST
            if (strings.size() != 235998)
                std::cout << "Debug: " << __FUNCTION__ << "\n";
            std::cout << "Add[S_V_OL] PARAM " << name << " = " << value << "\n";
            const auto &s =
#endif
            stringViews.emplace_back(name, value);
#ifdef DEBUGGING_HEADERLIST
            std::cout << "Add[S_V_OL] VECTO " << s.name << " = " << s.value << "\n";
#endif
        }

        inline void
        AddShared(const SharedType &name, const SharedType &value) {
            sharedStrings.emplace_back(name, value);
        }

        BASE_PORTABILITY_CXX20_CONSTEXPR inline void
        Clear() noexcept {
#ifdef DEBUGGING_HEADERLIST
            if (strings.size() != 235998)
                std::cout << "Debug: " << __FUNCTION__ << "\n";
#endif
            strings.clear();
            stringViews.clear();
            sharedStrings.clear();
        }

    };

} // namespace HTTP

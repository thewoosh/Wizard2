/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include "Source/HTTP/Request.hpp"
#include "Source/HTTP/Response.hpp"

namespace HTTP {

    class AbstractClient {
    protected:
        Request request{};
        Response response{};
    public:
        virtual ~AbstractClient() = default;

        virtual void
        Continue() = 0;

        [[nodiscard]] virtual bool
        SendResponseMetadata() noexcept = 0;

        /**
         * Some clients need specific behavior when the response didn't contain
         * a response body. In that case, this function is called instead.
         *
         * For example, the response body is omitted when:
         * 1. 204, 3xx is the status code
         * 2. "Content-Length" header is set to 0.
         * 3. The request had method "HEAD".
         */
        [[nodiscard]] virtual inline bool
        SendEmptyResponse() noexcept {
            return true;
        }

        [[nodiscard]] virtual bool
        SendResponseFile(int, std::size_t) noexcept = 0;

        [[nodiscard]] virtual bool
        SendResponseString(std::string_view) noexcept = 0;

        virtual void
        ResetConversation();

        /**
         * Returns whether or not the Host header conforms to the requirements
         * set in the configuration.
         */
        [[nodiscard]] bool
        VerifyHostHeader() const noexcept;

        [[nodiscard]] inline constexpr Request &
        GetRequest() noexcept {
            return request;
        }

        [[nodiscard]] inline constexpr Response &
        GetResponse() noexcept {
            return response;
        }
    };

} // namespace HTTP

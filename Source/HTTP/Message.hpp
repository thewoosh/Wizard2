/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <array> // for std::array
#include <vector> // for std::vector

#include "HeaderList.hpp" // for HeaderList

namespace HTTP {

    struct HTTPVersion {
        std::uint8_t major;
        std::uint8_t minor;
    };

    struct Message {

        HTTPVersion version{1, 1};

        HeaderList headerList{};

        std::vector<char> messageBody{};

        inline void
        SetMessageBody(std::string_view sv) noexcept {
            messageBody.clear();
            std::copy(std::cbegin(sv), std::cend(sv), std::back_inserter(messageBody));
        }

    };

} // namespace HTTP

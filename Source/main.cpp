/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <iostream>

#include <cstring>

#include "Source/Base/Configuration.hpp"
#include "Source/Base/ConfigurationReader.hpp"
#include "Source/Base/ExitCodes.hpp"
#include "Source/Base/Globals.hpp"
#include "Source/Base/Platforms.hpp"
#include "Source/Base/SignalHandler.hpp"
#include "Source/Base/SuggestionEngine.hpp"
#include "Source/Base/Thread.hpp"
#include "Source/HTTP/v2/Huffman.hpp"
#include "Source/Provider/MasterProvider.hpp"
#include "Source/Resources/ServerSocket.hpp"
#include "Source/Resources/ThreadManager.hpp"
#include "Source/Security/PlatformFeatures.hpp"
#include "Source/Security/TLSException.hpp"
#include "Source/Security/TLSManager.hpp"

#define STRTAG "\033[37m[\033[33m\033[1mWizard2\033[0m\033[37m] "

bool flag{true};

[[nodiscard]] Base::Configuration
LoadConfigurationUsingArguments(int, const char *const []) noexcept;

void
ProcessFailure(const Base::BasicFailure &failure) {
    std::cout << STRTAG "\033[37mFailed to setup \033[31m" << failure.module
              << "\033[37m during \033[31m" << failure.task << "\n";
    std::cout << STRTAG "\033[37mWhat went wrong: \033[31m" << failure.message << "\033[0m\n";
}

int main(const int argc, const char *const argv[]) {
    Base::Thread::SetName("MainThread");

    if (!Security::ApplyPlatformFeaturesBeforeConfiguration()) {
        return Base::ExitCodes::SecuritySetupFailure;
    }

    Base::SignalHandler signalHandler{};
    Base::Configuration configuration = LoadConfigurationUsingArguments(argc, argv);
    Base::Globals::configuration = std::addressof(configuration);

    if (configuration.verboseFlag)
        std::cout << STRTAG "Configuration is loaded.\n";

    if (!Security::ApplyPlatformFeaturesAfterConfiguration()) {
        return Base::ExitCodes::SecuritySetupFailure;
    }

    Provider::MasterProvider masterProvider{configuration};
    Base::Globals::masterProvider = std::addressof(masterProvider);

    if (configuration.silentFlag) {
        // TODO is this legal C/C++?
        std::fclose(stdout);
        std::fclose(stderr);
    }

    Base::SuggestionEngine::Run(configuration);

    HTTP::v2::HuffmanCoding huffmanCoding{};
    Base::Globals::huffmanCoding = std::addressof(huffmanCoding);

    std::unique_ptr<Security::TLSManager> manager{};
    if (configuration.tlsConfiguration.enable) {
        manager = std::make_unique<Security::TLSManager>(
                configuration.tlsConfiguration,
                configuration.enabledProtocols
        );
    }

    if (configuration.verboseFlag)
        std::cout << STRTAG "Utilities are ready.\n";

    Resources::ServerSocket socket{
            configuration.serverSocketConfiguration.port,
            configuration.serverSocketConfiguration.backlog
    };

    if (socket.failure) {
        ProcessFailure(socket.failure);
        return Base::ExitCodes::ServerSocketFailure;
    }

    std::cout << STRTAG "Server ready on port: " << configuration.serverSocketConfiguration.port << "\033[0m\n";

    signalHandler.RegisterHook([&socket]() mutable {
        socket.Shutdown();
    });

    Resources::ThreadManager threadManager{
        configuration.threadManagerConfiguration.threadCount
    };

    if (!Security::ApplyPlatformFeaturesAfterInitialization()) {
        return Base::ExitCodes::SecuritySetupFailure;
    }

    std::cout << STRTAG "Initialization passed.\033[0m\n";

    while (flag) {
        auto connection = configuration.tlsConfiguration.enable
                        ? socket.AcceptTLS(manager.get())
                        : socket.Accept();

        if (connection == nullptr) {
            break;
        }

        try {
            threadManager.ScheduleConnection(std::move(connection));
        } catch (const Security::TLSManagerException &exception) {
            // TODO it would be wise to report this exception in the future, if
            //      some verbose flag was toggled in the configuration.
            //      syslog(3) might be interesting
            if (!configuration.silentFlag && configuration.tlsConfiguration.printClientErrors) {
                std::cerr << "TLSexcept: \"" << exception.what() << "\"!\n";
            }
        }
    }

#ifndef NDEBUG
    std::cout << "Stopping because of the following reason:\n    ";
    if (socket.LatestError() < Resources::ServerSocketError::ERRNO_BOUNDARY) {
        std::cout << Resources::toString(socket.LatestError()) << '\n';
    } else {
        std::cout << "Unspecified error!\n    Errno: " <<
                std::strerror(static_cast<int>(socket.LatestError()) -
                              static_cast<int>(Resources::ServerSocketError::ERRNO_BOUNDARY)) << '\n';
    }
#endif

    //
    // To make valgrinds --track-fds happy, close parent fds 0, 1 and 2.
    //

    std::fclose(stdout);
    std::fclose(stdin);
    std::fclose(stderr);

    return Base::ExitCodes::Success;
}

struct ConfigurationError {

    std::ostream &output{std::cerr};

    ConfigurationError() noexcept {
        output << "Configuration Error:\n";
    }

    ConfigurationError(ConfigurationError &&) = delete;
    ConfigurationError(const ConfigurationError &) = delete;

    [[noreturn]] ~ConfigurationError() {
        output << std::endl;
        std::exit(Base::ExitCodes::InvalidConfiguration);
    }

    template<typename T>
    ConfigurationError &
    operator<<(T t) {
        output << std::forward<T>(t);
        return *this;
    }

};

Base::Configuration
LoadConfigurationUsingArguments(const int argc, const char *const argv[]) noexcept {
    Base::Configuration config{};

#ifdef BASE_PLATFORM_BSD
    std::string_view configurationFile{"/usr/local/etc/wizard2"};
#elif defined (BASE_PLATFORM_LINUX)
    std::string_view configurationFile{"/etc/wizard2"};
#else
    std::string_view configurationFile{};
#endif

    char *configurationFileFromEnvironment = std::getenv("WIZARD2_CONFIG");
    if (configurationFileFromEnvironment) {
        configurationFile = configurationFileFromEnvironment;
    }

    //
    // Argument parsing
    //

    bool silentFlagOverride{false};
    bool verboseFlagOverride{false};

    if (argc > 0) {
        for (std::size_t i = 1; i < static_cast<std::size_t>(argc); i++) {
            std::string_view string{argv[i]};
            if (string.starts_with('-') && string.length() == 2) {
                switch (string[1]) {
                    case 'c': // configuration file flag
                        configurationFile = argv[++i];
                        break;
                    case 's': // silent flag
                        silentFlagOverride = true;
                        break;
                    case 'v': // verbose flag
                        verboseFlagOverride = true;
                        break;
                    default:
                        ConfigurationError() << "Unknown argument: " << string;
                }
            }
        }
    }

    if (!std::empty(configurationFile)) {
        const auto optionalConfiguration = Base::ConfigurationReader::ReadConfiguration(configurationFile);
        if (!optionalConfiguration.has_value()) {
            ConfigurationError() << "Failed to load configuration file!";
        }

        config = std::move(optionalConfiguration.value());
    }

    if (silentFlagOverride) {
        config.silentFlag = true;
        config.verboseFlag = false;
    }

    if (verboseFlagOverride) {
        config.silentFlag = false;
        config.verboseFlag = true;
    }

    //
    // Configuration Validation
    //

    if ((config.silentFlag && config.verboseFlag) || (verboseFlagOverride && silentFlagOverride)) {
        ConfigurationError() << "Silent mode and verbose mode can't be both enabled!";
    }

    if (!config.enabledProtocols.http1 &&
            !config.enabledProtocols.http2) {
        ConfigurationError() << "No protocol specified!";
    }

    if (config.hostname == "system") {
#ifdef BASE_PLATFORM_POSIX
        config.hostname.resize(255);
        gethostname(std::data(config.hostname), config.hostname.capacity());
        config.hostname.shrink_to_fit();
#else
        config.hostname = "";
#endif
    }

    return config;
}

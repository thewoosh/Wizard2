/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Base/SuggestionEngine.hpp"

#include <cstdio> // for std::printf

#include <iostream> // for std::cout
#include <stdexcept> // for std::runtime_error
#include <string> // for std::to_string

#include <fmt/format.h>

#ifdef __unix__
#   include <unistd.h>
#   include <sys/stat.h>
#   include <pwd.h>
#endif

#define WARNING_TAG "\x1b[33m[WARNING] "

static void
RunGeneric(const Base::Configuration &);

static void
RunForDebugging(const Base::Configuration &);

static void
RunForProduction(const Base::Configuration &);

void
Base::SuggestionEngine::Run(const Base::Configuration &configuration) {
    auto suggestionLevel = configuration.suggestionLevel;

    if (suggestionLevel == SuggestionLevel::INITIAL) {
        std::puts(WARNING_TAG"Unknown suggestion level! Please configure \x1b[32msuggestions\x1b[33m to "
                             "\x1b[32mdebugging\x1b[33m, "
                             "\x1b[32mproduction\x1b[33m or "
                             "\x1b[32moff\x1b[33m.\x1b[0m");
        suggestionLevel = SuggestionLevel::DEBUGGING;
    }

    RunGeneric(configuration);

    switch (suggestionLevel) {
        case SuggestionLevel::INITIAL:
        case SuggestionLevel::OFF:
            break;
        case SuggestionLevel::DEBUGGING:
            RunForDebugging(configuration);
            break;
        case SuggestionLevel::PRODUCTION:
            RunForProduction(configuration);
            break;
        default:
            throw std::runtime_error("Unknown suggestion level: " +
                    std::to_string(static_cast<std::size_t>(suggestionLevel)));
    }
}

#ifdef __unix__
static void
FindOutRootDirectoryAccessProblem(const Base::Configuration &configuration) {
    const auto errorCode = errno;

    switch (errorCode) {
        case EACCES: {
            std::cout << "\x1b[33m[WARNING] The \x1b[32mroot"
                         "\x1b[33m directory is not accessible by Wizard: \x1b[32m"
                      << configuration.rootDirectory << "\x1b[33m.\x1b[0m\n"
                      << std::flush; // for {fmt} interoperability

            struct stat status{};
            if (stat(configuration.rootDirectory.c_str(), &status) == 0) {
                const auto uid = geteuid();
                if (status.st_uid != uid) {
                    std::string ourUsername{getpwuid(uid)->pw_name};
                    std::string owningUsername{getpwuid(status.st_uid)->pw_name};
                    fmt::print("\x1b[33m          => The directory is owned by \"{}\", but Wizard2 is running as "
                               "\"{}\".\n             Please make the directory readable for Wizard using e.g. "
                               "\x1b[32mchmod\x1b[33m.\x1b[0m\n",
                                owningUsername, ourUsername);
                    return;
                }
            }
            std::cout << "\x1b[33m          => System Error: " << strerror(errorCode) << "\x1b[0m\n";
            break;
        }
        case ENOENT:
            fmt::print("\x1b[33m[WARNING] The \x1b[32mroot\x1b[33m directory does not exist: \x1b[32m{}\x1b[33m.\x1b[0m\n",
                       configuration.rootDirectory);
            break;
        case ENAMETOOLONG:
            fmt::print("\x1b[33m[WARNING] The \x1b[32mroot\x1b[33m directory specified is too long for your system: \x1b[32m{}\x1b[33m.\x1b[0m\n",
                       configuration.rootDirectory);
            break;
        default:
            std::cout << "\x1b[33m[WARNING] The \x1b[32mroot"
                         "\x1b[33m directory is not accessible by Wizard: \x1b[32m"
                      << configuration.rootDirectory << "\x1b[33m."
                         "\x1b[33m          => System Error: " << strerror(errorCode) << "\x1b[0m\n";
            break;
    }
}
#endif

static void
RunGeneric(const Base::Configuration &configuration) {
#ifdef __unix__
    if (access(configuration.rootDirectory.c_str(), R_OK) == -1) {
        FindOutRootDirectoryAccessProblem(configuration);
    }
#endif
}

static void
RunForDebugging(const Base::Configuration &) {

}

static void RunForProduction(const Base::Configuration &config) {
    if (config.requestLimits.maximumHeaderNameLength < 25) {
        std::cout << "\x1b[33m[WARNING] \x1b[32mrequest-limits.maximum-header-name-length"
                     "\x1b[33m is less than the suggested minimum of 25; it is \x1b[32m"
                  << config.requestLimits.maximumHeaderNameLength << "\x1b[33m.\n"
                     "          => Some header names are naturally become long, like Strict-Transport-Security header."
                     "\x1b[0m\n";
    }

    if (config.requestLimits.maximumHeaderValueLength < 50) {
        std::cout << "\x1b[33m[WARNING] \x1b[32mrequest-limits.maximum-header-value-length"
                     "\x1b[33m is less than the suggested minimum of 50; it is \x1b[32m"
                  << config.requestLimits.maximumHeaderValueLength << "\x1b[33m.\n"
                     "          => Some header values can naturally become long, like the Cookie header."
                     "\x1b[0m\n";
    }
}

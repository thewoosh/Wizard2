/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 */

#pragma once

namespace Characters {

    /**
     * Is the input a visible character?
     *
     * RFC 5234 Appendix B.1
     * https://www.rfc-editor.org/rfc/rfc5234.html#appendix-B.1
     */
    [[nodiscard]] inline constexpr bool
    IsVChar(char input) noexcept {
        return input >= 0x21 && input <= 0x7E;
    }

    /**
     * Is the input a delimiter as per the HTTP specification?
     *
     * RFC 7230 3.2.6. Field Value Components
     * https://svn.tools.ietf.org/svn/wg/httpbis/specs/rfc7230.html#field.components
     */
    [[nodiscard]] inline constexpr bool
    IsDelimiter(char input) noexcept {
        switch (input) {
            case '"':
            case '(':
            case ')':
            case ',':
            case '/':
            case ':':
            case ';':
            case '<':
            case '=':
            case '>':
            case '?':
            case '@':
            case '[':
            case '\\':
            case ']':
            case '{':
            case '}':
                return true;
            default:
                return false;
        }
    }

    /**
     * Is the input a tchar (i.e. token character)?
     *
     * RFC 7230 3.2.6. Field Value Components
     * https://svn.tools.ietf.org/svn/wg/httpbis/specs/rfc7230.html#field.components
     */
    [[nodiscard]] inline constexpr bool
    IsTChar(char input) noexcept {
        return IsVChar(input) && !IsDelimiter(input);
    }

    [[nodiscard]] inline constexpr std::uint8_t
    ConvertCharacterToDigit(char input) noexcept {
        return static_cast<std::uint8_t>(input - '0');
    }

    template<typename T>
    [[nodiscard]] inline constexpr char
    ConvertDigitToCharacter(T value) noexcept
            requires(std::is_integral_v<T> && !std::is_same_v<T, bool>) {
        return static_cast<char>('0' + value);
    }

} // namespace Characters

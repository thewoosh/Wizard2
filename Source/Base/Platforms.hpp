/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * This header defines macros depending on the platform the code is being
 * compiled on.
 *
 * Depends heavily on the http://predef.sf.net/ site.
 */

#pragma once

//
// BSDs Section
//

#ifdef __bsdi__
    #define BASE_PLATFORM_BSDI
#endif

#ifdef __DragonFly__
    #define BASE_PLATFORM_DRAGONFLYBSD
#endif

#ifdef __FreeBSD__
    #define BASE_PLATFORM_FREEBSD
#endif

#ifdef __NetBSD__
    #define BASE_PLATFORM_NETBSD
#endif

#ifdef __OpenBSD__
    #define BASE_PLATFORM_OPENBSD
#endif

#if defined (BASE_PLATFORM_BSDI) || defined (BASE_PLATFORM_DRAGONFLYBSD) || \
        defined (BASE_PLATFORM_FREEBSD) || defined (BASE_PLATFORM_NETBSD) || \
        defined (BASE_PLATFORM_OPENBSD)
    #define BASE_PLATFORM_BSD
#endif

//
// Other Platforms
//

#if defined (__linux__) || defined (__linux) || defined (linux)
    #define BASE_PLATFORM_LINUX
#endif

#if defined (macintosh) || defined (Macintosh) || \
        (defined (__APPLE__) && defined (__MACH__))
    #define BASE_PLATFORM_MACOS
#endif

#if defined (_WIN16) || defined (_WIN32) || defined (_WIN64) || \
        defined (__WINDOWS__)
    #define BASE_PLATFORM_WINDOWS
#endif

//
// POSIX/UNIX Section
//

#if defined (__unix__) || defined (__unix) || defined (unix)
    #define BASE_PLATFORM_UNIX
#endif

#if defined (BASE_PLATFORM_BSD) || \
        defined (BASE_PLATFORM_LINUX) || \
        defined (BASE_PLATFORM_MACOS) || \
        defined (BASE_PLATFORM_UNIX)
    #define BASE_PLATFORM_POSIX
#endif

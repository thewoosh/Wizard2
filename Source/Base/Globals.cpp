/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Base/Globals.hpp"

namespace Base::Globals {

    Base::Configuration *configuration{nullptr};
    Provider::MasterProvider *masterProvider{nullptr};
    HTTP::v2::HuffmanCoding *huffmanCoding{nullptr};

} // namespace Base::Globals

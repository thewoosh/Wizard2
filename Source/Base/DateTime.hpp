/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include <ctime>

#include "Source/Base/Platforms.hpp"

#ifdef BASE_PLATFORM_UNIX
#    include <syslog.h>
#endif

namespace Base::DateTime {

    /**
     * Generates a string that contains the current date as formatted by the
     * rules of an HTTP date.
     *
     * The HTTP date format is defined in RFC 7231, Section 7.1.1.1.
     * https://tools.ietf.org/html/rfc7231#section-7.1.1.1
     */
    [[nodiscard]] inline std::string
    CreateHTTPDate(time_t time = std::time(nullptr)) noexcept {
        // "Fri, 01 Jan 2021 12:34:56 GMT" = 29 characters. The only thing that
        // isn't future proof is %Y, which, in the year 10000, will be
        // incorrectly assumed to be 4 characters long.

        constexpr std::size_t maxSize = 29;

        std::string buffer(maxSize, '\0');

        auto ret = std::strftime(buffer.data(),
                                 maxSize + 1, // +1 for null-character
                                 "%a, %d %h %Y %T GMT",
                                 std::localtime(&time));

        if (ret == 0) {
#ifdef BASE_PLATFORM_UNIX
            static std::size_t reportCount = 0;

            if (reportCount < 5) {
                ++reportCount;
                syslog(LOG_ALERT, "strftime(3) failed with ret=0");
            }
#endif
            // this means the buffer was too small. In this case, return nothing.
            return {};
        }

        return buffer;
    }

} // namespace Base::DateTime

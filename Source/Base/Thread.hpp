/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string_view>
#include <thread>

#include "Source/Base/Platforms.hpp"

#ifdef BASE_PLATFORM_POSIX
#   include <pthread.h>
#   ifdef _GNU_SOURCE
#       define BASE_PLATFORM_THREAD_SETNAME pthread_setname_np
#   endif
#endif

#ifdef BASE_PLATFORM_OPENBSD
#   include <pthread_np.h>
#   define BASE_PLATFORM_THREAD_SETNAME pthread_set_name_np
#endif

#ifdef BASE_PLATFORM_FREEBSD
#   include <pthread_np.h>
#   define BASE_PLATFORM_THREAD_SETNAME pthread_setname_np
#endif

namespace Base::Thread {

    /**
     * Sets the name of the thread when possible.
     */
    inline void
    SetName(std::string_view name) noexcept {
#ifdef BASE_PLATFORM_THREAD_SETNAME
        BASE_PLATFORM_THREAD_SETNAME(pthread_self(), std::data(name));
#else
#       pragma warning "pthread_setname_np not available on this platform."
        static_cast<void>(name);
#endif
    }

} // namespace Base::Thread

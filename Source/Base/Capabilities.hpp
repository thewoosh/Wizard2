/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 *
 */

#pragma once

#ifdef TLS_USE_OPENSSL
    #include <openssl/ssl.h>

    #if defined (OPENSSL_VERSION_MAJOR) && OPENSSL_VERSION_MAJOR == 3 && BIO_get_ktls_send != 0
        #define TLS_CAP_KTLS
    #endif
#endif // TLS_USE_OPENSSL

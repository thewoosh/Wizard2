/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * GCC can complain in the following situation:
 *
 * #define FUNCTION(...) 0
 * FUNCTION(1, 2, 3);
 *
 * which will translate to 0(1, 2, 3) which has no effect. We want it to have no
 * effect, and be optimized away. Thus instead of using 0 on line 11, provide
 * DoNothing instead:
 *
 * #define FUNCTION(...) DoNothing(__VA_ARGS__)
 * FUNCTION(1, 2, 3);
 */

#pragma once

static inline constexpr void
DoNothing(...) noexcept {}

/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

namespace Base {

    struct IPAddress {
        [[nodiscard]] inline static constexpr IPAddress
        CreateIPv4(const std::uint8_t data[4]) {
            IPAddress address;
            address.m_is4 = true;
            std::copy(data, data + 4, std::begin(address.m_ipv4));
            return address;
        }

        [[nodiscard]] inline static constexpr IPAddress
        CreateIPv6(const std::uint16_t data[8]) {
            IPAddress address;
            address.m_is4 = false;
            std::copy(data, data + 8, std::begin(address.m_ipv6));
            return address;
        }

        [[nodiscard]] inline constexpr const std::array<std::uint8_t, 4> &
        GetIPv4() const {
            return m_ipv4;
        }

        [[nodiscard]] inline constexpr const std::array<std::uint16_t, 8> &
        GetIPv6() const {
            return m_ipv6;
        }

        [[nodiscard]] inline constexpr bool
        IsIPv4() const {
            return m_is4;
        }

        [[nodiscard]] inline constexpr bool
        IsIPv6() const {
            return !m_is4;
        }

        [[nodiscard]] inline constexpr bool
        IsLocalhost() const {
            if (m_is4) {
                return (m_ipv4[0] == 0x00 && m_ipv4[1] == 0x00 && m_ipv4[2] == 0x00 && m_ipv4[3] == 0x00)
                    || (m_ipv4[0] == 0x7F && m_ipv4[1] == 0x00 && m_ipv4[2] == 0x00 && m_ipv4[3] == 0x01);
            }

            if (m_ipv6[0] != 0x0000 || m_ipv6[1] != 0x0000 || m_ipv6[2] != 0x0000 || m_ipv6[3] != 0x0000
                    || m_ipv6[4] != 0x0000)
                return false;

            if (m_ipv6[5] == 0x0000 && m_ipv6[6] == 0x0000 && m_ipv6[8] == 0x0000)
                return true;

            return m_ipv6Alt[10] == 0xFF && m_ipv6Alt[11] == 0xFF && m_ipv6Alt[12] == 0x7F && m_ipv6Alt[13] == 0x00
                    && m_ipv6Alt[14] == 0x00 && m_ipv6Alt[15] == 0x01;
        }

    private:
        union {
            std::array<std::uint8_t, 4> m_ipv4;
            std::array<std::uint16_t, 8> m_ipv6{};
            std::array<std::uint8_t, 16> m_ipv6Alt;
        };

        bool m_is4{};
    };

} // namespace Base

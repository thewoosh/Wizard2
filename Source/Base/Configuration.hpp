/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Base/Configuration/EnabledProtocols.hpp"
#include "Base/Configuration/HTTPSettings.hpp"
#include "Base/Configuration/HTTP2Settings.hpp"
#include "Base/Configuration/RequestLimits.hpp"
#include "Base/Configuration/SecuritySettings.hpp"
#include "Base/Configuration/ServerSocketConfiguration.hpp"
#include "Base/Configuration/SuggestionLevel.hpp"
#include "Base/Configuration/ThreadManagerConfiguration.hpp"
#include "Base/Configuration/TLSConfiguration.hpp"

namespace Base {

    struct Configuration {

        EnabledProtocols enabledProtocols{};
        HTTPSettings httpSettings{};
        HTTP2Settings http2Settings{};
        RequestLimits requestLimits{};
        ServerSocketConfiguration serverSocketConfiguration{};
        ThreadManagerConfiguration threadManagerConfiguration{};
        TLSConfiguration tlsConfiguration{};

        SecuritySettings securitySettings{};

        // When value is "system", it will try to get the value from the system
        // using,  for  example,  gethostname(2).  When  the  system  failed to
        // provide an hostname, it will be set to the empty string.
        //
        // When this value is set to the empty string, all rules about the host
        // verification (RFC 7230 § 5.4) will be skipped.
        std::string hostname{"system"};

        std::string rootDirectory{"/var/www/html"};

        //
        // General Flags
        //
        bool silentFlag{false};
        bool verboseFlag{false};
        bool logRequests{false};

        SuggestionLevel suggestionLevel{SuggestionLevel::INITIAL};

    };

} // namespace Base

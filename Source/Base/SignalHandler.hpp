/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <vector>
#include "Source/Base/Portability.hpp"

namespace Base {

    class SignalHandler {
        std::vector<std::function<void()>> hooks{};
    public:
        bool wasSignalFired{false};

        SignalHandler();

        inline BASE_PORTABILITY_CXX20_CONSTEXPR void
        RegisterHook(const std::function<void()> &hook) noexcept {
            hooks.push_back(hook);
        }

        [[nodiscard]] inline constexpr const
        std::vector<std::function<void()>> &
        Hooks() const noexcept {
            return hooks;
        }

        [[nodiscard]] inline constexpr bool
        WasFired() const noexcept {
            return wasSignalFired;
        }
    };

} // namespace Base

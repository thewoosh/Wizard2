/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Base { struct Configuration; }
namespace Provider { class MasterProvider; }
namespace Resources { class ThreadManager; }
namespace HTTP::v2 { class HuffmanCoding; }

namespace Base::Globals {

    extern Base::Configuration *configuration;
    extern Provider::MasterProvider *masterProvider;
    extern HTTP::v2::HuffmanCoding *huffmanCoding;

} // namespace Base::Globals

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

// C++20  changed  functions to  allow for almost any component of the standard
// library  to  be  constexpr.  Clang  supports this feature greatly, but other
// compilers still have some work to do.
#ifdef __clang__
#    define BASE_PORTABILITY_CXX20_CONSTEXPR constexpr
#    define BASE_PORTABILITY_CXX20_CONSTEXPR_STATUS 1
#else
#    define BASE_PORTABILITY_CXX20_CONSTEXPR
#    define BASE_PORTABILITY_CXX20_CONSTEXPR_STATUS 0
#endif

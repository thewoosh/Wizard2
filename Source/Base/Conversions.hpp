/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <type_traits>

#include <cstdint>

namespace Base::Conversions {

    /**
     * Construct an integer from an array.
     *
     * IntegerType should be unsigned.
     * Octets should be the size of the IntegerType. It can't be inferred from
     * sizeof(...) since sizeof(unsigned int : 24) returns 4, not 3.
     */
    template<typename IntegerType, std::size_t Octets, typename It>
    [[nodiscard]] inline constexpr IntegerType
    Construct(It it) noexcept
            requires(std::is_unsigned_v<IntegerType> &&
                     sizeof(IntegerType) >= Octets) {
        IntegerType val = 0;

        for (std::size_t i = 1; i <= Octets; ++i) {
            val += static_cast<IntegerType>(((*it++) & 0x000000FF) << ((Octets - i) * 8));
        }

        return val;
    }

    /**
     * Destructs an integer into an array.
     *
     * IntegerType should be unsigned.
     * Octets should be the size of the IntegerType. It can't be inferred from
     * sizeof(...) since sizeof(unsigned int : 24) returns 4, not 3.
     */
    template<std::size_t Octets, typename IntegerType, typename It>
    inline void
    Destruct(IntegerType value, It destination) noexcept
    requires(std::is_unsigned_v<IntegerType> &&
             sizeof(IntegerType) >= Octets) {
        using DestinationType = std::remove_cvref_t<decltype(*destination)>;

        *(destination + Octets - 1) = static_cast<DestinationType>(value & 0xff);

        for (long i = Octets - 1; i > 0; --i) {
            *(destination + i - 1) = static_cast<DestinationType>(value >>= 8);
        }
    }

}

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace Base {

    struct BasicFailure {

        /**
         * The name of the component where the failure occurred.
         */
        std::string_view module{};

        /**
         * The name of the task that failed.
         */
        std::string task{};

        /**
         * A message describing what went wrong.
         */
        std::string message{};

        BasicFailure(const BasicFailure &) noexcept = default;
        BasicFailure(BasicFailure &&) noexcept = default;
        BasicFailure &operator=(const BasicFailure &) noexcept = default;
        BasicFailure &operator=(BasicFailure &&) noexcept = default;

        [[nodiscard]] inline
        BasicFailure(std::string_view module = "", std::string &&task = "",
                     std::string &&message = "") noexcept
                     : module(module)
                     , task(std::move(task))
                     , message(std::move(message)) {
        }

        /**
         * @returns true when this instance isn't empty.
         */
        [[nodiscard]] inline explicit
        operator bool() const noexcept {
            return !std::empty(message);
        }

    };

} // namespace Base

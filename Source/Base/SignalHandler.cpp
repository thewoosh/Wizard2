/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "SignalHandler.hpp"

#include <csignal>
#include <stdexcept>

//
// Global variable because using contexts / user pointers is difficult.
//
static Base::SignalHandler *instance = nullptr;

void
SignalTarget(int) {
    if (!instance) {
        // Best thing to do is to handle it by the default action
        std::raise(SIGUSR1);
        return;
    }

    instance->wasSignalFired = true;

    for (const auto &hookFunction : instance->Hooks()) {
        hookFunction();
    }
}

Base::SignalHandler::SignalHandler() {
    if (instance) {
        throw std::runtime_error("Base::SignalHandler constructed twice!");
    }

    instance = this;

    std::signal(SIGPIPE, SIG_IGN);
    std::signal(SIGTERM, SignalTarget);
    std::signal(SIGINT,  SignalTarget);
}

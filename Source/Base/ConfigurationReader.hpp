/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <string_view>

#include "Source/Base/Configuration.hpp"

namespace Base::ConfigurationReader {

    [[nodiscard]] std::optional<Configuration>
    ReadConfiguration(std::string_view path) noexcept;

} // namespace Base::ConfigurationReader

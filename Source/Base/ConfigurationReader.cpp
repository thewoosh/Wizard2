/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <algorithm>
#include <array>
#include <fstream>
#include <limits>
#include <map>
#include <string>
#include <string_view>

#include <cstdio>

#include "Source/Base/ConfigurationReader.hpp"
#include "Source/Base/Platforms.hpp"

#ifdef BASE_PLATFORM_UNIX
#    include <unistd.h>
#endif

namespace Base::ConfigurationReader {

    bool useColors{false};

    namespace Messages {

        void
        EmptyValue(const char *formalName, std::string_view key) {
            if (useColors) {
                std::printf("\x1b[37mThe \x1b[36m%s\x1b[37m(\x1b[32m%s\x1b[37m) shouldn't be empty!\n",
                            formalName, std::string(key).c_str());
            } else {
                std::printf("The %s(%s) shouldn't be empty!\n", formalName, std::string(key).c_str());
            }
        }

        void
        ConfigFileNotFound(std::string_view path) {
            if (useColors) {
                std::printf("\x1b[31mConfiguration file couldn't be found!\n"
                            "    \x1b[37mPath: \x1b[36m%s\x1b[0m\n", std::string(path).c_str());
            } else {
                std::printf("Configuration file couldn't be found!\n"
                            "    Path: %s\n", std::string(path).c_str());
            }
        }

        void
        InvalidInteger(const char *formalName, std::string_view name, std::string_view value) {
            if (useColors) {
                std::printf("\x1b[37mThe \x1b[36m%s\x1b[37m(\x1b[32m%s\x1b[37m) has an invalid value!\n",
                            formalName, std::string(name).c_str());
                std::printf("    \x1b[37mValue: \x1b[36m%s\x1b[0m\n",
                            std::string(value).c_str());
            } else {
                std::printf("The %s(%s) has an invalid value!\n",
                            formalName, std::string(name).c_str());
                std::printf("    Value: %s\n",
                            std::string(value).c_str());
            }
        }

        template<std::size_t Size>
        void
        UnknownValue(const char *formalName, std::string_view name, std::string_view value,
                  const std::array<const char *, Size> &expectedValue) {
            if (useColors) {
                std::printf("\x1b[37mThe \x1b[36m%s\x1b[37m(\x1b[32m%s\x1b[37m) has an unknown value!\n",
                            formalName, std::string(name).c_str());
                std::printf("    \x1b[37mValue given was: \x1b[36m%s\n", std::string(value).c_str());
                std::fputs("    \x1b[37mExpected one of the following: \x1b[36m", stdout);
                std::size_t flag = 0;
                for (const char *expVal : expectedValue) {
                    if (flag++)
                        std::fputs("\x1b[37m, \x1b[36m", stdout);
                    std::fputs(expVal, stdout);
                }
                std::putchar('\n');
            } else {
                std::printf("The %s(%s) has an unknown value!\n", formalName, std::string(name).c_str());
                std::printf("    Value given was: %s\n", std::string(value).c_str());
                std::fputs("    Expected one of the following: ", stdout);
                std::size_t flag = 0;
                for (const char *expVal : expectedValue) {
                    if (flag++)
                        std::fputs(", ", stdout);
                    std::fputs(expVal, stdout);
                }
                std::putchar('\n');
            }
        }

    } // namespace Messages

    #define ADD_SUBROUTINE(name)                         \
            [[nodiscard]] bool name(Base::Configuration &configuration, \
                      std::string_view name,               \
                      std::string_view value) noexcept

    ADD_SUBROUTINE(ApplyGenericLine);
    ADD_SUBROUTINE(ApplyTLSLine);
    ADD_SUBROUTINE(ApplyServerSocketLine);
    ADD_SUBROUTINE(ApplyLoggingLine);
    ADD_SUBROUTINE(ApplyHTTPLine);
    ADD_SUBROUTINE(ApplyHTTP2Line);
    ADD_SUBROUTINE(ApplyRequestLimits);
    ADD_SUBROUTINE(ApplySecurityLine);

    [[nodiscard]] bool
    ProcessExplainLevel(Base::Configuration &, std::string_view) noexcept;

    [[nodiscard]] bool
    ProcessProtocolList(Base::Configuration &, std::string_view);

    [[nodiscard]] SuggestionLevel
    ProcessSuggestions(std::string_view);

    [[nodiscard]] constexpr std::string_view
    TrimStringView(std::string_view) noexcept;

    [[nodiscard]] std::optional<bool>
    ResolveToggle(const char *formalName, std::string_view name, std::string_view value) noexcept;

    template<typename T>
    [[nodiscard]] auto
    ConvertInteger(const std::string &source)
    requires(std::is_unsigned_v<T>) {
        return std::stoull(source);
    }

    template<typename T>
    [[nodiscard]] auto
    ConvertInteger(const std::string &source)
    requires(std::is_signed_v<T>) {
        return std::stoll(source);
    }

    template<typename T>
    [[nodiscard]] bool
    ApplyInteger(T &dst, std::string_view source) noexcept {
        std::string stringCopy{source};

        const auto value = ConvertInteger<T>(stringCopy);

        if (value > std::numeric_limits<T>::max()
                || value < std::numeric_limits<T>::min()) {
            return false;
        }

        dst = static_cast<T>(value);
        return true;
    }

    std::optional<Base::Configuration>
    ReadConfiguration(std::string_view path) noexcept {
#ifdef BASE_PLATFORM_UNIX
        useColors = isatty(fileno(stdout));
#endif

        Base::Configuration configuration{};

        std::ifstream fileStream(path.data());

        if (!fileStream) {
            Messages::ConfigFileNotFound(path);
            return std::nullopt;
        }

        std::string line{};

        std::string_view name{};
        std::string_view value{};

#define STRING_ASSIGN_FUNCTION(formalName, configurationAddress) \
        [&] (std::string_view name, std::string_view value) -> bool { \
            if (value.empty()) { \
                Messages::EmptyValue(formalName, name); \
                return false; \
            } \
            configuration.configurationAddress = value;\
            return true; \
        }

#define BOOLEAN_ASSIGN_FUNCTION(formalName, configurationAddress) \
        [&] (std::string_view name, std::string_view value) -> bool { \
            const auto toggle = ResolveToggle(formalName, name, value); \
            if (!toggle.has_value()) { \
                return false; \
            } \
            configuration.configurationAddress = toggle.value(); \
            return true; \
        }

#define INTEGER_ASSIGN_FUNCTION(formalName, configurationAddress) \
        [&] (std::string_view name, std::string_view value) -> bool { \
            if (!ApplyInteger(configuration.configurationAddress, value)) { \
                Messages::InvalidInteger(formalName, name, value); \
                return false; \
            } \
            return true; \
        }

        const std::map<std::string_view, std::function<bool (std::string_view, std::string_view)>> functionMap{
            {"job-dispatchers", INTEGER_ASSIGN_FUNCTION("Job Dispatchers (e.g. no. of threads)", threadManagerConfiguration.threadCount)},

            {"hostname", STRING_ASSIGN_FUNCTION("Hostname", hostname)},
            {"root", STRING_ASSIGN_FUNCTION("Root Directory", rootDirectory)},

            {"http.allow-missing-host-header", BOOLEAN_ASSIGN_FUNCTION("Skip Host Validation Toggle", httpSettings.skipHostValidation)},
            {"http.skip-host-validation", BOOLEAN_ASSIGN_FUNCTION("Allow Missing \"Host\"-Header Toggle", httpSettings.allowMissingHostHeader)},

            {"http2.debug-printing.errors", BOOLEAN_ASSIGN_FUNCTION("HTTP/2 Print Errors in Debug-Mode Toggle", http2Settings.debugPrintErrors)},
            {"http2.debug-printing.header-field.types", BOOLEAN_ASSIGN_FUNCTION("HTTP/2 Print the Header-Field Types in Debug-Mode Toggle", http2Settings.debugPrintHeaderFieldTypes)},
            {"http2.debug-printing.header-field.verbose", BOOLEAN_ASSIGN_FUNCTION("HTTP/2 Print the Header-Field Types Verbosely in Debug-Mode Toggle", http2Settings.debugPrintHeaderFieldVerbose)},
            {"http2.debug-printing.incoming-frames", BOOLEAN_ASSIGN_FUNCTION("HTTP/2 Print the Incoming Frames in Debug-Mode Toggle", http2Settings.debugPrintIncomingFrames)},
            {"http2.debug-printing.outgoing-frames", BOOLEAN_ASSIGN_FUNCTION("HTTP/2 Print the Outgoing Frames in Debug-Mode Toggle", http2Settings.debugPrintOutgoingFrames)},
            {"http2.debug-printing.requests", BOOLEAN_ASSIGN_FUNCTION("HTTP/2 Print the Requests in Debug-Mode Toggle", http2Settings.debugPrintRequestsAfterHandling)},
            {"http2.debug-printing.settings", BOOLEAN_ASSIGN_FUNCTION("HTTP/2 Print the Settings in Debug-Mode Toggle", http2Settings.debugPrintSettings)},

            {"logging.silent", BOOLEAN_ASSIGN_FUNCTION("Silent (No Logging) Toggle", silentFlag)},
            {"logging.verbose", BOOLEAN_ASSIGN_FUNCTION("Verbose Logging Toggle", verboseFlag)},
            {"logging.printTLSClientErrors", BOOLEAN_ASSIGN_FUNCTION("Print TLS Client Errors Toggle", silentFlag)},
            {"log-requests", BOOLEAN_ASSIGN_FUNCTION("Request Logging Toggle", logRequests)},

            {"product-name", STRING_ASSIGN_FUNCTION("Product Name", httpSettings.server)},

            {"request-limits.maximum-header-name-length", INTEGER_ASSIGN_FUNCTION("Maximum Header-Name Length", requestLimits.maximumHeaderNameLength)},
            {"request-limits.maximum-header-value-length", INTEGER_ASSIGN_FUNCTION("Maximum Header-Value Length", requestLimits.maximumHeaderValueLength)},

            {"security.path-verification.realpath", BOOLEAN_ASSIGN_FUNCTION("\"realpath\" Path-Verification Toggle", securitySettings.verifyPathUsingRealpath)},
            {"security.path-verification.folder-escape-detection", BOOLEAN_ASSIGN_FUNCTION("\"Folder Escape Detection\" Path-Verification Toggle", securitySettings.verifyPathUsingFolderEscapeDetection)},

            {"server-socket.port", INTEGER_ASSIGN_FUNCTION("Server Port", serverSocketConfiguration.port)},
            {"server-socket.backlog", INTEGER_ASSIGN_FUNCTION("Server Listen Backlog", serverSocketConfiguration.port)},

            {"tls", BOOLEAN_ASSIGN_FUNCTION("TLS Functionality Toggle", tlsConfiguration.enable)},
            {"tls.certificate", STRING_ASSIGN_FUNCTION("TLS Certificate Path", tlsConfiguration.certificateFile)},
            {"tls.privateKey", STRING_ASSIGN_FUNCTION("TLS Private Key Path", tlsConfiguration.privateKeyFile)},
            {"tls.chain", STRING_ASSIGN_FUNCTION("TLS Certificate Chain Path", tlsConfiguration.certificateChainFile)},
            {"tls.ciphers", STRING_ASSIGN_FUNCTION("TLS Cipher List", tlsConfiguration.ciphers)},
            {"tls.cipherSuites", STRING_ASSIGN_FUNCTION("TLS 1.3 Cipher Suites List", tlsConfiguration.cipherSuites)},

            {"protocols", [&] (std::string_view, std::string_view value) -> bool {
                return ProcessProtocolList(configuration, value);
            }},

            {"errors.explain-level", [&] (std::string_view, std::string_view value) -> bool {
                return ProcessExplainLevel(configuration, value);
            }},

            {"suggestions", [&] (std::string_view, std::string_view value) -> bool {
                configuration.suggestionLevel = ProcessSuggestions(value);
                return true;
            }},
        };

        while (std::getline(fileStream, line)) {
            auto numberSignPosition = line.find_first_of('#');
            std::string_view sv{line};

            if (numberSignPosition != std::string::npos) {
                sv = sv.substr(0, numberSignPosition);
            }

            sv = TrimStringView(sv);
            if (std::empty(sv) || (sv.length() == 1 && sv[0] == '\0'))
                continue;

            auto pos = sv.find('=');

            if (pos == std::string::npos) {
                name = TrimStringView(sv);
            } else {
                name = TrimStringView(sv.substr(0, pos));
                value = TrimStringView(sv.substr(pos + 1));
            }

            auto entry = functionMap.find(name);
            if (entry == std::end(functionMap)) {
                std::printf("\x1b[37mUnknown configuration key \x1b[36m%s\x1b[37m.\n", std::string(name).c_str());
                return std::nullopt;
            }

            if (!entry->second(name, value)) {
                return std::nullopt;
            }
        }

        return configuration;
    }

    constexpr std::string_view
    TrimStringView(std::string_view sv) noexcept {
        if (std::empty(sv))
            return {};

        std::string_view::size_type begin{0};
        std::string_view::size_type end{std::size(sv) - 1};

        while (sv[begin] == ' ' || sv[begin] == '\t') {
            ++begin;
        }

        while (sv[end] == ' ' || sv[end] == '\t') {
            if (end <= begin)
                return {};
            --end;
        }

        return sv.substr(begin, end - begin + 1);
    }

    bool
    ProcessExplainLevel(Base::Configuration &configuration, std::string_view inValue) noexcept {
        std::string value{inValue};
        std::for_each(std::begin(value), std::end(value), [](char &character) {
            character = static_cast<char>(std::tolower(character));
        });

        if (value == "all" || value == "descriptive" || value == "everything") {
            configuration.httpSettings.errorExplanationLevel = HTTPSettings::ExplainLevel::EVERYTHING;
            return true;
        }

        if (value == "limited" || value == "minimal" || value == "only-name" || value == "show-error-name") {
            configuration.httpSettings.errorExplanationLevel = HTTPSettings::ExplainLevel::SHOW_ERROR_NAME;
            return true;
        }

        if (value == "hide" || value == "no-explanation") {
            configuration.httpSettings.errorExplanationLevel = HTTPSettings::ExplainLevel::HIDE_ERROR;
            return true;
        }

        Messages::UnknownValue("Suggestion Level", "suggestions", value, std::array {
            "all", "descriptive", "everything",
            "limited", "minimal", "only-name", "show-error-name",
            "hide", "no-explanation",
        });

        return false;
    }

    [[nodiscard]] bool
    ProcessProtocol(Base::Configuration &configuration, std::string_view protocol, bool negated) noexcept {
        if (protocol == "http/1.1") {
            configuration.enabledProtocols.http1 = !negated;
        } else if (protocol == "http/2") {
            configuration.enabledProtocols.http2 = !negated;
        } else {
            Messages::UnknownValue("Protocol List", "protocols", protocol, std::array{
                "http/1.1", "http/2"
            });
            return false;
        }

        return true;
    }

    [[nodiscard]] bool
    ProcessProtocolList(Base::Configuration &configuration,
                          std::string_view sv) {
        if (std::empty(sv)) {
            Messages::EmptyValue("Protocol List", "protocols");
            return false;
        }

        if (configuration.enabledProtocols.initialValues) {
            configuration.enabledProtocols.initialValues = false;

            // Set all protocols to false. These can be enabled again by
            // specifying them in the value.
            configuration.enabledProtocols.http1 = false;
            configuration.enabledProtocols.http2 = false;
        }

        do {
            auto pos = std::min(sv.find_first_of(' '), sv.find_first_of('\t'));

            auto token = pos == std::string_view::npos
                    ? sv
                    : sv.substr(0, pos);

            if (std::empty(token))
                break;

            bool negated = false;
            if (token.front() == '!') {
                negated = true;
                token = token.substr(1);
            }

            if (!ProcessProtocol(configuration, token, negated))
                return false;

            if (pos == std::string_view::npos) {
                break;
            }

            sv = sv.substr(pos);
            pos = std::min(sv.find_first_of(' '), sv.find_first_of('\t'));

            for (auto i = pos; i < std::size(sv); ++i) {
                if (sv[i] != ' ' && sv[i] != '\t') {
                    sv = sv.substr(i);
                    break;
                }
            }
        } while (!std::empty(sv));

        return true;
    }

    [[nodiscard]] std::optional<bool>
    ResolveToggle(const char *formalName, std::string_view name, std::string_view inValue) noexcept {
        std::string value{inValue};
        std::for_each(std::begin(value), std::end(value), [](char &character) {
            character = static_cast<char>(std::tolower(character));
        });

        if (value == "on" || value == "enabled" || value == "yes" || value == "true" || value == "1")
            return true;

        if (value == "off" || value == "disabled" || value == "no" || value == "false" || value == "0")
            return false;

        Messages::UnknownValue(formalName, name, value, std::array{
                "on", "enabled", "yes", "true", "1",
                "off", "disabled", "no", "false", "0"
        });

        return std::nullopt;
    }

    SuggestionLevel
    ProcessSuggestions(std::string_view inValue) {
        std::string value{inValue};
        std::for_each(std::begin(value), std::end(value), [](char &character) {
            character = static_cast<char>(std::tolower(character));
        });

        if (value == "off" || value == "disabled" || value == "no" ||
                value == "false" || value == "0") {
            return SuggestionLevel::OFF;
        }

        if (value == "prod" || value == "production")
            return SuggestionLevel::PRODUCTION;

        if (value == "debug" || value == "debugging" ||
                value == "test" || value == "testing") {
            return SuggestionLevel::DEBUGGING;
        }

        Messages::UnknownValue("Suggestion Level", "suggestions", value, std::array {
                "off", "disabled", "no", "false", "0",
                "prod", "production",
                "debug", "debugging", "test", "testing"
        });

        return SuggestionLevel::INITIAL;
    }

} // namespace Base::ConfigurationReader

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string>

namespace Base {

    /**
     * This struct contains settings for HTTP.
     *
     * Mainly the header values. If the string is empty, no such header is sent.
     */
    struct HTTPSettings {

        /**
         * https://svn.tools.ietf.org/svn/wg/httpbis/specs/rfc7231.html#header.server
         */
        std::string server = "Wizard/v2";

        /**
         * https://tools.ietf.org/html/rfc6797
         */
        std::string strictTransportSecurity = "max-age=106384710; includeSubDomains; preload";

        /**
         * Whether  or  not  to  skip the required Host-header validation. This
         * breaches the specification of RFC 7230 § 5.4.
         *
         * This  flag can be useful in an environment where the hostname header
         * varies or is redundant.
         *
         * RFC 7230 § 5.4:
         *   https://tools.ietf.org/html/rfc7230#section-5.4
         */
        bool skipHostValidation = false;

        /**
         * A  lot of sites allow the omission of the Host header. Since this is
         * not  allowed  by the RFC (see above for skipHostValidation), this is
         * disabled by default.
         */
        bool allowMissingHostHeader = false;

        enum class ExplainLevel {
            /**
             * Don't show what went wrong.
             *
             * E.g. Google's "Something went wrong. That's all we know",
             * obviously a lie, but is useful for security purposes.
             */
            HIDE_ERROR,

            /**
             * Show error name.
             *
             * E.g. "Error Code: header-name-empty"
             */
            SHOW_ERROR_NAME,

            /**
             * Show as much information as possible.
             */
            EVERYTHING
        };

        ExplainLevel errorExplanationLevel{ExplainLevel::EVERYTHING};

    };

} // namespace Base

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Base {

    enum class SuggestionLevel {

        /**
         * Will translate to DEBUGGING.
         */
        INITIAL,

        /**
         * Don't show suggestions at all.
         */
        OFF,

        /**
         * Show suggestions useful in a debugging environment.
         */
        DEBUGGING,

        /**
         * Show suggestions useful in production.
         */
        PRODUCTION,

    };

} // namespace Base
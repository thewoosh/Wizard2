/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Base {

    /**
     * The configuration for the HTTP/2 server.
     *
     * This is in no way the settings table for HTTP/2 clients and/or servers.
     * See Source/HTTP/v2/SettingsTable.hpp instead.
     */
    struct HTTP2Settings {

        bool debugPrintSettings{false};
        bool debugPrintIncomingFrames{false};
        bool debugPrintOutgoingFrames{false};
        bool debugPrintOutgoingErrorFrames{false};
        bool debugPrintHeaderFieldTypes{false};
        bool debugPrintHeaderFieldVerbose{false};
        bool debugPrintErrors{false};
        bool debugPrintRequestsAfterHandling{false};

    };

} // namespace Base

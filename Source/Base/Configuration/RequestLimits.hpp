/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

namespace Base {

    /**
     * This struct defines limits for the reading the request. Any std::size_t
     * with value of 0 is unlimited.
     */
    struct RequestLimits {

        std::size_t maximumRequestLineTotalLength{8000};
        std::size_t maximumMethodLength{50};
        std::size_t maximumRequestTargetLength{300};

        // Size in bytes
        // Default: 1 MB
        std::size_t maximumMessageBodySize{1'000'000};

        std::size_t maximumHeaderNameLength{32};
        std::size_t maximumHeaderValueLength{512};

    };

} // namespace Base

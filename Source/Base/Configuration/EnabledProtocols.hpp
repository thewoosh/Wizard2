/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Base {

    struct EnabledProtocols {

        /**
         * This flag specifies if the alpnUse* flags are initially defined.
         */
        bool initialValues{true};

        bool http1 = true;

        /**
         * HTTP/2 implementation is WIP. Enable via configuration.
         */
        bool http2 = false;

    };

} // namespace Base

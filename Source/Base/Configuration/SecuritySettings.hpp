/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace Base {

    struct SecuritySettings {

        /**
         * Should we verify the path using realpath(2)?
         *
         * E.g. prevent against the client retrieving:
         *   /etc/shadow
         * Using request-target:
         *   /../../../etc/shadow
         */
        bool verifyPathUsingRealpath{true};

        /**
         * Another    mechanism,    like   the   realpath-based   one,   except
         * FolderEscapeDetection  is  more  dumb  in  that it doesn't know what
         * directories  are,  but  prevents  chdir-ing to a parent directory by
         * counting the directory level, and decreasing it when it encounters a
         * /../ segment.
         */
        bool verifyPathUsingFolderEscapeDetection{true};

    };

} // namespace Base

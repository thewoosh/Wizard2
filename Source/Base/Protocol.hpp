/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <iostream>

namespace Base {

    // Useful for e.g. ALPN
    enum class ApplicationProtocol {

        HTTP_V1_1,

        HTTP_V2,

    };

    [[nodiscard]] constexpr inline const char *
    TranslateApplicationProtocolToString(ApplicationProtocol clientHandlingState) noexcept {
        switch (clientHandlingState) {
            case ApplicationProtocol::HTTP_V1_1: return "HTTP/1.1";
            case ApplicationProtocol::HTTP_V2: return "HTTP/2";

            default: return "illegal-value";
        }
    }

    inline std::ostream &
    operator<<(std::ostream &stream, ApplicationProtocol applicationProtocol) {
        stream << TranslateApplicationProtocolToString(applicationProtocol);
        return stream;
    }

} // namespace Base

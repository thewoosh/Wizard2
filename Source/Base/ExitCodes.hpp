/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * This file declares exit codes used by main() or abort().
 */

#pragma once

namespace Base::ExitCodes {

    constexpr int Success = 0;
    constexpr int ServerSocketFailure = 1;
    constexpr int InvalidConfiguration = 2;
    constexpr int SecuritySetupFailure = 3;

}

file(GLOB_RECURSE FuzzSources CONFIGURE_DEPENDS */*.cpp)


foreach(FuzzFile ${FuzzSources})
    file(RELATIVE_PATH RelativeName ${CMAKE_CURRENT_SOURCE_DIR} ${FuzzFile})
    get_filename_component(FuzzName ${RelativeName} NAME_WE)
    get_filename_component(DirectoryName ${RelativeName} DIRECTORY)

    string(PREPEND FuzzName "Fuzz_" ${DirectoryName} "_")

    add_executable(${FuzzName} ${RelativeName})
    set_target_properties(${FuzzName} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${DirectoryName})
    target_link_libraries(${FuzzName} BaseLibrary ContentLibrary HTTPLibrary ProviderLibrary)

    target_compile_options(${FuzzName} PRIVATE -fsanitize=fuzzer,address)
    target_link_options(${FuzzName} PRIVATE -fsanitize=fuzzer,address)
endforeach()

#
# Include subdirectories for targets that have special libraries to be linked.
#
add_subdirectory(HTTPv1)

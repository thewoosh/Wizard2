/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string_view>
#include <vector>

#include <cstdint>

#include "Source/Base/Configuration.hpp"
#include "Source/Base/Globals.hpp"
#include "Source/HTTP/v1/Client.hpp"
#include "Source/Provider/MasterProvider.hpp"
#include "Source/Resources/MockConnection.hpp"

namespace Internal {

    Base::Configuration configuration{};
    Provider::MasterProvider masterProvider{configuration};

    bool DoInitialization() {
        Base::Globals::configuration = &configuration;
        Base::Globals::masterProvider = &masterProvider;

        // Skip host validation, since it is pointless on localhost. Should
        // fuzz on this target in the future though, since it is ignored here.
        configuration.hostname = "";
        configuration.httpSettings.allowMissingHostHeader = true;
        configuration.httpSettings.skipHostValidation = true;

        return true;
    }

}

void SendRequest(const std::uint8_t *data, std::size_t size, std::string_view prefix, std::string_view suffix) {
    std::vector<char> vector{};
    vector.resize(std::size(prefix) + size + std::size(suffix));
    std::copy(std::cbegin(prefix), std::cend(prefix), std::begin(vector));
    std::copy(data, data + size, std::data(vector) + std::size(prefix));
    std::copy(std::cbegin(suffix), std::cend(suffix), std::data(vector) + std::size(prefix) + size);

    Resources::MockConnection connection{std::move(vector)};

    HTTP::v1::Client client{&connection};
    client.Continue();
}

[[gnu::always_inline]]
void Fuzz(const uint8_t *data, std::size_t size);

extern "C"
[[maybe_unused]] int
LLVMFuzzerTestOneInput(const uint8_t *data, std::size_t size) {
    [[maybe_unused]] static bool Initialized = Internal::DoInitialization();

    Fuzz(data, size);

    return 0;
}

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <array>

#include "Testing/TestSymbols.hpp"

#include "Source/Base/Conversions.hpp"

struct Data {
    unsigned int value : 24;
};

TEST(ConversionsTest, DestructTestU24) {
    Data data{};
    data.value = 0xABCDEF;
    std::array<std::uint8_t, 3> output{};

    Base::Conversions::Destruct<3>(data.value, std::begin(output));

    EXPECT_EQ(output[0], 0xAB);
    EXPECT_EQ(output[1], 0xCD);
    EXPECT_EQ(output[2], 0xEF);

}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

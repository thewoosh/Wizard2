/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Testing/TestSymbols.hpp"
#include "Source/Content/FileTypeDetector.hpp"

Content::FileTypeDetector fileTypeDetector{};

TEST(FileTypeDetectorTest, SimpleTest) {
    auto mediaType = fileTypeDetector.DetectByFileName("/index.html");
    EXPECT_EQ(mediaType.type, "text");
    EXPECT_EQ(mediaType.subtype, "html");
    EXPECT_TRUE(mediaType.includeCharset);
}

TEST(FileTypeDetectorTest, ImageTest) {
    auto mediaType = fileTypeDetector.DetectByFileName("/test.png");
    EXPECT_EQ(mediaType.type, "image");
    EXPECT_EQ(mediaType.subtype, "png");
    EXPECT_FALSE(mediaType.includeCharset);
}

TEST(FileTypeDetectorTest, NonExistantTest) {
    auto mediaType = fileTypeDetector.DetectByFileName("a.ahahjaihjdihjweuij");
    EXPECT_EQ(mediaType.type, std::string_view());
    EXPECT_EQ(mediaType.subtype, std::string_view());
    EXPECT_FALSE(mediaType.includeCharset);
}

TEST(FileTypeDetectorTest, WithoutExtensionTest) {
    auto mediaType = fileTypeDetector.DetectByFileName("/Assets/Styles/test");
    EXPECT_EQ(mediaType.type, std::string_view());
    EXPECT_EQ(mediaType.subtype, std::string_view());
    EXPECT_FALSE(mediaType.includeCharset);
}

TEST(FileTypeDetectorTest, CapitalistationTest) {
    for (const auto &input : {"test.hTmL", "test.HTML", "test.html",
                              "test.hTML", "test.Html", "test.hTmL"}) {
        auto mediaType = fileTypeDetector.DetectByFileName(input);
        EXPECT_EQ(mediaType.type, "text");
        EXPECT_EQ(mediaType.subtype, "html");
        EXPECT_TRUE(mediaType.includeCharset);
    }
}

TEST(FileTypeDetectorTest, JSONTest) {
    auto mediaType = fileTypeDetector.DetectByFileName("/.well-known/gpc.json");
    EXPECT_EQ(mediaType.type, "application");
    EXPECT_EQ(mediaType.subtype, "json");
    EXPECT_TRUE(mediaType.includeCharset);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include <fstream>
#include <string_view>
#include <thread>

#include <csignal>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "Testing/TestSymbols.hpp"

#include "Source/Simulation/TempFile.hpp"
#include "Source/Simulation/TestConnection.hpp"

const char *globalTestExecutable = nullptr;

class SimulatorEnvironment : public ::testing::Test {
    Simulation::TempFile configurationFile{};
    Simulation::TempFile stdoutFile{};

    std::string testExecutableDup{};
    std::string environmentConfigurationFileName{};
    std::array<char *, 2> environment{};
    std::array<char *, 2> argv{};

    int pipefd[2]{};
    pid_t wizardPID{0};

    void WriteConfiguration() {
        const char config[] = R"(server-socket.port = 64800
tls = disable
hostname =
)";

        const char *ptr = config;
        std::size_t len = sizeof(config) / sizeof(config[0]);

        do {
            const auto ret = write(configurationFile.FD(), ptr, len);

            if (ret == -1) {
                perror("tempFile write(2)");
                throw std::runtime_error("Failed to write configuration to temporary file");
            }

            len -= static_cast<std::size_t>(ret);
            ptr += ret;
        } while (len > 0);

        fsync(configurationFile.FD());
    }

    void SetArgv() {
        testExecutableDup = globalTestExecutable;
        argv[0] = std::data(testExecutableDup);
        argv[1] = nullptr;
    }

    void CreatePipes() {
        if (pipe(pipefd) == -1) {
            perror("pipe");
            exit(EXIT_FAILURE);
        }
    }

    void SetEnvironment() {
        environmentConfigurationFileName = "WIZARD2_CONFIG=";
        environmentConfigurationFileName += configurationFile.Path();
        environment[0] = std::data(environmentConfigurationFileName);
        environment[1] = nullptr;
    }

    void DoFork() {
        auto pid = fork();
        if (pid == -1) {
            FAIL() << "fork() failed";
        } else if (pid == 0) {
            close(pipefd[0]); // close unnecessary read fd

            if (dup2(pipefd[1], STDOUT_FILENO) < 0) {
                perror("dup2 on fd0");
                throw std::runtime_error("dup2 error");
            }

            close(STDERR_FILENO);

            if (execve(globalTestExecutable, &argv[0], &environment[0]) == -1) {
                perror("execve");
                throw std::runtime_error("DoFork: execve failure");
            }
        } else {
            ContinueSetUp(pid);
        }
    }

    void ContinueSetUp(pid_t pid) {
        wizardPID = pid;

        std::thread([this]() {
            int status;
            if (waitpid(wizardPID, &status, 0) == -1) {
                perror("waitpid(2)");
                FAIL() << "Failed to waitpid()";
            }
            if (WIFEXITED(status)) {
                wizardPID = 0;
                printf("Process %d returned %d\n", wizardPID, WEXITSTATUS(status));
            }
//            if (WIFSIGNALED(status)) {
//                printf("Process %d killed: signal %d%s\n",
//                       pid, WTERMSIG(status),
//                       WCOREDUMP(status) ? " - core dumped" : "");
//            }
        }).detach();

        std::array<char, 128> buf{};
        close(pipefd[1]); // close unnecessary write fd

        do {
            const auto ret = read(pipefd[0], std::data(buf), std::size(buf));
            if (ret < 0) {
                perror("read(2)");
                FAIL() << "Failed to read for child pipe";
            }

            if (ret == 0) {
                FAIL() << "EOF on read from child pipe";
            }

            std::string_view sv(std::data(buf), static_cast<std::size_t>(ret));

            std::cout << sv << '\n';

            if (sv.find("64800") != std::string_view::npos) {
                std::cout << "Socket opened: " << sv << '\n';
                break;
            }
        } while (true);
    }

    void SetUp() override {
        CreatePipes();
        SetArgv();
        SetEnvironment();
        WriteConfiguration();
        DoFork();
    }

    void TearDown() override {
        if (wizardPID != 0) {
            kill(wizardPID, SIGABRT);
        }
    }

};

TEST_F(SimulatorEnvironment, TestConditions) {
    Simulation::TestConnection connection{};
    connection.Write("GET / HTTP/1.1\r\n"
                     "Host: localhost:64800\r\n"
                     "\r\n");
    std::string rawStartLine = connection.ReadUntilCRLF();
    std::string_view startLine{rawStartLine};

    auto firstSpace = startLine.find_first_of(' ');
    ASSERT_NE(firstSpace, std::string::npos) << "Malformed status-line";

    std::string_view protocol{startLine.substr(0, firstSpace)};
    ASSERT_EQ(protocol, "HTTP/1.1");

    std::uint8_t statusCodeParts[3]{
        static_cast<std::uint8_t>(startLine[firstSpace + 1] - '0'),
        static_cast<std::uint8_t>(startLine[firstSpace + 2] - '0'),
        static_cast<std::uint8_t>(startLine[firstSpace + 3] - '0')
    };

    auto statusCode{
        statusCodeParts[0] * 100 +
        statusCodeParts[1] * 10 +
        statusCodeParts[2]
    };

    EXPECT_GT(statusCode, 99) << "Invalid Status Code";
    EXPECT_LT(statusCode, 600) << "Invalid Status Code";
    EXPECT_LT(statusCode, 400) << "Request Failure";
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    for (int i = 0; i < argc; ++i) {
        std::string_view sv{argv[i]};

        const char prefix[] = "--test-executable=";
        if (sv.starts_with(prefix)) {
            globalTestExecutable = std::data(sv) + (sizeof(prefix)/sizeof(prefix[0])) - 1;
        }
    }

    if (!globalTestExecutable) {
        std::cerr << "TEST_EXECUTABLE option wasn't given!\n";
        return EXIT_FAILURE;
    }

    return RUN_ALL_TESTS();
}


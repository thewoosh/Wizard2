/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "gtest/gtest.h"

#include <random>
#include <string_view>

using namespace std::literals;

static std::random_device randomDevice;

std::uniform_int_distribution<std::size_t> sizeDistribution(2, 20);

/**
 * Defines how many samples we should take when using random input values.
 */
static constexpr std::size_t RANDOM_SAMPLES = 16;

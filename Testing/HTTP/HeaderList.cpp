/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Testing/TestSymbols.hpp"
#include "Source/HTTP/HeaderList.hpp"

TEST(HeaderList, ContainsString) {
    HTTP::HeaderList headerList{};
    headerList.Add(std::string("Content-Type"), std::string("text/html;charset=utf-8"));
    EXPECT_TRUE(headerList.Contains("Content-Type"));
    EXPECT_FALSE(headerList.Contains("Content-Type: text/html;charset=utf-8"));
    EXPECT_FALSE(headerList.Contains("content-type"));
    EXPECT_FALSE(headerList.Contains("something-else"));
}

TEST(HeaderList, ContainsStringView) {
    HTTP::HeaderList headerList{};
    headerList.Add("Content-Type", "text/html;charset=utf-8");
    EXPECT_TRUE(headerList.Contains("Content-Type"));
    EXPECT_FALSE(headerList.Contains("Content-Type: text/html;charset=utf-8"));
    EXPECT_FALSE(headerList.Contains("content-type"));
    EXPECT_FALSE(headerList.Contains("something-else"));
}

TEST(HeaderList, AddString) {
    HTTP::HeaderList headerList{};
    headerList.Add(std::string("StringIsKeyOnly"), "Value");
    headerList.Add(std::string("StringIsBoth"), std::string("Value"));
    headerList.Add("StringIsValueOnly", std::string("Value"));

    EXPECT_EQ(std::size(headerList.strings), 3);
    EXPECT_EQ(std::size(headerList.stringViews), 0);
    EXPECT_TRUE(headerList.Contains("StringIsKeyOnly"));
    EXPECT_TRUE(headerList.Contains("StringIsBoth"));
    EXPECT_TRUE(headerList.Contains("StringIsValueOnly"));

    EXPECT_EQ(headerList["StringIsKeyOnly"], "Value");
    EXPECT_EQ(headerList["StringIsBoth"], "Value");
    EXPECT_EQ(headerList["StringIsValueOnly"], "Value");
}

//TEST(HeaderList, AddStringView) {
//    HTTP::HeaderList headerList{};
//    std::string_view name = "Name";
//
//    headerList.Add(name, "Value");
//
//    EXPECT_EQ(std::size(headerList.strings), 0);
//    EXPECT_EQ(std::size(headerList.stringViews), 1);
//    EXPECT_TRUE(headerList.Contains("Hello"));
//    EXPECT_EQ(headerList["Hello"], "Value");
//}

TEST(HeaderList, AddStringView_ConstCharPtr) {
    HTTP::HeaderList headerList{};
    const char *name = "Name";

    headerList.Add(name, "Value");

    EXPECT_EQ(std::size(headerList.strings), 0);
    EXPECT_EQ(std::size(headerList.stringViews), 1);
    EXPECT_TRUE(headerList.Contains("Name"));
    EXPECT_EQ(headerList["Name"], "Value");
}

TEST(HeaderList, AddStringView_AsStringCRef) {
    HTTP::HeaderList headerList{};
    const std::string name("Hello");

    headerList.Add(name, "Value");

    EXPECT_EQ(std::size(headerList.strings), 0);
    EXPECT_EQ(std::size(headerList.stringViews), 1);
    EXPECT_TRUE(headerList.Contains("Hello"));
    EXPECT_EQ(headerList["Hello"], "Value");
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

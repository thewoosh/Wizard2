# Wizard2
A WebServer for UNIX-like operating systems.

## Dependencies
This project requires [OpenSSL](https://openssl.org/) for its TLS functionality.
There are intended programmatically ways to use a different TLS/SSL library, but
those haven't been officially implemented.

We create build files using [CMake](https://cmake.org), and recommend using
[Ninja](https://ninja-build.org/) instead of Unix Makefiles. [GCC](https://gcc.gnu.org/)
and [Clang](https://clang.llvm.org/) are officially supported compilers. Any
other C++20 compiler should work though.

## Building
Building the project is pretty straight-forward.
```sh
mkdir build && cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
# Or for Ninja,
cmake -G "ninja" -DCMAKE_BUILD_TYPE=Release ..
```

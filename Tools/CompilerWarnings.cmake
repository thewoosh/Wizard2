add_compile_options(
        -Wall
        -Wextra
        -Wpedantic

        -Wnon-virtual-dtor
        -Wold-style-cast
        -Wcast-align
        -Wunused
        -Woverloaded-virtual
        -Wconversion
        -Wsign-conversion
        -Wnull-dereference
        -Wdouble-promotion

        -Wformat=2
        -Wmissing-noreturn

        -Werror
)

if (CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
    add_compile_options(
            -Wdocumentation
    )
endif()

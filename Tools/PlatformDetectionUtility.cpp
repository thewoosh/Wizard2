/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * Motivation for this program:
 * - Platform detection can fail from time to time. To better pinpoint the
 *   cause, this utility prints out all the information that is used to support
 *   multi-platforming.
 */

#include <iostream>

#include <cstdlib>

#include "Source/Base/Platforms.hpp"
#include "Source/Base/Portability.hpp"

#ifdef BASE_PLATFORM_LINUX
#   include <linux/version.h>
#elif defined (BASE_PLATFORM_UNIX)
#   include <sys/param.h>
#endif

inline void PrintOSName() {
    std::cout << "  \033[0mName:\033[0;35m "
#ifdef BASE_PLATFORM_LINUX
    "Linux\n";
#elif defined (BASE_PLATFORM_BSDI)
    "BSDi\n";
#elif defined (BASE_PLATFORM_DRAGONFLY)
    "DragonFly BSD\n";
#elif defined (BASE_PLATFORM_FREEBSD)
    "FreeBSD\n";
#elif defined (BASE_PLATFORM_NETBSD)
    "NetBSD\n";
#elif defined (BASE_PLATFORM_OPENBSD)
    "OpenBSD\n";
#elif defined (BASE_PLATFORM_MACOS)
    "macOS\n";
#elif defined (BASE_PLATFORM_WINDOWS)
    "Microsoft Windows\n";
#else
    "Unknown\n";
#endif
}

inline void PrintOSVersion() {
    std::cout << "  \033[0mVersion:\033[0;35m "
#ifdef BASE_PLATFORM_LINUX
    << ((LINUX_VERSION_CODE >> 16) & 0xFF) << '.'
    << ((LINUX_VERSION_CODE >> 8) & 0xFF) << '.'
    << (LINUX_VERSION_CODE & 0xFF) << '\n';
#elif defined (BASE_PLATFORM_FREEBSD)
    << (__FreeBSD_version / 100000) << '.'
    << ((__FreeBSD_version / 1000) % 100) << '\n';
#elif defined (BASE_PLATFORM_OPENBSD)
    << (OpenBSD / 100) << '.' << (OpenBSD % 100) << "\n";
#else
    "Unknown\n";
#endif
}

inline void PrintUNIX() {
    std::cout << "  \033[0mUNIX-like\033[0;37m: "
#ifdef BASE_PLATFORM_UNIX
    "\033[32mYES\n";
#else
    "\033[31mNO\n";
#endif
}

inline void PrintPOSIX() {
    std::cout << "  \033[0mPOSIX-compliant\033[0;37m: "
#ifdef BASE_PLATFORM_POSIX
    "\033[32mYES\n";
#else
    "\033[31mNO\n";
#endif
}

inline void PrintBSD() {
    std::cout << "  \033[0mBSD-descendant\033[0;37m: "
#ifdef BASE_PLATFORM_BSD
    "\033[32mYES"

#   ifdef BSD4_4
        "\033[37m (at least BSD4.4)\n";
#   elif BSD4_3
        "\033[37m (at least BSD4.3)\n";
#   else
        "\n";
#   endif
#else
    "\033[31mNO\n";
#endif
}

void PrintCompilerName() {
    std::cout << "  \033[0mName:\033[0;35m "

#ifdef __clang__
    "Clang\n";
#elif __GNUC__
    "GCC\n";
#elif _MSC_VER
    "Visual C++\n";
#else
    "Unknown\n";
#endif
}

void PrintCompilerVersion() {
    std::cout << "  \033[0mVersion:\033[0;35m "

#ifdef __clang__
    << __clang_version__ << '\n';
#elif __GNUC__
    << __GNUC__ << '.' << __GNUC_MINOR__ << '.' << __GNUC_PATCHLEVEL__ << '\n';
#else
    "Unknown\n";
#endif
}

void PrintLanguageVersion() {
    std::cout << "  \033[0mLanguage Version: \033[0;35m"

#if __cplusplus == 199711L
    "C++98\n";
#elif __cplusplus == 201103L
    "C++11\n";
#elif __cplusplus == 201402L
    "C++14\n";
#elif __cplusplus == 201703L
    "C++17\n";
#elif __cplusplus == 202002L
    "C++20\n";
#elif __cplusplus > 202002L
    "C++ Experimental";
#elif __cplusplus == 201709L
    "Probably GNU++20\n";
#else
    "Unknown C++ version: " << __cplusplus << '\n';
#endif
}

void PrintRelaxedConstexpr() {
    std::cout << "  \033[0mRelaxed Constexpr (C++20)\033[0;37m: "
#if BASE_PORTABILITY_CXX20_CONSTEXPR_STATUS == 1
    "\033[32mYES\n";
#else
    "\033[31mNO\n";
#endif
}

void PrintEventQueue() {
    std::cout << "  \033[0mEvent Queue: \033[0;35m"
#ifdef BASE_PLATFORM_LINUX
    "epoll\n";
#elif defined (BASE_PLATFORM_BSD)
    "kqueue\n";
#else
    "unsupported\n";
#endif
}

int main() {
    std::cout << "\033[1;35mOperating System>\n";

    PrintOSName();
    PrintOSVersion();
    std::cout << '\n';

    PrintUNIX();
    PrintPOSIX();
    PrintBSD();
    std::cout << '\n';

    std::cout << "\033[1;35mCompiler>\n";
    PrintCompilerName();
    PrintCompilerVersion();

    std::cout << '\n';
    PrintLanguageVersion();
    PrintRelaxedConstexpr();
    std::cout << '\n';


    std::cout << "\033[1;35mPlatform-dependent modules>\n";
    PrintEventQueue();

    std::cout << "\033[0m";

    return EXIT_SUCCESS;
}

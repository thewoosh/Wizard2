#
# TLS library
#

add_compile_definitions(TLS_USE_OPENSSL)

find_package(OpenSSL REQUIRED)
set(TLS_LIBARIES ${OPENSSL_LIBRARIES})

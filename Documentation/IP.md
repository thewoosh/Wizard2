# IP Documentation
Some platforms have explicit distinctions between IPv4 and IPv6. On Linux for
example: AF_INET is IPv4 and AF_INET6 is IPv6.

IPv6 in the UNIX-like world isn't very standard and syscalls aren't very
compatible. To solve this, some macros exist to support inconsistencies.

## IPv4 to IPv6
In general, to support IPv4 and IPv6 you need to use AF_INET as socket type,
and map IPv4 to IPv6. In Linux, this is done by the kernel by default. On other
systems, you might need to enable this manually.

On FreeBSD, you need to add `ipv6_ipv4mapping="YES"` to the /etc/rc.conf file,
as explained in the
[documentation](https://docs.freebsd.org/en/books/handbook/network-ipv6.html#idp73291512).

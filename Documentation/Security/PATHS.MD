# Paths
This file documents potentially dangerous request-targets, that should be mitigated in some ways.

### Motivation
Allowing `.` and `..` is useful for relative paths, so it shouldn't be
rejected entirely. However, attackers can use the practice to escape the
directory containing the static files, and can possible request the contents of
protected or otherwise private files.

### Illegal Paths

#### Obvious paths
- `/../../../etc/shadow`
- `/../../../var/www/not-html/index.html`
- `/../../../var/www/not-html/index.html`

#### Bruteforcing the Directory Structure
- `/../../../var/www/html/index.html`
- `/test-directory/../../../../var/../var/www/html/index.html`

### Mitigations
#### Segment Stack
Wizard also implements a simple segment-stack based algorithm. It will iterate
through the segments of the *origin* request-target, as defined in
[RFC 3986 § 3.3](https://www.rfc-editor.org/rfc/rfc3986.html#section-3.3).

When an `..` segment is reached, the stack is decreased. When anything other
than `.` and `..` is reached, the stack is increased.

When the segment stack is decreased, but the stack is already 0, the is
considered dangerous.

#### realpath(2)
The `realpath` syscall follows `..`, `.`, and symlinks, and returns the
resolved 'real' path. When this value does not begin with the file system
prefix (e.g. `/var/www/html`), the path should be considered dangerous.

### unveil(2)
The `unveil` system call, available on OpenBSD and SerenityOS, allows the
application programmer to set restrictions on the file system. It specifically
requires the programmer to enter all the paths that the application may want to
access.

This way, Wizard can, for example, only access the configuration file
`/etc/wizard2`, and the static HTML(-related) resources in `/var/www/html/`.
